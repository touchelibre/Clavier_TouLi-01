[Site Web du Projet](http://touchelibre.fr)

# Clavier : TouLi-01

![Logo Projet](01_Système/04_Logo/Logo_ToucheLibre_V3.png)


## Présentation du Projet

![Clavier TouLi](01_Système/04_Logo/ToucheLibre_Fini_02.JPG)

Il s’agit de la pierre angulaire du projet ToucheLibre, le clavier ergonomique en bois.


## Avancement

Voici le reste à faire et l’intention final du projet. L’avancement est donné entre accolade {x %}.

* [x] {100%} Créer le design général.
* [x] {100%} Maquettage de la solution.
* [x] {100%} Créer une disposition spécialement adaptée au clavier ToucheLibre.
* [x] {100%} Fabriquer la partie mécanique du clavier.
* [x] {100%} Faire des maquettes pour définir la carte CPU.
* [x] {100%} Faire les schémas électroniques des cartes.
* [x] {100%} Faire fabriquer le PCB des cartes.
* [x] {100%} Souder tous les composants sur les cartes.
- [ ] {70%} Débeug de l’électronique.
- [ ] {2/5} Fabriquer 5 clavier pour le développement et les béta‑testeurs.
- [ ] {0%} Développement du pilotage des LEDs.
- [ ] {0%} Développement des macro.
- [ ] {0%} Développement de la fonction Bluetooth.
- [ ] {0%} Manuel de fabrication.


## Les dossiers

Le dossier **01_Système** contient tous les éléments architecturant, la spécification générale du produit et toutes les informations qui consernent potentiellement tous les métiers

Les dossiers **02_ à 04_** servent à la conception du produit. Il contient toute l’étude et toutes les sources. Nous sommes organisés par métier. Ici on trouve la mécanique l’électronique, le logiciel, mais on pourrait en imaginer d’autres selon la nature du projet.

Le dossier **05_Fabrication** est destiné à ceux qui veulent  simplement fabriquer le produit sans trop réfléchir au détail de la conception. Ce dossier est un peu l’équivalent de la rubrique «Téléchargement» pour un projet de type logiciel libre. Mais évidement, un objet libre ne se fabrique pas aussi facilement qu’un simple téléchargement. La philosophie de ce dossier est d’expliquer le plus simplement possible comment fabriquer l’objet.


## Licence

La licence attribuée est différente selon le métier considéré. Aussi, il y a une licence différente dans chaque dossier selon la répartition suivante :

| Dossier         | Licence (SPDX identifier) |
| --------------- | ------------------------- |
| 01_Système      | CC-BY-SA-4.0-or-later     |
| 02_Mécanique    | CERN-OHL-S-2.0-or-later   |
| 03_Électronique | CERN-OHL-S-2.0-or-later   |
| 04_Logiciel     | GNU GPL-3.0-or-later      |
| 05_Fabrication  | CC-BY-SA-4.0-or-later     |

Voir <https://spdx.org/licenses/>


## À Propos de l’Auteur

__Lilian Tribouilloy :__

* __Formation :__ Ingénieur en électronique, diplômé de l’[ENSEA](https://www.ensea.fr/fr) en 2004.
* __Métier :__ Concepteur électronique spécialisé dans les radiofréquences et la CEM (Compatibilité ÉlectroMagnétique).
* __Exemples de produit conçu dans le cadre professionnel :__ Émetteur et Réémetteur pour la télévision numérique ; Amplificateur de puissance classe AB pour une modulation OFDM ; Calculateur entrées/sorties pour camion ; Tableau de bord pour véhicule spéciaux ; Boîtier de télématique pour la gestion de flotte.
* __Objet Libre conçu :__ [ToucheLibre](http://touchelibre.fr/), un clavier d’ordinateur ergonomique en bois. Alliant l’esthétique à l’utile, il s’inscrit dans des valeurs de liberté, d’écologie et de santé.


