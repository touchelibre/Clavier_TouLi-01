Ébauche de spécification
=====================

## Préservation de l’activité «programmation»

En programmation, seul les 95 caractères imprimables de ASCII sont utilisés.
(C'est d'ailleurs les seuls caractères disponibles sur un clavier QWERTY US standart.)

```
Soit en détail :				Commentaires : Comment c’est pris en compte par Kéa‑ToucheLibre ?

 _								OK fait avec l'espace et l’espace + AltGr
0123456789.,+-*/=^()			OK fait en direct grâce à partie math. «( ) . , - /» sont aussi en accès sur la partie alpha.
abcdefghijklmnopqrstuvwxyz		OK fait en direct
ABCDEFGHIJKLMNOPQRSTUVWXYZ		OK fait avec MAJ
:;?!							OK fait avec MAJ. Logique OK car sur touches ponctuations.
[]								Fait avec MAJ sur même touches que (). C'est logique, donc OK.
{}								Fait avec AltGr sur même touches que (). C'est logique, donc OK.

@#&%$\"'~|`<>					Reste à travailler pour optimiser la programmation.
```

### Recherche d’une solution :

@#&%$\"'~| \` <> seront placés sur la ligne des caractères spéciaux.
L’utilisation de 2 touches modificatives, typiquement AltGr+MAJ, est à exclure car l’accès n’est pas assez rapide.
Par contre, grace au clavier ToucheLibre, MAJ et AltGr ont le même degré d’accessibilité. 

```
12345«	»67890					position sur la ligne spéciale. Les guillements sont hors du périmétre de recherche.

~/$@"«	»-<>%©					direct		le «‑» est ici inséccable.
`\&#'‹	›-  °®					MAJ
§|   “	”     					AltGr
     ‘	’     					AltGr + MAJ
```

§«»-°© autre paradigme conservé : une place est donnée aux autres spéciaux importants.

____________________________________________________________________________________________________


## Les Paradigmes du clavier ToucheLibre

### Pourquoi changer de clavier ?

Pour les mêmes raisons qui nous ont conduit à remplacer la plume d’oie par un stylo à réservoire.
C’est tellement plus pratique !


### Un clavier pour écrire quoi ?

* Du texte avec une typographie soignée.
* Priorité à l’écriture du français. Écriture de l’anglais et de l’espagnol en seconde optimisation.
* Écrire toutes les langues du monde transcrites ou transcriptibles sur la base de l’alphabet latin.
* Pour un usage scientifique et technique avec l’accès aux symboles mathématiques avancés.
* Accès à l’alphabet grecque pour un usage scientifique et technique.
* Ajout du grecque polyphonique et des lettres grecques archaïques aussi.
* Alphabet Phonétique International pour le lien avec toutes les langues du monde.
* Faciliter l’accès aux caractères spéciaux pour la programmation (ASCII).
* Ajout de certains caractères spéciaux courants, dont symboles monétaires et juridiques.
* Accès aux icônes et émoticônes grâce à la touche «compose».
* Sur les «tables» optionnelles, basculement possible sur les alphabets : grecque, cyrillique, arabe, hébreu ou autres.


### Quelle géométrie pour ce clavier ?

* Résolument conçu pour une véritable ergonomie.
* Rupture avec les claviers ISO et ANSI qui sont non optimisés pour des mains humaines.
* Révolution du traditionnel pavé numérique pour renforcer l’accès aux symboles mathématiques avancés.
* L’usage des pousses est optimisé.
* L’usage des auriculaires est soulagé.


### Quelle disposition pour ce clavier ?

* Toutes les dispositions sont possibles, y compris : qwerty, azerty, dvorak et bépo.
* Toutefois, la nouvelle disposition KÉA est recommandée car spécialement optimisée pour le clavier ToucheLibre.
* Le KÉA est d’abord optimisé pour le français, puis amélioré pour un usage performent de l’anglais et l’espagnol.
* On peut facilement passer d’une disposition à l’autre pour les ami·e·s non-accoutumé·e·s à votre disposition préférée.
* Vous pouvez aussi construire votre propre disposition perso.
* Il est possible de graver sa disposition préférée, mais ce n’est pas recommandé pour 3 raisons : On se coupe de toute  évolution sur le choix de la disposition ; La béquille psychologique de la sériegraphie ne facilite pas l’apprentissage de la dactilographie sans regarder son clavier ; C’est moins chers.


### Configurations possibles pour ce clavier
* Le clavier est modulaire.


### Des innovations majeurs



### Un clavier conçu selon une éthique libre, écologique et sociale.



____________________________________________________________________________________________________

## MODE de SECOURS

Le clavier original Qwerty est le suivant :

![image: Qwerty-US](/media/lilian/Tada_Données/ToucheLibre/01_Système/01_Spécification/00_Données_d_Entrées/KB_USA-international.svg.png)

Ce clavier est la référence technique international. Et on doit pouvoir y revenir en cas de problème. Aussi il convient de pouvoir l’intégré au clavier ToucheLibre. Pour cela, il faut symétriser cette disposition.


### Mode Secours en Qwerty standard US (Qwerty Rescue Mode)

Transposition de la disposition Qwerty-US depuis un clavier ANSI vers le clavier ToucheLibre :

```
`12345	67890-		Direct
=qwert	yuiop/
\asdfg	hjkl;'
¬zxcvb	nm,.[]

~!@#$%	^&*()_		Shift
+QWERT	YUIOP?
|ASDFG	HJKL:"
¦ZXCVB	NM<>{}
```


### Mode de Secours en azerty Français

Transposition de la disposition Azerty-FR depuis un clavier ISO vers le clavier ToucheLibre :

```
²&é"'(	-è_çà)		Direct
=azert	yuiop^
$qsdfg	hjklmù
<wxcvb	n,;:!*

 12345	67890°		Majuscule
+AZERT	YUIOP¨
£QSDFG	HJKLM%
>WXCVB	N?./§µ

  ~#{[	|`\^@]		AltGr
}  €
¤
```

### Recherche de solution

```
123456	7890-=
qwerty	uiop[]
asdfgh	jkl;'\
zxdvbn	m,./`

!@#$%^	&*()_+
QWERTY	UIOP{}
ASDFGH	JKL:"|
ZXCVBN	M<>?~
```

> Minimum de modification, mais problème de décalage droite/gauche

**ou**

```
`12345	67890-
=qwert	yuiop[
]asdfg	hjkl;'
\zxcvb	nm,./

~!@#$%	^&*()_
+QWERT	YUIOP{
}ASDFG	HJKL:"
|ZXCVB	NM<>?
```

> Paranthèse pas côte à côte.

**ou**

```
`12345	67890-
=qwert	yuiop'
?asdfg	hjkl[]
 zxcvb	nm,./\

~!@#$%	^&*()_
+QWERT	YUIOP"
|ASDFG	HJKL{}
 ZXCVB	NM;:<>
```

> la mieux optimisée, mais la disposition ne sera pas reconnu par le plus grand nombre.

**ou**

```
`12345	67890-
=qwert	yuiop/
\asdfg	hjkl;'
¬zxcvb	nm,.[]

~!@#$%	^&*()_
+QWERT	YUIOP?
|ASDFG	HJKL:"
¦ZXCVB	NM<>{}
```

> Le bon compromis. (¬ et ¦ sont optionels car ne sont pas en direct sur un ANSI 104 touches.)



