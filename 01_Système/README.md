[ToucheLibre project](http://touchelibre.fr)

![Logo](http://touchelibre.fr/wp-content/uploads/2019/03/Icon_ToucheLibre_V3.png)


# Partie SYSTÈME

## 00_Datasheet

Ensemble des documentations techniques des composants utilisés dans ce clavier.


## 01_Spécification

Spécification système du clavier ToucheLibre.


## 02_Disposition

Quelques mots sur les dispositions de clavier associées au clavier TouLi et liens vers la disposition Kéa.


## 03_Beugs

Tableau de suivi des beugs, défauts ou autre évolution sur le clavier TouLi.


## 04_Logo

Les logos et images nécessaires à la description du projet.


