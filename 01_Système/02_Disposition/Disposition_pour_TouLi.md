# Les Dispositions de Clavier pour TouLi

## Définition

* __Géométrie :__ Caractéristiques physiques des touches : taille, position, forme des touches…
* __Disposition :__ À quels glyphes ou caractères sont affectés chacune des touches.


## Disposition par Défaut de TouLi

La disposition par défaut de TouLi est **Kéa**.  
Voir le repository « Disposition_Kéa ».

Plus d’information sur le [site ToucheLibre](http://touchelibre.fr/index.php/la-disposition-kea/)


## Construire sa propre disposition

La disposition idéale, c’est celle que vous aurez imaginé pour votre usage spécifique. Nous vous encourageons à faire ce travail de création de disposition.


