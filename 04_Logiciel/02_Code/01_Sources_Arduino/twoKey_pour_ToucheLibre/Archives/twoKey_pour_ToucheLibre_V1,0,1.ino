/*
*********************************************
*   Projet : ToucheLibre                    *
*   Produit : Clavier ergonomique en bois   *
*   Programme : Main                        *
*   But : Première mise en œuvre            *
*   Date : 06 déc 2019                      *
*   Version : 1.0.1                         *
*   CPU : ATmega32U4 by Atmel/Microchip     *
*                                           *
*   http://touchelibre.fr                   *
*   lilian@touchelibre.fr                   *
*   https://gitlab.com/touchelibre          *
*********************************************
*/

/*
Programme forké depuis le projet twoKey:
    Key multiplication for Arduino : allows you to have two keys instead of one
    Copyright (C) 2019  Frederic Pouchal.  All rights reserved.
    https://github.com/fred260571/How-to-build-a-keyboard/blob/master/twoKey-finale.ino
*/

/*
License:
    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  

    See the GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

/*
Description du programme:
    Allows Arduino micro to control 200 keys instead of 100 = (10*10)
    Can easily be modified for standard Arduino and Raspberry Pi
*/

/*
Modification entre v1.0.0 et v1.0.1
Nom des touches a changé pour moins modifier la correspondance avec le qwerty.
*/

// ~~~~~~~~~~ DÉFINITION DES VARIABLES ~~~~~~~~~~

#include "KeyboardHID.h"

const int numSwis = 2; //nombre de switch par nœux
const int numCols = 8; //nombre de colonne dans la matrice
const int numRows = 8; //nombre de ligne dans la matrice

const int colPins[numCols] = {8, 9, 10, 11, 5, 13, 6, 7}; //définition des pins affectées aux colonnes
const int rowPins[numRows] = {A5, A4, A3, A2, A1, A0, A6, A11}; //définition des pins affectées aux lignes

/* Correspondance entre les nets de ToucheLibre, les GPIO du ATmega32U4 et la convention Arduino.
Col_1 = PB4 = 8
Col_2 = PB5 = 9
Col_3 = PB6 = 10
Col_4 = PB7 = 11
Col_5 = PC6 = 5
Col_6 = PC7 = 13
Col_7 = PD7 = 6
Col_8 = PE6 = 7

Lig_F = PF0(ADC0) = A5
Lig_E = PF1(ADC1) = A4
Lig_D = PF4(ADC4) = A3
Lig_C = PF5(ADC5) = A2
Lig_B = PF6(ADC6) = A1
Lig_A = PF7(ADC7) = A0
Lig_M = PD4(ADC8) = A6
Lig_X = PD6(ADC9) = A11
*/

char keyState[numSwis][numCols][numRows]; // État du switch : ON=1 ou OFF=0
char keyValue[numSwis][numCols][numRows]; // Valeur ou scancode HID associée 
// Voir la norme USB de classe HID (Human Interface Device) au § «10 Keyboard/Keypad Page (0x07)»

int sensorValue; // valeur lu pendant le scan sur les lignes configurées en entrée analogique

void (*Press[numSwis][numCols][numRows])(); // Pointeur sur le tableau de fonction Press pour les macros
void (*Release[numSwis][numCols][numRows])(); // Pointeur sur le tableau de fonction Release pour les macros

int key; // variable pour le débeug (à commenter à la fin)



// ~~~~~~~~~~ CONFIGURATION ~~~~~~~~~~~

void setup()
{
  Serial.begin(9600); // Utiliser pour le débeug : Vérifier quelle est la touche activée (à commenter à la fin)

// Initialisation des tableaux
  for (int i = 0; i < numSwis; i++) // Tableau keyState
  {
    for (int j = 0; j < numCols; j++)
    {
      for (int k = 0; k < numRows; k++)
      {
        keyState[i][j][k] = 0;
      }
    }
  }

  for (int i = 0; i < numSwis; i++) // Tableau keyValue
  {
    for (int j = 0; j < numCols; j++)
    {
      for (int k = 0; k < numRows; k++)
      {
        keyValue[i][j][k] = 0;
      }
    }
  }

  for (int i = 0; i < numSwis; i++) // Tableau de fonction Press
  {
    for (int j = 0; j < numCols; j++)
    {
      for (int k = 0; k < numRows; k++)
      {
        Press[i][j][k] = nada;
      }
    }
  }

  for (int i = 0; i < numSwis; i++) // Tableau de fonction Release
  {
    for (int j = 0; j < numCols; j++)
    {
      for (int k = 0; k < numRows; k++)
      {
        Release[i][j][k] = nada;
      }
    }


// Configuration des colonnes en sortie
  for (int column = 0; column < numCols; column++)
  {
    pinMode(colPins[column], OUTPUT); // Configuration en sortie
    digitalWrite(colPins[column], LOW); // État «0» par défaut au lancement du programme
  }

// Configuration des lignes en entrée analogique
  for (int row = 0; row < numRows; row++)
  {
    pinMode(rowPins[row], INPUT); // Configuration en entrée analogique
  }

    // Valeur lu en point de 0 à 1023 car l’ADC est en 10 bits
    // Soit Pour une alimentation en 3,3V: 1pt = 3,22mV
}


// Dénifition de la disposition des touches
// Compatibilité avec le Qwerty US standard

// To be Used               nom touche: correspondance Qwerty US
  keyValue[1][0][3] = 0x04;    // AC01: Keyboard a and A
  keyValue[1][2][4] = 0x05;    // AB05: Keyboard b and B
  keyValue[1][1][4] = 0x06;    // AB03: Keyboard c and C
  keyValue[1][1][3] = 0x07;    // AC03: Keyboard d and D
  keyValue[1][1][2] = 0x08;    // AD03: Keyboard e and E
  keyValue[0][2][3] = 0x09;    // AC04: Keyboard f and F
  keyValue[1][2][3] = 0x0A;    // AC05: Keyboard g and G
  keyValue[0][5][3] = 0x0B;    // AC06: Keyboard h and H
  keyValue[0][6][2] = 0x0C;    // AD08: Keyboard i and I
  keyValue[1][5][3] = 0x0D;    // AC07: Keyboard j and J
  keyValue[0][6][3] = 0x0E;    // AC08: Keyboard k and K
  keyValue[1][6][3] = 0x0F;    // AC09: Keyboard l and L
  keyValue[1][5][4] = 0x10;    // AB07: Keyboard m and M
  keyValue[0][5][4] = 0x11;    // AB06: Keyboard n and N
  keyValue[1][6][2] = 0x12;    // AD09: Keyboard o and O
  keyValue[0][7][2] = 0x13;    // AD10: Keyboard p and P
  keyValue[1][0][2] = 0x14;    // AD01: Keyboard q and Q
  keyValue[0][2][2] = 0x15;    // AD04: Keyboard r and R
  keyValue[0][1][3] = 0x16;    // AC02: Keyboard s and S
  keyValue[1][2][2] = 0x17;    // AD05: Keyboard t and T
  keyValue[1][5][2] = 0x18;    // AD07: Keyboard u and U
  keyValue[0][2][4] = 0x19;    // AB04: Keyboard v and V
  keyValue[0][1][2] = 0x1A;    // AD02: Keyboard w and W
  keyValue[0][1][4] = 0x1B;    // AB02: Keyboard x and X
  keyValue[0][5][2] = 0x1C;    // AD06: Keyboard y and Y
  keyValue[1][0][4] = 0x1D;    // AB01: Keyboard z and Z

  keyValue[1][0][1] = 0x1E;    // AE01: Keyboard 1 and !
  keyValue[0][1][1] = 0x1F;    // AE02: Keyboard 2 and @
  keyValue[1][1][1] = 0x20;    // AE03: Keyboard 3 and #
  keyValue[0][2][1] = 0x21;    // AE04: Keyboard 4 and $
  keyValue[1][2][1] = 0x22;    // AE05: Keyboard 5 and %
  keyValue[0][5][1] = 0x23;    // AE06: Keyboard 6 and ^
  keyValue[1][5][1] = 0x24;    // AE07: Keyboard 7 and &
  keyValue[0][6][1] = 0x25;    // AE08: Keyboard 8 and *
  keyValue[1][6][1] = 0x26;    // AE09: Keyboard 9 and (
  keyValue[0][7][1] = 0x27;    // AE10: Keyboard 0 and )

  keyValue[1][7][1] = 0x2D;    // AE11: Keyboard - and _    modification v1.0.1
  keyValue[1][7][2] = 0x2E;    // AE12: Keyboard = and +    modification v1.0.1
  keyValue[0][7][4] = 0x2F;    // AD11: Keyboard [ and {
  keyValue[1][7][4] = 0x30;    // AD12: Keyboard ] and }
  keyValue[0][0][2] = 0x31;    // BKSL: Keyboard \ and |    modification v1.0.1
//  keyValue[][][] = 0x32;    // Keyboard Non-US # and ~
  keyValue[0][7][3] = 0x33;    // AC10: Keyboard ; and :
  keyValue[1][7][3] = 0x34;    // AC11: Keyboard ' and "
  keyValue[0][0][1] = 0x35;    // TLDE: Keyboard ` and ~
  keyValue[0][6][4] = 0x36;    // AB08: Keyboard , and <
  keyValue[1][6][4] = 0x37;    // AB09: Keyboard . and >
  keyValue[0][0][3] = 0x38;    // AB10: Keyboard / and ?    modification v1.0.1

  keyValue[0][0][4] = 0x64;    // LSGT: Keyboard Non-US \ and |

  keyValue[0][3][4] = 0x2C;    // LSPC: Keyboard Left Spacebar
  keyValue[1][3][4] = 0x2C;    // RSPC: Keyboard Right Spacebar

//  keyValue[0][4][0] = 0x53;    // NMLK: Keypad Num Lock and Clear
  keyValue[1][3][5] = 0x54;    // KPDV: Keypad /
  keyValue[0][1][6] = 0x55;    // KPMU: Keypad *
  keyValue[0][4][5] = 0x56;    // KPSU: Keypad -
  keyValue[0][3][6] = 0x57;    // KPAD: Keypad +
  keyValue[1][4][5] = 0x58;    // KPEN: Keypad ENTER
  keyValue[1][0][6] = 0x59;    // KP1:  Keypad 1 and End
  keyValue[1][2][6] = 0x5A;    // KP2:  Keypad 2 and Down Arrow
  keyValue[1][3][6] = 0x5B;    // KP3:  Keypad 3 and PageDn
  keyValue[1][6][6] = 0x5C;    // KP4:  Keypad 4 and Left Arrow
  keyValue[0][6][6] = 0x5D;    // KP5:  Keypad 5 
  keyValue[1][5][6] = 0x5E;    // KP6:  Keypad 6 and Right Arrow
  keyValue[0][5][6] = 0x5F;    // KP7:  Keypad 7 and Home
  keyValue[1][1][6] = 0x60;    // KP8:  Keypad 8 and Up Arrow
  keyValue[0][0][6] = 0x61;    // KP9:  Keypad 9 and PageUp
  keyValue[0][2][6] = 0x62;    // KP0:  Keypad 0 and Insert
  keyValue[1][4][6] = 0x63;    // KPDL: Keypad . and Delete

  keyValue[0][0][0] = 0x3A;    // FK01: Keyboard F1
  keyValue[1][0][0] = 0x3B;    // FK02: Keyboard F2
  keyValue[0][1][0] = 0x3C;    // FK03: Keyboard F3
  keyValue[1][1][0] = 0x3D;    // FK04: Keyboard F4
  keyValue[0][2][0] = 0x3E;    // FK05: Keyboard F5
  keyValue[1][2][0] = 0x3F;    // FK06: Keyboard F6
  keyValue[0][5][0] = 0x40;    // FK07: Keyboard F7
  keyValue[1][5][0] = 0x41;    // FK08: Keyboard F8
  keyValue[0][6][0] = 0x42;    // FK09: Keyboard F9
  keyValue[1][6][0] = 0x43;    // FK10: Keyboard F10
  keyValue[0][7][0] = 0x44;    // FK11: Keyboard F11
  keyValue[1][7][0] = 0x45;    // FK12: Keyboard F12

//  keyValue[][][] = 0x46;    // Keyboard PrintScreen
//  keyValue[][][] = 0x47;    // Keyboard Scroll Lock
//  keyValue[][][] = 0x48;    // Keyboard Pause
  keyValue[1][3][1] = 0x49;    // INS: Keyboard Insert

  keyValue[0][1][7] = 0x4A;    // HOME: Keyboard Home
  keyValue[1][1][7] = 0x4B;    // PGUP: Keyboard PageUp
  keyValue[1][2][7] = 0x4D;    // END:  Keyboard End
  keyValue[0][2][7] = 0x4E;    // PGDN: Keyboard PageDown
  keyValue[1][6][7] = 0x4F;    // RGHT: Keyboard RightArrow
  keyValue[0][5][7] = 0x50;    // LEFT: Keyboard LeftArrow
  keyValue[1][5][7] = 0x51;    // DOWN: Keyboard DownArrow
  keyValue[0][6][7] = 0x52;    // UP:   Keyboard UpArrow

  keyValue[0][3][2] = 0x28;    // LRTN: Keyboard Left Return (ENTER)
  keyValue[1][3][2] = 0x28;    // RRTN: Keyboard Right Return (ENTER)
  keyValue[0][3][0] = 0x29;    // ESC:  Keyboard ESCAPE
  keyValue[0][3][3] = 0x2A;    // BKSP: Keyboard DELETE (Backspace)
  keyValue[1][3][3] = 0x4C;    // DELE: Keyboard Delete Forward
  keyValue[0][3][1] = 0x2B;    // TAB:  Keyboard Tab

  keyValue[1][2][5] = 0xE0;    // LCTL: Keyboard LeftControl
  keyValue[0][2][5] = 0xE1;    // LFSH: Keyboard LeftShift
  keyValue[1][1][5] = 0xE2;    // LALT: RKeyboard LeftAlt
  keyValue[1][0][5] = 0xE6;    // LAG1: Keyboard LeftAltGr1
  keyValue[0][1][5] = 0xE3;    // LWIN: Keyboard Left GUI
  keyValue[0][5][5] = 0xE4;    // RCTL: Keyboard RightControl
  keyValue[1][5][5] = 0xE5;    // RTSH: Keyboard RightShift
  keyValue[0][6][5] = 0xE2;    // RALT: Keyboard RightAlt
  keyValue[0][7][5] = 0xE6;    // RAG1: Keyboard RightAltGr1
//  keyValue[][][] = 0xE7;    // Keyboard Right GUI

  keyValue[1][7][5] = 0x39;    // CAPS: Keyboard Caps Lock
  keyValue[0][0][5] = 0x76;    // MENU: Keyboard Menu

// Could be interesting

  keyValue[1][4][5] = 0x67;    // KPEQ: Keypad =
//  keyValue[1][4][5] = 0x86;    // Keypad Equal Sign

  keyValue[0][3][5] = 0xC3;    // KPEX: Keypad ^
  keyValue[0][7][6] = 0xB6;    // KPLP: Keypad (
  keyValue[1][7][6] = 0xB7;    // KPRG: Keypad )

  keyValue[0][4][6] = 0x85;    // KPCM: Keypad Comma
//  keyValue[0][4][6] = 0xB3;    // Decimal Separator

  keyValue[0][4][0] = 0xB4;    // KPCU: Currency Unit
/*
  keyValue[][][] = 0x7F;    // Keyboard Mute
  keyValue[][][] = 0x80;    // Keyboard Volume Up
  keyValue[][][] = 0x81;    // Keyboard Volume Down

  keyValue[][][] = 0x82;    // Keyboard Locking Caps Lock
  keyValue[][][] = 0x83;    // Keyboard Locking Num Lock
  keyValue[][][] = 0x84;    // Keyboard Locking Scroll Lock

*/

// Not used
/*
  keyValue[][][] = 0x65;    // Keyboard Application
  keyValue[][][] = 0x66;    // Keyboard Power

  keyValue[][][] = 0x68;    // Keyboard F13
  keyValue[][][] = 0x69;    // Keyboard F14
  keyValue[][][] = 0x6A;    // Keyboard F15
  keyValue[][][] = 0x6B;    // Keyboard F16
  keyValue[][][] = 0x6C;    // Keyboard F17
  keyValue[][][] = 0x6D;    // Keyboard F18
  keyValue[][][] = 0x6E;    // Keyboard F19
  keyValue[][][] = 0x6F;    // Keyboard F20
  keyValue[][][] = 0x70;    // Keyboard F21
  keyValue[][][] = 0x71;    // Keyboard F22
  keyValue[][][] = 0x72;    // Keyboard F23
  keyValue[][][] = 0x73;    // Keyboard F24

  keyValue[][][] = 0x74;    // Keyboard Execute
  keyValue[][][] = 0x75;    // Keyboard Help
  keyValue[][][] = 0x77;    // Keyboard Select
  keyValue[][][] = 0x78;    // Keyboard Stop
  keyValue[][][] = 0x79;    // Keyboard Again
  keyValue[][][] = 0x7A;    // Keyboard Undo
  keyValue[][][] = 0x7B;    // Keyboard Cut
  keyValue[][][] = 0x7C;    // Keyboard Copy
  keyValue[][][] = 0x7D;    // Keyboard Paste
  keyValue[][][] = 0x7E;    // Keyboard Find


  keyValue[][][] = 0x87;    // Keyboard International 1
  keyValue[][][] = 0x88;    // Keyboard International 2
  keyValue[][][] = 0x89;    // Keyboard International 3
  keyValue[][][] = 0x8A;    // Keyboard International 4
  keyValue[][][] = 0x8B;    // Keyboard International 5
  keyValue[][][] = 0x8C;    // Keyboard International 6
  keyValue[][][] = 0x8D;    // Keyboard International 7
  keyValue[][][] = 0x8E;    // Keyboard International 8
  keyValue[][][] = 0x8F;    // Keyboard International 9
  keyValue[][][] = 0x90;    // Keyboard LANG 1
  keyValue[][][] = 0x91;    // Keyboard LANG 2
  keyValue[][][] = 0x92;    // Keyboard LANG 3
  keyValue[][][] = 0x93;    // Keyboard LANG 4
  keyValue[][][] = 0x94;    // Keyboard LANG 5
  keyValue[][][] = 0x95;    // Keyboard LANG 6
  keyValue[][][] = 0x96;    // Keyboard LANG 7
  keyValue[][][] = 0x97;    // Keyboard LANG 8
  keyValue[][][] = 0x98;    // Keyboard LANG 9

  keyValue[][][] = 0x99;    // Keyboard Alternate Erase
  keyValue[][][] = 0x9A;    // Keyboard SysReq/Attention
  keyValue[][][] = 0x9B;    // Keyboard Cancel
  keyValue[][][] = 0x9C;    // Keyboard Clear
  keyValue[][][] = 0x9D;    // Keyboard Prior
  keyValue[][][] = 0x9E;    // Keyboard Return
  keyValue[][][] = 0x9F;    // Keyboard Separator
  keyValue[][][] = 0xA0;    // Keyboard Out
  keyValue[][][] = 0xA1;    // Keyboard Oper
  keyValue[][][] = 0xA2;    // Keyboard Clear/Again
  keyValue[][][] = 0xA3;    // Keyboard CrSel/Props
  keyValue[][][] = 0xA4;    // Keyboard ExSel

  keyValue[][][] = 0xB0;    // Keypad 00
  keyValue[][][] = 0xB1;    // Keypad 000
  keyValue[][][] = 0xB2;    // Thousands Separator
  keyValue[][][] = 0xB5;    // Currency Sub-unit

  keyValue[][][] = 0xB8;    // Keypad {
  keyValue[][][] = 0xB9;    // Keypad }
  keyValue[][][] = 0xBA;    // Keypad Tab
  keyValue[][][] = 0xBB;    // Keypad Backspace
  keyValue[][][] = 0xBC;    // Keypad A
  keyValue[][][] = 0xBD;    // Keypad B
  keyValue[][][] = 0xBE;    // Keypad C
  keyValue[][][] = 0xBF;    // Keypad D
  keyValue[][][] = 0xC0;    // Keypad E
  keyValue[][][] = 0xC1;    // Keypad F
  keyValue[][][] = 0xC2;    // Keypad XOR
  keyValue[][][] = 0xC4;    // Keypad %
  keyValue[][][] = 0xC5;    // Keypad <
  keyValue[][][] = 0xC6;    // Keypad >
  keyValue[][][] = 0xC7;    // Keypad &
  keyValue[][][] = 0xC8;    // Keypad &&
  keyValue[][][] = 0xC9;    // Keypad |
  keyValue[][][] = 0xCA;    // Keypad ||
  keyValue[][][] = 0xCB;    // Keypad :
  keyValue[][][] = 0xCC;    // Keypad #
  keyValue[][][] = 0xCD;    // Keypad Space
  keyValue[][][] = 0xCE;    // Keypad @
  keyValue[][][] = 0xCF;    // Keypad !
  keyValue[][][] = 0xD0;    // Keypad Memory Store
  keyValue[][][] = 0xD1;    // Keypad Memory Recall
  keyValue[][][] = 0xD2;    // Keypad Memory Clear
  keyValue[][][] = 0xD3;    // Keypad Memory Add
  keyValue[][][] = 0xD4;    // Keypad Memory Subtract
  keyValue[][][] = 0xD5;    // Keypad Memory Multiply
  keyValue[][][] = 0xD6;    // Keypad Memory Divide
  keyValue[][][] = 0xD7;    // Keypad +/-
  keyValue[][][] = 0xD8;    // Keypad Clear
  keyValue[][][] = 0xD9;    // Keypad Clear Entry
  keyValue[][][] = 0xDA;    // Keypad Binary
  keyValue[][][] = 0xDB;    // Keypad Octal
  keyValue[][][] = 0xDC;    // Keypad Decimal
  keyValue[][][] = 0xDD;    // Keypad Hexadecimal
*/


// Affectation des touches à une macro
/*
  keyValue[0][0][5] = 0; // Macro 1
  Press[0][0][5] = P005;
  Release[0][0][5] = R005;

  keyValue[1][0][5] = 0; // Macro 2
  Press[1][0][5] = P105;
  Release[1][0][5] = R105;
*/
}


// ~~~~~~~~~~ MATRIÇAGE : Séquence de lecture de la matrice de touches ~~~~~~~~~~
void loop()
{

  int row; // Lignes à activer
  int column; // Colonnes à activer

// Activation des colonnes l’une auprès l’autre
  for (column = 0; column < numCols; column++)
  {
    digitalWrite(colPins[column], HIGH); // Activation de la colonne

// Puis lecture des lignes une par une (Toutes les lignes sont lues à chaque activation d’une colonne).
    for ( row = 0; row < numRows; row++)
    {
      delay(1);  // 1ms d’attente pour stabiliser la lecture. 
                 // => Période du cycle du scan de toutes les touches = numCols x numRows en ms

      sensorValue = analogRead(rowPins[row]); // Lecture la tension sur la pin ligne

      key = 0; // Débeug : initialisation de la variable de débeug


// CAS 1 : Aucune touche appuyée
      if (sensorValue >= 828)
      {
        key = 0;

        // Switch 1 (Col_xx)
        if (keyState[0][column][row] == 1)
        {
          keyState[0][column][row] = 0;

            //Sélection entre une touche normale ou une macro
          if (keyValue[0][column][row]) // Touche normale (keyValue /= 0)
          {
            KeyboardHID.release(keyValue[0][column][row]); // Scancode envoyer au PC
            //key1 release
          }
          else // Touche macro
          {
            Release[0][column][row]();
          }
        }

        // Switch 2 (Col_xxbis)
        if (keyState[1][column][row] == 1)
        {
          keyState[1][column][row] = 0;

            //Sélection entre une touche normale ou une macro
          if (keyValue[1][column][row]) // Touche normale (keyValue /= 0)
          {
            KeyboardHID.release(keyValue[1][column][row]); // Scancode envoyer au PC
            //key2 release
          }
          else // Touche macro
          {
            Release[1][column][row]();
          }
        }
      }


// CAS 2 : Touche Col_xx appuyée
      else if (sensorValue >= 452 && sensorValue <= 620)
      {
        key = 1;

        // Switch 1 (Col_xx)
        if (keyState[0][column][row] == 0)
        {
          keyState[0][column][row] = 1;

            //Sélection entre une touche normale ou une macro
          if (keyValue[0][column][row]) // Touche normale (keyValue /= 0)
          {
            KeyboardHID.press(keyValue[0][column][row]);
            //key1 press
          }

          else // Touche macro
          {
            Press[0][column][row]();
          }

        }

        // Switch 2 (Col_xxbis)
        if (keyState[1][column][row] == 1)
        {
          keyState[1][column][row] = 0;

            //Sélection entre une touche normale ou une macro
          if (keyValue[1][column][row]) // Touche normale (keyValue /= 0)
          {
            KeyboardHID.release(keyValue[1][column][row]);
            //key2 release
          }
          else // Touche macro
          {
            Release[1][column][row]();
          }
        }
      }


// CAS 3 : Touche Col_xxbis appuyée
      else if (sensorValue >= 621 && sensorValue <= 827)
      {
        key = 2;

        // Switch 1 (Col_xx)
        if (keyState[0][column][row] == 1)
        {
          keyState[0][column][row] = 0;

            //Sélection entre une touche normale ou une macro
          if (keyValue[0][column][row]) // Touche normale (keyValue /= 0)
          {
            KeyboardHID.release(keyValue[0][column][row]);
            //key1 release
          }
          else // Touche macro
          {
            Release[0][column][row]();
          }
        }

        // Switch 2 (Col_xxbis)
        if (keyState[1][column][row] == 0)
        {
          keyState[1][column][row] = 1;

            //Sélection entre une touche normale ou une macro
          if (keyValue[1][column][row]) // Touche normale (keyValue /= 0)
          {
            KeyboardHID.press(keyValue[1][column][row]);
            //key2 press
          }
          else // Touche macro
          {
            Press[1][column][row]();
          }
        }
      }


// CAS 4 : Les 2 touches appuyées
      else
      {
        key = 3;

        // Switch 1 (Col_xx)
        if (keyState[0][column][row] == 0)
        {
          keyState[0][column][row] = 1;

            //Sélection entre une touche normale ou une macro
          if (keyValue[0][column][row]) // Touche normale (keyValue /= 0)
          {
            KeyboardHID.press(keyValue[0][column][row]);
            //key1 press
          }
          else // Touche macro
          {
            Press[0][column][row]();
          }
        }

        // Switch 2 (Col_xxbis)
        if (keyState[1][column][row] == 0)
        {
          keyState[1][column][row] = 1;

            //Sélection entre une touche normale ou une macro
          if (keyValue[1][column][row]) // Touche normale (keyValue /= 0)
          {
            KeyboardHID.press(keyValue[1][column][row]);
            //key2 press
          }
          else // Touche macro
          {
            Press[1][column][row]();
          }
        }
}


// Débeug : Renvoi la touche activée

      if (key > 0)
      {
        Serial.print(key - 1);
        Serial.print(column);
        Serial.println(row);
      }
    }

// Fin Débeug


    digitalWrite(colPins[column], LOW); // Déactivation de la colonne avant de passer à la suivante.

  }
}


// Fonction qui fait rien pour sortir proprement.
void nada()
{

}


/*
// ~~~~~~~~~~ MACROS ~~~~~~~~~~

// MACRO 1

void P005() // Press macro 1
{
  if (keyState[0][5][6] == 0)
  {
    KeyboardHID.press(40);
    KeyboardHID.press(11);
    KeyboardHID.press(8);
    KeyboardHID.write(15);
    KeyboardHID.write(15);
    KeyboardHID.press(18);
    KeyboardHID.press(16);
  }
  else
  {
    keyState[0][5][6] = 0;
    KeyboardHID.release(keyValue[0][5][6]);
    KeyboardHID.press(44);
    KeyboardHID.press(29);
    KeyboardHID.write(18);
    KeyboardHID.write(21);
    KeyboardHID.press(15);
    KeyboardHID.press(7);
    KeyboardHID.press(37);
    KeyboardHID.press(40);
  }
}

void R005() // Release macro 1
{
  if (keyState[0][5][6] == 0)
  {
    KeyboardHID.release(40);
    KeyboardHID.release(11);
    KeyboardHID.release(8);
    KeyboardHID.release(18);
    KeyboardHID.release(16);
  }
  else
  {
    keyState[0][5][6] = 0;
    KeyboardHID.release(keyValue[0][5][6]);
    KeyboardHID.release(44);
    KeyboardHID.release(29);
    KeyboardHID.release(15);
    KeyboardHID.release(7);
    KeyboardHID.release(37);
    KeyboardHID.release(40);
  }
}


// MACRO 2

void P105() // Press macro 2
{
  // CTRL-ALT-DEL Keyboard logout
  KeyboardHID.press(KEY_LEFT_CTRL);
  KeyboardHID.press(KEY_LEFT_ALT);
  KeyboardHID.press(KEY_DELETE);
}

void R105() // Release macro 2
{
  KeyboardHID.release(KEY_LEFT_CTRL);
  KeyboardHID.release(KEY_LEFT_ALT);
  KeyboardHID.release(KEY_DELETE);
}

*/

