/*
*********************************************
*   Projet : ToucheLibre                    *
*   Produit : Clavier ergonomique en bois   *
*   Programme : KeyValue pour azerty ou Bépo*
*   But : Définition Touche / Scancode HID  *
*   Date : 15 déc 2019                      *
*   Version : 1.0.0                         *
*   CPU : ATmega32U4 by Atmel/Microchip     *
*                                           *
*   http://touchelibre.fr                   *
*   lilian@touchelibre.fr                   *
*   https://gitlab.com/touchelibre          *
*********************************************
*/

/*
Programme forké depuis le projet twoKey:
    Key multiplication for Arduino : allows you to have two keys instead of one
    Copyright (C) 2019  Frederic Pouchal.  All rights reserved.
    https://github.com/fred260571/How-to-build-a-keyboard/blob/master/KeyValue.ino
*/

/*
License:
    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  

    See the GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

/*
Description du programme:
    Cette bibliothèque a pour but de définir l’affectation entre les Touches et le scancode HID.
    Les touches ont été définie pour obtenir une bonne compatibilité avec le Qwerty US standard.

    Définition de la variable keyValue :
    keyValue[colonne normale(0) ou bis(1)][numéro colonne(0 à 7)][numéro ligne(0 à 7)]

    Correspondance avec le schéma électronique :
    [Col_xx(0) ou Col_xxbis(1)][Col_1(0) à Col_8(7)][Lig_F(0) à Lig_A(5);Lig_M(6);Lig_X(7)]

    Valeur affectée en hexa, voir la norme «Universal Serial Bus (USB) HID Usage Tables 10/28/2004 Version 1.12»
    au § 10 Keyboard/Keypad Page (0x07).
    Document téléchargeable gratuitement sur : https://www.usb.org/documents
*/

// ********** Key Name **********
//
// The key name has been designed to get a good compatibility with Azerty FR layout or Bépo Layout.
//
// ┌─────┬─────┬─────┬─────┬─────┬─────┬─────┐     ┌─────┬─────┬─────┬─────┬─────┬─────┬─────┐
// │ FK01│ FK02│ FK03│ FK04│ FK05│ FK06│ ESC │     │ CLAV│ FK07│ FK08│ FK09│ FK10│ FK11│ FK12│
// └─────┴─────┴─────┴─────┴─────┴─────┤  ⎋  │     │  αβ ├─────┴─────┴─────┴─────┴─────┴─────┘
//                                     └─────┘     └─────┘
// ┏━━━━━┯━━━━━┯━━━━━┯━━━━━┯━━━━━┯━━━━━┓                 ┏━━━━━┯━━━━━┯━━━━━┯━━━━━┯━━━━━┯━━━━━┓
// ┃ TLDE│ AE01│ AE02│ AE03│ AE04│ AE05┃                 ┃ AE06│ AE07│ AE08│ AE09│ AE10│ AE11┃
// ┃ ` ~ │ 1 ! │ 2 @ │ 3 # │ 4 $ │ 5 % ┃                 ┃ 6 ^ │ 7 & │ 8 * │ 9 ( │ 0 ) │ - _ ┃
// ┡━━━━━┿━━━━━┿━━━━━┿━━━━━┿━━━━━╅─────╂─────┐     ┌─────╂─────╆━━━━━┿━━━━━┿━━━━━┿━━━━━┿━━━━━┩
// │ AD12│ AD01│ AD02│ AD03│ AD04┃ AD05┃ TAB │     │ INS ┃ AD06┃ AD07│ AD08│ AD09│ AD10│ AD11│
// │ $ £ │ a A │ z Z │ e E │ r R ┃ t T ┃  ↹  │     │  ⎀  ┃ y Y ┃ u U │ i I │ o O │ p P │ ^ ¨ │
// ├─────┼─────┼─────┼─────┼─────╂─────╂─────┤     ├─────╂─────╂─────┼─────┼─────┼─────┼─────┤
// │ AC12│ AC01│ AC02│ AC03│ AC04┃ AC05┃ RTRN│     │ RTRN┃ AC06┃ AC07│ AC08│ AC09│ AC10│ AC11│
// │ * µ │ q Q │ s S │ d D │ f F ┃ g G ┃  ⏎  │     │  ⏎  ┃ h H ┃ j J │ k K │ l L │ m M │ ù % │
// ├─────┼─────┼─────┼─────┼─────╂─────┨     │     │     ┠─────╂─────┼─────┼─────┼─────┼─────┤
// │ LSGT│ AB01│ AB02│ AB03│ AB04┃ AB05┠─────┤     ├─────┨ AB06┃ AB07│ AB08│ AB09│ AB10│ AE12│
// │ < > │ w W │ x X │ c C │ v V ┃ b B ┃ BKSP│     │ DELE┃ n N ┃ , ? │ ; . │ : / │ ! § │ = + │
// └─────┴─────┴─────┴─────┴─────┺━━━━━┩  ⌫  │     │  ⌦  ┡━━━━━┹─────┴─────┴─────┴─────┴─────┘
//                                     │     │     │     │
//       ┌─────┐           ┌─────┬─────╆━━━━━┪     ┢━━━━━╅─────┬─────┐           ┌─────┐
//       │ PGUP│           │ MENU│ ALGR┃ SPCE┃     ┃ SPCE┃ ALGR│ CAPS│           │  UP │
//       │  ⇞  │           │  ⎄  │  ⇮  ┃  ␣  ┃     ┃  ␣  ┃  ⇮  │  ⇬  │           │  ↑  │
// ┌─────┼─────┼─────┐     ├─────┼─────╄━━━━━┩     ┡━━━━━╃─────┼─────┤     ┌─────┼─────┼─────┐
// │ HOME│/////│ END │     │ LWIN│/////│ LFSH│     │ RTSH│/////│     │     │ LEFT│/////│ RGHT│
// │  ⇱  │/////│  ⇲  │     │  ⌘  │/////│  ⇧  │     │  ⇧  │/////│  Fn │     │  ←  │/////│  →  │
// └─────┼─────┼─────┘     └─────┼─────┼─────┤     ├─────┼─────┼─────┘     └─────┼─────┼─────┘
//       │ PGDN│                 │ LALT│ LCTL│     │ RCTL│ LALT│                 │ DOWN│
//       │  ⇟  │                 │  ⌥  │  ⎈  │     │  ⎈  │  ⌥  │                 │  ↓  │
//       └─────┘                 └─────┴─────┘     └─────┴─────┘                 └─────┘
//
// alias <AC12> = <BKSL>;
//
//                ┌─────┐
//                │ NMLK│
//                │  €  │
//             ┌──┴──┬──┴──┐
//             │ KPLP│ KPRP│
//             │  (  │  )  │
//          ┌──┴──┬──┴──┬──┴──┐
//          │ KP6 │ KP5 │ KP4 │
//          │  6  │  5  │  4  │
//       ┌──┴──┬──┴──┬──┴──┬──┴──┐
//       │ KP7 │ KPDL│ KPPT│ KP3 │
//       │  7  │  ,  │  .  │  3  │
//    ┌──┴──┬──┴──┬──┴──┬──┴──┬──┴──┐
//    │ KPMU│ KP8 │ KP0 │ KP2 │ KPAD│
//    │  *  │  8  │  0  │  2  │  +  │
// ┌──┴──┬──┴──┬──┴──┬──┴──┬──┴──┬──┴──┐
// │ KPEQ│ KPDV│ KP9 │ KP1 │ KPSU│ KPEN│
// │  ^  │  /  │  9  │  1  │  -  │  =  │
// └─────┴─────┴─────┴─────┴─────┴─────┘
//
//	alias <KPPT> = <I129>;


void initKeyAzertyFR()
{
// ~~~~~~~~~~ To be Used ~~~~~~~~~~
//                            Key Name: Value in Qwerty-US  /  If different, value in Azerty-FR
  keyValue[1][0][3] = 0x04;    // AC01: Keyboard a and A   /   q Q
  keyValue[1][2][4] = 0x05;    // AB05: Keyboard b and B
  keyValue[1][1][4] = 0x06;    // AB03: Keyboard c and C
  keyValue[1][1][3] = 0x07;    // AC03: Keyboard d and D
  keyValue[1][1][2] = 0x08;    // AD03: Keyboard e and E
  keyValue[0][2][3] = 0x09;    // AC04: Keyboard f and F
  keyValue[1][2][3] = 0x0A;    // AC05: Keyboard g and G
  keyValue[0][5][3] = 0x0B;    // AC06: Keyboard h and H
  keyValue[0][6][2] = 0x0C;    // AD08: Keyboard i and I
  keyValue[1][5][3] = 0x0D;    // AC07: Keyboard j and J
  keyValue[0][6][3] = 0x0E;    // AC08: Keyboard k and K
  keyValue[1][6][3] = 0x0F;    // AC09: Keyboard l and L
  keyValue[1][5][4] = 0x10;    // AB07: Keyboard m and M   /   , ?
  keyValue[0][5][4] = 0x11;    // AB06: Keyboard n and N
  keyValue[1][6][2] = 0x12;    // AD09: Keyboard o and O
  keyValue[0][7][2] = 0x13;    // AD10: Keyboard p and P
  keyValue[1][0][2] = 0x14;    // AD01: Keyboard q and Q   /   a A
  keyValue[0][2][2] = 0x15;    // AD04: Keyboard r and R
  keyValue[0][1][3] = 0x16;    // AC02: Keyboard s and S
  keyValue[1][2][2] = 0x17;    // AD05: Keyboard t and T
  keyValue[1][5][2] = 0x18;    // AD07: Keyboard u and U
  keyValue[0][2][4] = 0x19;    // AB04: Keyboard v and V
  keyValue[0][1][2] = 0x1A;    // AD02: Keyboard w and W   /   z Z 
  keyValue[0][1][4] = 0x1B;    // AB02: Keyboard x and X
  keyValue[0][5][2] = 0x1C;    // AD06: Keyboard y and Y
  keyValue[1][0][4] = 0x1D;    // AB01: Keyboard z and Z   /   w W

  keyValue[1][0][1] = 0x1E;    // AE01: Keyboard 1 and !   /   & 1
  keyValue[0][1][1] = 0x1F;    // AE02: Keyboard 2 and @   /   é 2
  keyValue[1][1][1] = 0x20;    // AE03: Keyboard 3 and #   /   " 3
  keyValue[0][2][1] = 0x21;    // AE04: Keyboard 4 and $   /   ' 4
  keyValue[1][2][1] = 0x22;    // AE05: Keyboard 5 and %   /   ( 5
  keyValue[0][5][1] = 0x23;    // AE06: Keyboard 6 and ^   /   - 6
  keyValue[1][5][1] = 0x24;    // AE07: Keyboard 7 and &   /   è 7
  keyValue[0][6][1] = 0x25;    // AE08: Keyboard 8 and *   /   _ 8
  keyValue[1][6][1] = 0x26;    // AE09: Keyboard 9 and (   /   ç 9
  keyValue[0][7][1] = 0x27;    // AE10: Keyboard 0 and )   /   à 0

  keyValue[1][7][1] = 0x2D;    // AE11: Keyboard - and _   /   ) °
  keyValue[1][7][4] = 0x2E;    // AE12: Keyboard = and +
  keyValue[1][7][2] = 0x2F;    // AD11: Keyboard [ and {   /   ^ ¨
  keyValue[0][0][2] = 0x30;    // AD12: Keyboard ] and }   /   $ £
  keyValue[0][0][3] = 0x31;    // BKSL: Keyboard \ and |   /   AC12: * µ
//  keyValue[][][] = 0x32;    // Keyboard Non-US # and ~
  keyValue[0][7][3] = 0x33;    // AC10: Keyboard ; and :   /   m M
  keyValue[1][7][3] = 0x34;    // AC11: Keyboard ' and "   /   ù %
  keyValue[0][0][1] = 0x35;    // TLDE: Keyboard ` and ~   /   ²
  keyValue[0][6][4] = 0x36;    // AB08: Keyboard , and <   /   ; .
  keyValue[1][6][4] = 0x37;    // AB09: Keyboard . and >   /   : /
  keyValue[0][7][4] = 0x38;    // AB10: Keyboard / and ?   /   ! §

  keyValue[0][0][4] = 0x64;    // LSGT: Keyboard Non-US \ and |   /   < >

  keyValue[0][3][4] = 0x2C;    // LSPC: Keyboard Left Spacebar
  keyValue[1][3][4] = 0x2C;    // RSPC: Keyboard Right Spacebar

  keyValue[0][4][0] = 0x53;    // NMLK: Keypad Num Lock and Clear ; Utiliser pour «Currency Unit»
  keyValue[1][3][5] = 0x54;    // KPDV: Keypad /
  keyValue[0][1][6] = 0x55;    // KPMU: Keypad *
  keyValue[0][4][5] = 0x56;    // KPSU: Keypad -
  keyValue[0][3][6] = 0x57;    // KPAD: Keypad +
  keyValue[1][4][5] = 0x58;    // KPEN: Keypad ENTER
  keyValue[1][0][6] = 0x59;    // KP1:  Keypad 1 and End
  keyValue[1][2][6] = 0x5A;    // KP2:  Keypad 2 and Down Arrow
  keyValue[1][3][6] = 0x5B;    // KP3:  Keypad 3 and PageDn
  keyValue[1][6][6] = 0x5C;    // KP4:  Keypad 4 and Left Arrow
  keyValue[0][6][6] = 0x5D;    // KP5:  Keypad 5 
  keyValue[1][5][6] = 0x5E;    // KP6:  Keypad 6 and Right Arrow
  keyValue[0][5][6] = 0x5F;    // KP7:  Keypad 7 and Home
  keyValue[1][1][6] = 0x60;    // KP8:  Keypad 8 and Up Arrow
  keyValue[0][0][6] = 0x61;    // KP9:  Keypad 9 and PageUp
  keyValue[0][2][6] = 0x62;    // KP0:  Keypad 0 and Insert
  keyValue[1][4][6] = 0x63;    // KPDL: Keypad . and Delete

  keyValue[0][0][0] = 0x3A;    // FK01: Keyboard F1
  keyValue[1][0][0] = 0x3B;    // FK02: Keyboard F2
  keyValue[0][1][0] = 0x3C;    // FK03: Keyboard F3
  keyValue[1][1][0] = 0x3D;    // FK04: Keyboard F4
  keyValue[0][2][0] = 0x3E;    // FK05: Keyboard F5
  keyValue[1][2][0] = 0x3F;    // FK06: Keyboard F6
  keyValue[0][5][0] = 0x40;    // FK07: Keyboard F7
  keyValue[1][5][0] = 0x41;    // FK08: Keyboard F8
  keyValue[0][6][0] = 0x42;    // FK09: Keyboard F9
  keyValue[1][6][0] = 0x43;    // FK10: Keyboard F10
  keyValue[0][7][0] = 0x44;    // FK11: Keyboard F11
  keyValue[1][7][0] = 0x45;    // FK12: Keyboard F12

  keyValue[0][3][0] = 0x29;    // ESC:  Keyboard ESCAPE

  keyValue[0][1][7] = 0x4A;    // HOME: Keyboard Home
  keyValue[1][2][7] = 0x4D;    // END:  Keyboard End
  keyValue[1][1][7] = 0x4B;    // PGUP: Keyboard PageUp
  keyValue[0][2][7] = 0x4E;    // PGDN: Keyboard PageDown

  keyValue[1][6][7] = 0x4F;    // RGHT: Keyboard RightArrow
  keyValue[0][5][7] = 0x50;    // LEFT: Keyboard LeftArrow
  keyValue[0][6][7] = 0x52;    // UP:   Keyboard UpArrow
  keyValue[1][5][7] = 0x51;    // DOWN: Keyboard DownArrow

  keyValue[0][3][2] = 0x28;    // LRTN: Keyboard Left Return (ENTER)
  keyValue[1][3][2] = 0x28;    // RRTN: Keyboard Right Return (ENTER)

  keyValue[0][3][3] = 0x2A;    // BKSP: Keyboard Backspace
  keyValue[1][3][3] = 0x4C;    // DELE: Keyboard Delete Forward

  keyValue[0][3][1] = 0x2B;    // TAB:  Keyboard Tab
  keyValue[1][3][1] = 0x49;    // INS: Keyboard Insert

  keyValue[0][2][5] = 0xE1;    // LFSH: Keyboard LeftShift
  keyValue[1][5][5] = 0xE5;    // RTSH: Keyboard RightShift
  keyValue[1][2][5] = 0xE0;    // LCTL: Keyboard LeftControl
  keyValue[0][5][5] = 0xE4;    // RCTL: Keyboard RightControl
  keyValue[1][1][5] = 0xE2;    // LALT: RKeyboard LeftAlt
  keyValue[0][6][5] = 0xE2;    // RALT: Keyboard RightAlt
  keyValue[1][0][5] = 0xE6;    // LAG1: Keyboard LeftAltGr1
  keyValue[0][7][5] = 0xE6;    // RAG1: Keyboard RightAltGr1
  keyValue[0][1][5] = 0xE3;    // LWIN: Keyboard Left GUI
//  keyValue[][][] = 0xE7;    // RWIN: Keyboard Right GUI

  keyValue[1][7][5] = 0x39;    // CAPS: Keyboard Caps Lock ; Utiliser pour «Level 5»
  keyValue[0][0][5] = 0x76;    // MENU: Keyboard Menu ; Utiliser pour «Compose»



// ~~~~~~~~~~ To be tested, for additional mathematical key ~~~~~~~~~~

  KeyValue[1][4][5] = 0x67;    // KPEQ: Keypad = ; Utiliser pour «Keypad ^»
//  KeyValue[1][4][5] = 0x86;    // Keypad Equal Sign

//  KeyValue[0][3][5] = 0xC3;    // KPEX: Keypad ^
  KeyValue[0][7][6] = 0xB6;    // KPLP: Keypad (
  KeyValue[1][7][6] = 0xB7;    // KPRG: Keypad )

  KeyValue[0][4][6] = 0x85;    // KPPT: Keypad Comma
//  KeyValue[0][4][6] = 0xB3;    // Decimal Separator

//  KeyValue[0][4][0] = 0xB4;    // KPCU: Currency Unit ; Voir NMLK

//  KeyValue[][][] = 0x32;    // Keyboard Non-US # and ~



// ~~~~~~~~~~ To be tested, for Fn virtual key ~~~~~~~~~~

//  KeyValue[][][] = 0x7F;    // Keyboard Mute
//  KeyValue[][][] = 0x80;    // Keyboard Volume Up
//  KeyValue[][][] = 0x81;    // Keyboard Volume Down

//  KeyValue[][][] = 0x46;    // Keyboard PrintScreen
//  KeyValue[][][] = 0x47;    // Keyboard Scroll Lock
//  KeyValue[][][] = 0x48;    // Keyboard Pause



// ~~~~~~~~~~ To be tested, for LED indicators ~~~~~~~~~~

//  KeyValue[][][] = 0x82;    // Keyboard Locking Caps Lock
//  KeyValue[][][] = 0x83;    // Keyboard Locking Num Lock
//  KeyValue[][][] = 0x84;    // Keyboard Locking Scroll Lock



// ~~~~~~~~~~ Not used, but exist in USB HIB standard ~~~~~~~~~~
/*
  keyValue[][][] = 0x65;    // Keyboard Application
  keyValue[][][] = 0x66;    // Keyboard Power

  keyValue[][][] = 0x68;    // Keyboard F13
  keyValue[][][] = 0x69;    // Keyboard F14
  keyValue[][][] = 0x6A;    // Keyboard F15
  keyValue[][][] = 0x6B;    // Keyboard F16
  keyValue[][][] = 0x6C;    // Keyboard F17
  keyValue[][][] = 0x6D;    // Keyboard F18
  keyValue[][][] = 0x6E;    // Keyboard F19
  keyValue[][][] = 0x6F;    // Keyboard F20
  keyValue[][][] = 0x70;    // Keyboard F21
  keyValue[][][] = 0x71;    // Keyboard F22
  keyValue[][][] = 0x72;    // Keyboard F23
  keyValue[][][] = 0x73;    // Keyboard F24

  keyValue[][][] = 0x74;    // Keyboard Execute
  keyValue[][][] = 0x75;    // Keyboard Help
  keyValue[][][] = 0x77;    // Keyboard Select
  keyValue[][][] = 0x78;    // Keyboard Stop
  keyValue[][][] = 0x79;    // Keyboard Again
  keyValue[][][] = 0x7A;    // Keyboard Undo
  keyValue[][][] = 0x7B;    // Keyboard Cut
  keyValue[][][] = 0x7C;    // Keyboard Copy
  keyValue[][][] = 0x7D;    // Keyboard Paste
  keyValue[][][] = 0x7E;    // Keyboard Find


  keyValue[][][] = 0x87;    // Keyboard International 1
  keyValue[][][] = 0x88;    // Keyboard International 2
  keyValue[][][] = 0x89;    // Keyboard International 3
  keyValue[][][] = 0x8A;    // Keyboard International 4
  keyValue[][][] = 0x8B;    // Keyboard International 5
  keyValue[][][] = 0x8C;    // Keyboard International 6
  keyValue[][][] = 0x8D;    // Keyboard International 7
  keyValue[][][] = 0x8E;    // Keyboard International 8
  keyValue[][][] = 0x8F;    // Keyboard International 9
  keyValue[][][] = 0x90;    // Keyboard LANG 1
  keyValue[][][] = 0x91;    // Keyboard LANG 2
  keyValue[][][] = 0x92;    // Keyboard LANG 3
  keyValue[][][] = 0x93;    // Keyboard LANG 4
  keyValue[][][] = 0x94;    // Keyboard LANG 5
  keyValue[][][] = 0x95;    // Keyboard LANG 6
  keyValue[][][] = 0x96;    // Keyboard LANG 7
  keyValue[][][] = 0x97;    // Keyboard LANG 8
  keyValue[][][] = 0x98;    // Keyboard LANG 9

  keyValue[][][] = 0x99;    // Keyboard Alternate Erase
  keyValue[][][] = 0x9A;    // Keyboard SysReq/Attention
  keyValue[][][] = 0x9B;    // Keyboard Cancel
  keyValue[][][] = 0x9C;    // Keyboard Clear
  keyValue[][][] = 0x9D;    // Keyboard Prior
  keyValue[][][] = 0x9E;    // Keyboard Return
  keyValue[][][] = 0x9F;    // Keyboard Separator
  keyValue[][][] = 0xA0;    // Keyboard Out
  keyValue[][][] = 0xA1;    // Keyboard Oper
  keyValue[][][] = 0xA2;    // Keyboard Clear/Again
  keyValue[][][] = 0xA3;    // Keyboard CrSel/Props
  keyValue[][][] = 0xA4;    // Keyboard ExSel

  keyValue[][][] = 0xB0;    // Keypad 00
  keyValue[][][] = 0xB1;    // Keypad 000
  keyValue[][][] = 0xB2;    // Thousands Separator
  keyValue[][][] = 0xB5;    // Currency Sub-unit

  keyValue[][][] = 0xB8;    // Keypad {
  keyValue[][][] = 0xB9;    // Keypad }
  keyValue[][][] = 0xBA;    // Keypad Tab
  keyValue[][][] = 0xBB;    // Keypad Backspace
  keyValue[][][] = 0xBC;    // Keypad A
  keyValue[][][] = 0xBD;    // Keypad B
  keyValue[][][] = 0xBE;    // Keypad C
  keyValue[][][] = 0xBF;    // Keypad D
  keyValue[][][] = 0xC0;    // Keypad E
  keyValue[][][] = 0xC1;    // Keypad F
  keyValue[][][] = 0xC2;    // Keypad XOR
  keyValue[][][] = 0xC4;    // Keypad %
  keyValue[][][] = 0xC5;    // Keypad <
  keyValue[][][] = 0xC6;    // Keypad >
  keyValue[][][] = 0xC7;    // Keypad &
  keyValue[][][] = 0xC8;    // Keypad &&
  keyValue[][][] = 0xC9;    // Keypad |
  keyValue[][][] = 0xCA;    // Keypad ||
  keyValue[][][] = 0xCB;    // Keypad :
  keyValue[][][] = 0xCC;    // Keypad #
  keyValue[][][] = 0xCD;    // Keypad Space
  keyValue[][][] = 0xCE;    // Keypad @
  keyValue[][][] = 0xCF;    // Keypad !
  keyValue[][][] = 0xD0;    // Keypad Memory Store
  keyValue[][][] = 0xD1;    // Keypad Memory Recall
  keyValue[][][] = 0xD2;    // Keypad Memory Clear
  keyValue[][][] = 0xD3;    // Keypad Memory Add
  keyValue[][][] = 0xD4;    // Keypad Memory Subtract
  keyValue[][][] = 0xD5;    // Keypad Memory Multiply
  keyValue[][][] = 0xD6;    // Keypad Memory Divide
  keyValue[][][] = 0xD7;    // Keypad +/-
  keyValue[][][] = 0xD8;    // Keypad Clear
  keyValue[][][] = 0xD9;    // Keypad Clear Entry
  keyValue[][][] = 0xDA;    // Keypad Binary
  keyValue[][][] = 0xDB;    // Keypad Octal
  keyValue[][][] = 0xDC;    // Keypad Decimal
  keyValue[][][] = 0xDD;    // Keypad Hexadecimal
*/
}

