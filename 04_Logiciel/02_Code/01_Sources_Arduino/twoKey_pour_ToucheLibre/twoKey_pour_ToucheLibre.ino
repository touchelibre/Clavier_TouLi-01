/*
*********************************************
*   Projet : ToucheLibre                    *
*   Produit : Clavier ergonomique en bois   *
*   Programme : Main                        *
*   But : Première mise en œuvre            *
*   Date : 15 déc 2019                      *
*   Version : 1.1.0                         *
*   CPU : ATmega32U4 by Atmel/Microchip     *
*                                           *
*   http://touchelibre.fr                   *
*   lilian@touchelibre.fr                   *
*   https://gitlab.com/touchelibre          *
*********************************************
*/

/*
Programme forké depuis le projet twoKey:
    Key multiplication for Arduino : allows you to have two keys instead of one
    Copyright (C) 2019  Frederic Pouchal.  All rights reserved.
    https://github.com/fred260571/How-to-build-a-keyboard/blob/master/twoKey-finale.ino
*/

/*
License:
    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  

    See the GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

/*
Description du programme:
    Allows Arduino micro to control 200 keys instead of 100 = (10*10)
    Can easily be modified for standard Arduino and Raspberry Pi
*/



// ~~~~~~~~~~ DÉFINITION DES VARIABLES ~~~~~~~~~~

#include "KeyboardHID.h"

// Il faut choisir entre les dispositions / géométries suivantes:
//    * Qwerty / ANSI compatible avec les dispositions Qwerty-US ou Kéa-ToucheLibre
//    * Azerty / ISO compatible avec les dispositions Azerty-FR ou Bépo-ToucheLibre
#include "KeyValue_Qwerty-US.h"
//#include "KeyValue_Azerty-FR.h"

const int numSwis = 2; //nombre de switch par nœux
const int numCols = 8; //nombre de colonne dans la matrice
const int numRows = 8; //nombre de ligne dans la matrice

const int colPins[numCols] = {8, 9, 10, 11, 5, 13, 6, 7}; //définition des pins affectées aux colonnes
const int rowPins[numRows] = {A5, A4, A3, A2, A1, A0, A6, A11}; //définition des pins affectées aux lignes

/* Correspondance entre les nets de ToucheLibre, les GPIO du ATmega32U4 et la convention Arduino.
Col_1 = PB4 = 8
Col_2 = PB5 = 9
Col_3 = PB6 = 10
Col_4 = PB7 = 11
Col_5 = PC6 = 5
Col_6 = PC7 = 13
Col_7 = PD7 = 6
Col_8 = PE6 = 7

Lig_F = PF0(ADC0) = A5
Lig_E = PF1(ADC1) = A4
Lig_D = PF4(ADC4) = A3
Lig_C = PF5(ADC5) = A2
Lig_B = PF6(ADC6) = A1
Lig_A = PF7(ADC7) = A0
Lig_M = PD4(ADC8) = A6
Lig_X = PD6(ADC9) = A11
*/

char keyState[numSwis][numCols][numRows]; // État du switch : ON=1=«Press» ou OFF=0=«Release»
char keyValue[numSwis][numCols][numRows]; // Valeur ou scancode HID associée
// Voir la norme USB de classe HID (Human Interface Device) au § «10 Keyboard/Keypad Page (0x07)»

int sensorValue; // valeur lu pendant le scan sur les lignes configurées en entrée analogique

void (*Press[numSwis][numCols][numRows])(); // Pointeur sur le tableau de fonction Press pour les macros
void (*Release[numSwis][numCols][numRows])(); // Pointeur sur le tableau de fonction Release pour les macros

//int key; // variable pour le débeug


// Fonction qui fait rien pour sortir proprement.
void nada()
{

}

// ~~~~~~~~~~ CONFIGURATION ~~~~~~~~~~~

void setup()
{
//  Serial.begin(9600); // Utiliser pour le débeug : Vérifier quelle est la touche activée

// Initialisation des tableaux
  for (int i = 0; i < numSwis; i++) // Tableau keyState
  {
    for (int j = 0; j < numCols; j++)
    {
      for (int k = 0; k < numRows; k++)
      {
        keyState[i][j][k] = 0;
      }
    }
  }

  for (int i = 0; i < numSwis; i++) // Tableau keyValue
  {
    for (int j = 0; j < numCols; j++)
    {
      for (int k = 0; k < numRows; k++)
      {
        keyValue[i][j][k] = 0;
      }
    }
  }

  for (int i = 0; i < numSwis; i++) // Tableau de fonction Press
  {
    for (int j = 0; j < numCols; j++)
    {
      for (int k = 0; k < numRows; k++)
      {
        Press[i][j][k] = nada;
      }
    }
  }

  for (int i = 0; i < numSwis; i++) // Tableau de fonction Release
  {
    for (int j = 0; j < numCols; j++)
    {
      for (int k = 0; k < numRows; k++)
      {
        Release[i][j][k] = nada;
      }
    }
  }

// Configuration des colonnes en sorties
  for (int column = 0; column < numCols; column++)
  {
    pinMode(colPins[column], OUTPUT); // Configuration en sortie
    digitalWrite(colPins[column], LOW); // État «0» par défaut au lancement du programme
  }


// Configuration des lignes en entrées analogiques
  for (int row = 0; row < numRows; row++)
  {
    pinMode(rowPins[row], INPUT); // Configuration en entrée analogique
    // Valeur lu en point de 0 à 1023 car l’ADC est en 10 bits
    // Soit Pour une alimentation en 3,3V: 1pt = 3,22mV
  }


// Dénifition de la disposition des touches (association touche / scancode HID)
// Il faut choisir entre les dispositions / géométries suivantes:
//    * Qwerty / ANSI compatible avec les dispositions Qwerty-US ou Kéa-ToucheLibre
//    * Azerty / ISO compatible avec les dispositions Azerty-FR ou Bépo-ToucheLibre

  initKeyQwertyUS();  // Voir fichier KeyValue_Qwerty-US.h
//  initKeyAzertyFR();  // Voir fichier KeyValue_Azerty-FR.h

// Affectation des touches à une macro
/*
  // Macro 1 : Touche Fn
  keyValue[0][0][5] = 0; // pour contre‑carrer la précédente définition dans intitKey()
  Press[0][0][5] = Pfn;
  Release[0][0][5] = Rfn;

  // Macro 2 : Touche Verrouillage
  keyValue[1][0][5] = 0; // pour contre‑carrer la précédente définition dans intitKey()
  Press[1][0][5] = Pverrou;
  Release[1][0][5] = Rverrou;

  // Macro 3 : Touche AlphaBeta
  keyValue[1][0][5] = 0; // pour contre‑carrer la précédente définition dans intitKey()
  Press[1][0][5] = Palphabeta;
  Release[1][0][5] = Ralphabeta;

  // Macro 4 : Mode de secours
  keyValue[1][0][5] = 0; // pour contre‑carrer la précédente définition dans intitKey()
  Press[1][0][5] = Phelp;
  Release[1][0][5] = Rhelp;
*/
}


// ~~~~~~~~~~ MATRIÇAGE : Séquence de lecture de la matrice de touches ~~~~~~~~~~
void loop()
{

  int row; // Lignes à activer
  int column; // Colonnes à activer

// Activation des colonnes l’une auprès l’autre
  for (column = 0; column < numCols; column++)
  {
    digitalWrite(colPins[column], HIGH); // Activation de la colonne

// Puis lecture des lignes une par une (Toutes les lignes sont lues à chaque activation d’une colonne).
    for ( row = 0; row < numRows; row++)
    {
      delay(1);  // 1ms d’attente pour stabiliser la lecture. 
                 // => Période du cycle du scan de toutes les touches = numCols x numRows en ms

      sensorValue = analogRead(rowPins[row]); // Lecture la tension sur la pin ligne

      //key = 0; // Débeug : initialisation de la variable de débeug


// CAS 1 : Aucune touche appuyée
      if (sensorValue >= 828)
      {
        //key = 0;

        // Switch 1 (Col_xx)
        if (keyState[0][column][row] == 1)
        {
          keyState[0][column][row] = 0;

            //Sélection entre une touche normale ou une macro
          if (keyValue[0][column][row]) // Touche normale (keyValue /= 0)
          {
            KeyboardHID.release(keyValue[0][column][row]); // Scancode envoyer au PC
            //key1 release
          }
          else // Touche macro
          {
            Release[0][column][row]();
          }
        }

        // Switch 2 (Col_xxbis)
        if (keyState[1][column][row] == 1)
        {
          keyState[1][column][row] = 0;

            //Sélection entre une touche normale ou une macro
          if (keyValue[1][column][row]) // Touche normale (keyValue /= 0)
          {
            KeyboardHID.release(keyValue[1][column][row]); // Scancode envoyer au PC
            //key2 release
          }
          else // Touche macro
          {
            Release[1][column][row]();
          }
        }
      }


// CAS 2 : Touche Col_xx appuyée
      else if (sensorValue >= 452 && sensorValue <= 620)
      {
        //key = 1;

        // Switch 1 (Col_xx)
        if (keyState[0][column][row] == 0)
        {
          keyState[0][column][row] = 1;

            //Sélection entre une touche normale ou une macro
          if (keyValue[0][column][row]) // Touche normale (keyValue /= 0)
          {
            KeyboardHID.press(keyValue[0][column][row]);
            //key1 press
          }

          else // Touche macro
          {
            Press[0][column][row]();
          }

        }

        // Switch 2 (Col_xxbis)
        if (keyState[1][column][row] == 1)
        {
          keyState[1][column][row] = 0;

            //Sélection entre une touche normale ou une macro
          if (keyValue[1][column][row]) // Touche normale (keyValue /= 0)
          {
            KeyboardHID.release(keyValue[1][column][row]);
            //key2 release
          }
          else // Touche macro
          {
            Release[1][column][row]();
          }
        }
      }


// CAS 3 : Touche Col_xxbis appuyée
      else if (sensorValue >= 621 && sensorValue <= 827)
      {
        //key = 2;

        // Switch 1 (Col_xx)
        if (keyState[0][column][row] == 1)
        {
          keyState[0][column][row] = 0;

            //Sélection entre une touche normale ou une macro
          if (keyValue[0][column][row]) // Touche normale (keyValue /= 0)
          {
            KeyboardHID.release(keyValue[0][column][row]);
            //key1 release
          }
          else // Touche macro
          {
            Release[0][column][row]();
          }
        }

        // Switch 2 (Col_xxbis)
        if (keyState[1][column][row] == 0)
        {
          keyState[1][column][row] = 1;

            //Sélection entre une touche normale ou une macro
          if (keyValue[1][column][row]) // Touche normale (keyValue /= 0)
          {
            KeyboardHID.press(keyValue[1][column][row]);
            //key2 press
          }
          else // Touche macro
          {
            Press[1][column][row]();
          }
        }
      }


// CAS 4 : Les 2 touches appuyées
      else
      {
        //key = 3;

        // Switch 1 (Col_xx)
        if (keyState[0][column][row] == 0)
        {
          keyState[0][column][row] = 1;

            //Sélection entre une touche normale ou une macro
          if (keyValue[0][column][row]) // Touche normale (keyValue /= 0)
          {
            KeyboardHID.press(keyValue[0][column][row]);
            //key1 press
          }
          else // Touche macro
          {
            Press[0][column][row]();
          }
        }

        // Switch 2 (Col_xxbis)
        if (keyState[1][column][row] == 0)
        {
          keyState[1][column][row] = 1;

            //Sélection entre une touche normale ou une macro
          if (keyValue[1][column][row]) // Touche normale (keyValue /= 0)
          {
            KeyboardHID.press(keyValue[1][column][row]);
            //key2 press
          }
          else // Touche macro
          {
            Press[1][column][row]();
          }
        }
      }


// Débeug : Renvoi la touche activée
/*
      if (key > 0)
      {
        Serial.print(key - 1);
        Serial.print(column);
        Serial.println(row);
      }
    }
*/
// Fin Débeug


    digitalWrite(colPins[column], LOW); // Déactivation de la colonne avant de passer à la suivante.

    }
  }
}


/*
// ~~~~~~~~~~ MACROS ~~~~~~~~~~

// MACRO 1

void P005() // Press macro 1
{
  if (keyState[0][5][6] == 0)
  {
    KeyboardHID.press(40);
    KeyboardHID.press(11);
    KeyboardHID.press(8);
    KeyboardHID.write(15);
    KeyboardHID.write(15);
    KeyboardHID.press(18);
    KeyboardHID.press(16);
  }
  else
  {
    keyState[0][5][6] = 0;
    KeyboardHID.release(keyValue[0][5][6]);
    KeyboardHID.press(44);
    KeyboardHID.press(29);
    KeyboardHID.write(18);
    KeyboardHID.write(21);
    KeyboardHID.press(15);
    KeyboardHID.press(7);
    KeyboardHID.press(37);
    KeyboardHID.press(40);
  }
}

void R005() // Release macro 1
{
  if (keyState[0][5][6] == 0)
  {
    KeyboardHID.release(40);
    KeyboardHID.release(11);
    KeyboardHID.release(8);
    KeyboardHID.release(18);
    KeyboardHID.release(16);
  }
  else
  {
    keyState[0][5][6] = 0;
    KeyboardHID.release(keyValue[0][5][6]);
    KeyboardHID.release(44);
    KeyboardHID.release(29);
    KeyboardHID.release(15);
    KeyboardHID.release(7);
    KeyboardHID.release(37);
    KeyboardHID.release(40);
  }
}


// MACRO 2

void P105() // Press macro 2
{
  // CTRL-ALT-DEL Keyboard logout
  KeyboardHID.press(KEY_LEFT_CTRL);
  KeyboardHID.press(KEY_LEFT_ALT);
  KeyboardHID.press(KEY_DELETE);
}

void R105() // Release macro 2
{
  KeyboardHID.release(KEY_LEFT_CTRL);
  KeyboardHID.release(KEY_LEFT_ALT);
  KeyboardHID.release(KEY_DELETE);
}

*/
