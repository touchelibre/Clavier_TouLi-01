/*
*********************************************
*   Projet : ToucheLibre                    *
*   Produit : Clavier ergonomique en bois   *
*   Programme : KeyValue pour Qwerty ou Kéa *
*   But : Définition Touche / Scancode HID  *
*   Date : 15 déc 2019                      *
*   Version : 1.0.0                         *
*   CPU : ATmega32U4 by Atmel/Microchip     *
*                                           *
*   http://touchelibre.fr                   *
*   lilian@touchelibre.fr                   *
*   https://gitlab.com/touchelibre          *
*********************************************
*/

/*
Programme forké depuis le projet twoKey:
    Key multiplication for Arduino : allows you to have two keys instead of one
    Copyright (C) 2019  Frederic Pouchal.  All rights reserved.
    https://github.com/fred260571/How-to-build-a-keyboard/blob/master/KeyValue.ino
*/

/*
License:
    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  

    See the GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

/*
Description du programme:
    Cette bibliothèque a pour but de définir l’affectation entre les Touches et le scancode HID.
    Les touches ont été définie pour obtenir une bonne compatibilité avec le Qwerty US standard.

    Définition de la variable KeyValue :
    KeyValue[colonne normale(0) ou bis(1)][numéro colonne(0 à 7)][numéro ligne(0 à 7)]

    Correspondance avec le schéma électronique :
    [Col_xx(0) ou Col_xxbis(1)][Col_1(0) à Col_8(7)][Lig_F(0) à Lig_A(5);Lig_M(6);Lig_X(7)]

    Valeur affectée en hexa, voir la norme «Universal Serial Bus (USB) HID Usage Tables 10/28/2004 Version 1.12»
    au § 10 Keyboard/Keypad Page (0x07).
    Document téléchargeable gratuitement sur : https://www.usb.org/documents
*/

// ********** Key Name **********
//
// The key name has been designed to get a good compatibility with Qwerty US layout and USB HID definition.
//
// ┌─────┬─────┬─────┬─────┬─────┬─────┬─────┐     ┌─────┬─────┬─────┬─────┬─────┬─────┬─────┐
// │ FK01│ FK02│ FK03│ FK04│ FK05│ FK06│ ESC │     │ CLAV│ FK07│ FK08│ FK09│ FK10│ FK11│ FK12│
// └─────┴─────┴─────┴─────┴─────┴─────┤  ⎋  │     │  αβ ├─────┴─────┴─────┴─────┴─────┴─────┘
//                                     └─────┘     └─────┘
// ┏━━━━━┯━━━━━┯━━━━━┯━━━━━┯━━━━━┯━━━━━┓                 ┏━━━━━┯━━━━━┯━━━━━┯━━━━━┯━━━━━┯━━━━━┓
// ┃ TLDE│ AE01│ AE02│ AE03│ AE04│ AE05┃                 ┃ AE06│ AE07│ AE08│ AE09│ AE10│ AE11┃
// ┃ ` ~ │ 1 ! │ 2 @ │ 3 # │ 4 $ │ 5 % ┃                 ┃ 6 ^ │ 7 & │ 8 * │ 9 ( │ 0 ) │ - _ ┃
// ┡━━━━━┿━━━━━┿━━━━━┿━━━━━┿━━━━━╅─────╂─────┐     ┌─────╂─────╆━━━━━┿━━━━━┿━━━━━┿━━━━━┿━━━━━┩
// │ BKSL│ AD01│ AD02│ AD03│ AD04┃ AD05┃ TAB │     │ INS ┃ AD06┃ AD07│ AD08│ AD09│ AD10│ AE12│
// │ \ | │ q Q │ w W │ e E │ r R ┃ t T ┃  ↹  │     │  ⎀  ┃ y Y ┃ u U │ i I │ o O │ p P │ = + │
// ├─────┼─────┼─────┼─────┼─────╂─────╂─────┤     ├─────╂─────╂─────┼─────┼─────┼─────┼─────┤
// │ AB10│ AC01│ AC02│ AC03│ AC04┃ AC05┃ RTRN│     │ RTRN┃ AC06┃ AC07│ AC08│ AC09│ AC10│ AC11│
// │ / ? │ a A │ s S │ d D │ f F ┃ g G ┃  ⏎  │     │  ⏎  ┃ h H ┃ j J │ k K │ l L │ ; : │ ' " │
// ├─────┼─────┼─────┼─────┼─────╂─────┨     │     │     ┠─────╂─────┼─────┼─────┼─────┼─────┤
// │ LSGT│ AB01│ AB02│ AB03│ AB04┃ AB05┠─────┤     ├─────┨ AB06┃ AB07│ AB08│ AB09│ AD11│ AD12│
// │ < > │ z Z │ x X │ c C │ v V ┃ b B ┃ BKSP│     │ DELE┃ n N ┃ m M │ , < │ . > │ [ { │ ] } │
// └─────┴─────┴─────┴─────┴─────┺━━━━━┩  ⌫  │     │  ⌦  ┡━━━━━┹─────┴─────┴─────┴─────┴─────┘
//                                     │     │     │     │
//       ┌─────┐           ┌─────┬─────╆━━━━━┪     ┢━━━━━╅─────┬─────┐           ┌─────┐
//       │ PGUP│           │ MENU│ ALGR┃ SPCE┃     ┃ SPCE┃ ALGR│ CAPS│           │  UP │
//       │  ⇞  │           │  ⎄  │  ⇮  ┃  ␣  ┃     ┃  ␣  ┃  ⇮  │  ⇬  │           │  ↑  │
// ┌─────┼─────┼─────┐     ├─────┼─────╄━━━━━┩     ┡━━━━━╃─────┼─────┤     ┌─────┼─────┼─────┐
// │ HOME│/////│ END │     │ LWIN│/////│ LFSH│     │ RTSH│/////│     │     │ LEFT│/////│ RGHT│
// │  ⇱  │/////│  ⇲  │     │  ⌘  │/////│  ⇧  │     │  ⇧  │/////│  Fn │     │  ←  │/////│  →  │
// └─────┼─────┼─────┘     └─────┼─────┼─────┤     ├─────┼─────┼─────┘     └─────┼─────┼─────┘
//       │ PGDN│                 │ LALT│ LCTL│     │ RCTL│ LALT│                 │ DOWN│
//       │  ⇟  │                 │  ⌥  │  ⎈  │     │  ⎈  │  ⌥  │                 │  ↓  │
//       └─────┘                 └─────┴─────┘     └─────┴─────┘                 └─────┘
//
//                ┌─────┐
//                │ NMLK│
//                │  €  │
//             ┌──┴──┬──┴──┐
//             │ KPLP│ KPRP│
//             │  (  │  )  │
//          ┌──┴──┬──┴──┬──┴──┐
//          │ KP6 │ KP5 │ KP4 │
//          │  6  │  5  │  4  │
//       ┌──┴──┬──┴──┬──┴──┬──┴──┐
//       │ KP7 │ KPDL│ KPPT│ KP3 │
//       │  7  │  ,  │  .  │  3  │
//    ┌──┴──┬──┴──┬──┴──┬──┴──┬──┴──┐
//    │ KPMU│ KP8 │ KP0 │ KP2 │ KPAD│
//    │  *  │  8  │  0  │  2  │  +  │
// ┌──┴──┬──┴──┬──┴──┬──┴──┬──┴──┬──┴──┐
// │ KPEQ│ KPDV│ KP9 │ KP1 │ KPSU│ KPEN│
// │  ^  │  /  │  9  │  1  │  -  │  =  │
// └─────┴─────┴─────┴─────┴─────┴─────┘
//
//	alias <KPPT> = <I129>;


const int NumSwis = 2; //nombre de switch par nœux
const int NumCols = 8; //nombre de colonne dans la matrice
const int NumRows = 8; //nombre de ligne dans la matrice
char KeyValue[NumSwis][NumCols][NumRows]; // Valeur ou scancode HID associée


void initKeyQwertyUS()
{
// ~~~~~~~~~~ To be Used ~~~~~~~~~~
//                            Key Name: value in Qwerty-US layout
  KeyValue[1][0][3] = 0x04;    // AC01: Keyboard a and A
  KeyValue[1][2][4] = 0x05;    // AB05: Keyboard b and B
  KeyValue[1][1][4] = 0x06;    // AB03: Keyboard c and C
  KeyValue[1][1][3] = 0x07;    // AC03: Keyboard d and D
  KeyValue[1][1][2] = 0x08;    // AD03: Keyboard e and E
  KeyValue[0][2][3] = 0x09;    // AC04: Keyboard f and F
  KeyValue[1][2][3] = 0x0A;    // AC05: Keyboard g and G
  KeyValue[0][5][3] = 0x0B;    // AC06: Keyboard h and H
  KeyValue[0][6][2] = 0x0C;    // AD08: Keyboard i and I
  KeyValue[1][5][3] = 0x0D;    // AC07: Keyboard j and J
  KeyValue[0][6][3] = 0x0E;    // AC08: Keyboard k and K
  KeyValue[1][6][3] = 0x0F;    // AC09: Keyboard l and L
  KeyValue[1][5][4] = 0x10;    // AB07: Keyboard m and M
  KeyValue[0][5][4] = 0x11;    // AB06: Keyboard n and N
  KeyValue[1][6][2] = 0x12;    // AD09: Keyboard o and O
  KeyValue[0][7][2] = 0x13;    // AD10: Keyboard p and P
  KeyValue[1][0][2] = 0x14;    // AD01: Keyboard q and Q
  KeyValue[0][2][2] = 0x15;    // AD04: Keyboard r and R
  KeyValue[0][1][3] = 0x16;    // AC02: Keyboard s and S
  KeyValue[1][2][2] = 0x17;    // AD05: Keyboard t and T
  KeyValue[1][5][2] = 0x18;    // AD07: Keyboard u and U
  KeyValue[0][2][4] = 0x19;    // AB04: Keyboard v and V
  KeyValue[0][1][2] = 0x1A;    // AD02: Keyboard w and W
  KeyValue[0][1][4] = 0x1B;    // AB02: Keyboard x and X
  KeyValue[0][5][2] = 0x1C;    // AD06: Keyboard y and Y
  KeyValue[1][0][4] = 0x1D;    // AB01: Keyboard z and Z

  KeyValue[1][0][1] = 0x1E;    // AE01: Keyboard 1 and !
  KeyValue[0][1][1] = 0x1F;    // AE02: Keyboard 2 and @
  KeyValue[1][1][1] = 0x20;    // AE03: Keyboard 3 and #
  KeyValue[0][2][1] = 0x21;    // AE04: Keyboard 4 and $
  KeyValue[1][2][1] = 0x22;    // AE05: Keyboard 5 and %
  KeyValue[0][5][1] = 0x23;    // AE06: Keyboard 6 and ^
  KeyValue[1][5][1] = 0x24;    // AE07: Keyboard 7 and &
  KeyValue[0][6][1] = 0x25;    // AE08: Keyboard 8 and *
  KeyValue[1][6][1] = 0x26;    // AE09: Keyboard 9 and (
  KeyValue[0][7][1] = 0x27;    // AE10: Keyboard 0 and )

  KeyValue[1][7][1] = 0x2D;    // AE11: Keyboard - and _
  KeyValue[1][7][2] = 0x2E;    // AE12: Keyboard = and +
  KeyValue[0][7][4] = 0x2F;    // AD11: Keyboard [ and {
  KeyValue[1][7][4] = 0x30;    // AD12: Keyboard ] and }
  KeyValue[0][0][2] = 0x31;    // BKSL: Keyboard \ and |
  KeyValue[0][7][3] = 0x33;    // AC10: Keyboard ; and :
  KeyValue[1][7][3] = 0x34;    // AC11: Keyboard ' and "
  KeyValue[0][0][1] = 0x35;    // TLDE: Keyboard ` and ~
  KeyValue[0][6][4] = 0x36;    // AB08: Keyboard , and <
  KeyValue[1][6][4] = 0x37;    // AB09: Keyboard . and >
  KeyValue[0][0][3] = 0x38;    // AB10: Keyboard / and ?

  KeyValue[0][0][4] = 0x64;    // LSGT: Keyboard Non-US \ and |

  KeyValue[0][3][4] = 0x2C;    // LSPC: Keyboard Left Spacebar
  KeyValue[1][3][4] = 0x2C;    // RSPC: Keyboard Right Spacebar

  KeyValue[0][4][0] = 0x53;    // NMLK: Keypad Num Lock and Clear ; Utiliser pour «Currency Unit»
  KeyValue[1][3][5] = 0x54;    // KPDV: Keypad /
  KeyValue[0][1][6] = 0x55;    // KPMU: Keypad *
  KeyValue[0][4][5] = 0x56;    // KPSU: Keypad -
  KeyValue[0][3][6] = 0x57;    // KPAD: Keypad +
  KeyValue[1][4][5] = 0x58;    // KPEN: Keypad ENTER
  KeyValue[1][0][6] = 0x59;    // KP1:  Keypad 1 and End
  KeyValue[1][2][6] = 0x5A;    // KP2:  Keypad 2 and Down Arrow
  KeyValue[1][3][6] = 0x5B;    // KP3:  Keypad 3 and PageDn
  KeyValue[1][6][6] = 0x5C;    // KP4:  Keypad 4 and Left Arrow
  KeyValue[0][6][6] = 0x5D;    // KP5:  Keypad 5 
  KeyValue[1][5][6] = 0x5E;    // KP6:  Keypad 6 and Right Arrow
  KeyValue[0][5][6] = 0x5F;    // KP7:  Keypad 7 and Home
  KeyValue[1][1][6] = 0x60;    // KP8:  Keypad 8 and Up Arrow
  KeyValue[0][0][6] = 0x61;    // KP9:  Keypad 9 and PageUp
  KeyValue[0][2][6] = 0x62;    // KP0:  Keypad 0 and Insert
  KeyValue[1][4][6] = 0x63;    // KPDL: Keypad . and Delete

  KeyValue[0][0][0] = 0x3A;    // FK01: Keyboard F1
  KeyValue[1][0][0] = 0x3B;    // FK02: Keyboard F2
  KeyValue[0][1][0] = 0x3C;    // FK03: Keyboard F3
  KeyValue[1][1][0] = 0x3D;    // FK04: Keyboard F4
  KeyValue[0][2][0] = 0x3E;    // FK05: Keyboard F5
  KeyValue[1][2][0] = 0x3F;    // FK06: Keyboard F6
  KeyValue[0][5][0] = 0x40;    // FK07: Keyboard F7
  KeyValue[1][5][0] = 0x41;    // FK08: Keyboard F8
  KeyValue[0][6][0] = 0x42;    // FK09: Keyboard F9
  KeyValue[1][6][0] = 0x43;    // FK10: Keyboard F10
  KeyValue[0][7][0] = 0x44;    // FK11: Keyboard F11
  KeyValue[1][7][0] = 0x45;    // FK12: Keyboard F12

  KeyValue[0][3][0] = 0x29;    // ESC:  Keyboard ESCAPE

  KeyValue[0][1][7] = 0x4A;    // HOME: Keyboard Home
  KeyValue[1][2][7] = 0x4D;    // END:  Keyboard End
  KeyValue[1][1][7] = 0x4B;    // PGUP: Keyboard PageUp
  KeyValue[0][2][7] = 0x4E;    // PGDN: Keyboard PageDown
  
  KeyValue[1][6][7] = 0x4F;    // RGHT: Keyboard RightArrow
  KeyValue[0][5][7] = 0x50;    // LEFT: Keyboard LeftArrow
  KeyValue[0][6][7] = 0x52;    // UP:   Keyboard UpArrow
  KeyValue[1][5][7] = 0x51;    // DOWN: Keyboard DownArrow

  KeyValue[0][3][2] = 0x28;    // LRTN: Keyboard Left Return (ENTER)
  KeyValue[1][3][2] = 0x28;    // RRTN: Keyboard Right Return (ENTER)

  KeyValue[0][3][3] = 0x2A;    // BKSP: Keyboard Backspace
  KeyValue[1][3][3] = 0x4C;    // DELE: Keyboard Delete Forward

  KeyValue[0][3][1] = 0x2B;    // TAB:  Keyboard Tab
  KeyValue[1][3][1] = 0x49;    // INS: Keyboard Insert

  KeyValue[0][2][5] = 0xE1;    // LFSH: Keyboard LeftShift
  KeyValue[1][5][5] = 0xE5;    // RTSH: Keyboard RightShift
  KeyValue[1][2][5] = 0xE0;    // LCTL: Keyboard LeftControl
  KeyValue[0][5][5] = 0xE4;    // RCTL: Keyboard RightControl
  KeyValue[1][1][5] = 0xE2;    // LALT: RKeyboard LeftAlt
  KeyValue[0][6][5] = 0xE2;    // RALT: Keyboard RightAlt
  KeyValue[1][0][5] = 0xE6;    // LAG1: Keyboard LeftAltGr1
  KeyValue[0][7][5] = 0xE6;    // RAG1: Keyboard RightAltGr1
  KeyValue[0][1][5] = 0xE3;    // LWIN: Keyboard Left GUI
//  KeyValue[][][] = 0xE7;    // RWIN: Keyboard Right GUI

  KeyValue[1][7][5] = 0x39;    // CAPS: Keyboard Caps Lock ; Utiliser pour «Level 5»
  KeyValue[0][0][5] = 0x76;    // MENU: Keyboard Menu ; Utiliser pour «Compose»



// ~~~~~~~~~~ To be tested, for additional mathematical key ~~~~~~~~~~

  KeyValue[1][4][5] = 0x67;    // KPEQ: Keypad = ; Utiliser pour «Keypad ^»
//  KeyValue[1][4][5] = 0x86;    // Keypad Equal Sign

//  KeyValue[0][3][5] = 0xC3;    // KPEX: Keypad ^
  KeyValue[0][7][6] = 0xB6;    // KPLP: Keypad (
  KeyValue[1][7][6] = 0xB7;    // KPRG: Keypad )

  KeyValue[0][4][6] = 0x85;    // KPPT: Keypad Comma
//  KeyValue[0][4][6] = 0xB3;    // Decimal Separator

//  KeyValue[0][4][0] = 0xB4;    // KPCU: Currency Unit ; Voir NMLK

//  KeyValue[][][] = 0x32;    // Keyboard Non-US # and ~



// ~~~~~~~~~~ To be tested, for Fn virtual key ~~~~~~~~~~

//  KeyValue[][][] = 0x7F;    // Keyboard Mute
//  KeyValue[][][] = 0x80;    // Keyboard Volume Up
//  KeyValue[][][] = 0x81;    // Keyboard Volume Down

//  KeyValue[][][] = 0x46;    // Keyboard PrintScreen
//  KeyValue[][][] = 0x47;    // Keyboard Scroll Lock
//  KeyValue[][][] = 0x48;    // Keyboard Pause



// ~~~~~~~~~~ To be tested, for LED indicators ~~~~~~~~~~

//  KeyValue[][][] = 0x82;    // Keyboard Locking Caps Lock
//  KeyValue[][][] = 0x83;    // Keyboard Locking Num Lock
//  KeyValue[][][] = 0x84;    // Keyboard Locking Scroll Lock



// ~~~~~~~~~~ Not used, but exist in USB HIB standard ~~~~~~~~~~
/*
  KeyValue[][][] = 0x65;    // Keyboard Application
  KeyValue[][][] = 0x66;    // Keyboard Power

  KeyValue[][][] = 0x68;    // Keyboard F13
  KeyValue[][][] = 0x69;    // Keyboard F14
  KeyValue[][][] = 0x6A;    // Keyboard F15
  KeyValue[][][] = 0x6B;    // Keyboard F16
  KeyValue[][][] = 0x6C;    // Keyboard F17
  KeyValue[][][] = 0x6D;    // Keyboard F18
  KeyValue[][][] = 0x6E;    // Keyboard F19
  KeyValue[][][] = 0x6F;    // Keyboard F20
  KeyValue[][][] = 0x70;    // Keyboard F21
  KeyValue[][][] = 0x71;    // Keyboard F22
  KeyValue[][][] = 0x72;    // Keyboard F23
  KeyValue[][][] = 0x73;    // Keyboard F24

  KeyValue[][][] = 0x74;    // Keyboard Execute
  KeyValue[][][] = 0x75;    // Keyboard Help
  KeyValue[][][] = 0x77;    // Keyboard Select
  KeyValue[][][] = 0x78;    // Keyboard Stop
  KeyValue[][][] = 0x79;    // Keyboard Again
  KeyValue[][][] = 0x7A;    // Keyboard Undo
  KeyValue[][][] = 0x7B;    // Keyboard Cut
  KeyValue[][][] = 0x7C;    // Keyboard Copy
  KeyValue[][][] = 0x7D;    // Keyboard Paste
  KeyValue[][][] = 0x7E;    // Keyboard Find


  KeyValue[][][] = 0x87;    // Keyboard International 1: Ro (Japanese)
  KeyValue[][][] = 0x88;    // Keyboard International 2: Kana (Japanese)
  KeyValue[][][] = 0x89;    // Keyboard International 3: ¥ (Japanese)
  KeyValue[][][] = 0x8A;    // Keyboard International 4: Henkan (Japanese)
  KeyValue[][][] = 0x8B;    // Keyboard International 5: Muhenkan (Japanese)
  KeyValue[][][] = 0x8C;    // Keyboard International 6
  KeyValue[][][] = 0x8D;    // Keyboard International 7
  KeyValue[][][] = 0x8E;    // Keyboard International 8
  KeyValue[][][] = 0x8F;    // Keyboard International 9
  KeyValue[][][] = 0x90;    // Keyboard LANG 1: Hangul / English toggle (Korean)
  KeyValue[][][] = 0x91;    // Keyboard LANG 2: Convert to "Hanja" characters (Korean)
  KeyValue[][][] = 0x92;    // Keyboard LANG 3: Katakana (Japanese)
  KeyValue[][][] = 0x93;    // Keyboard LANG 4: Hiragana (Japanese)
  KeyValue[][][] = 0x94;    // Keyboard LANG 5: "Kaku": Hankaku/Zenkaku ("Full-size"/"Half-size"/"Kanji") when not on Keyboard Tilde key (Japanese)
  KeyValue[][][] = 0x95;    // Keyboard LANG 6
  KeyValue[][][] = 0x96;    // Keyboard LANG 7
  KeyValue[][][] = 0x97;    // Keyboard LANG 8
  KeyValue[][][] = 0x98;    // Keyboard LANG 9

  KeyValue[][][] = 0x99;    // Keyboard Alternate Erase
  KeyValue[][][] = 0x9A;    // Keyboard SysReq/Attention
  KeyValue[][][] = 0x9B;    // Keyboard Cancel
  KeyValue[][][] = 0x9C;    // Keyboard Clear
  KeyValue[][][] = 0x9D;    // Keyboard Prior
  KeyValue[][][] = 0x9E;    // Keyboard Return
  KeyValue[][][] = 0x9F;    // Keyboard Separator
  KeyValue[][][] = 0xA0;    // Keyboard Out
  KeyValue[][][] = 0xA1;    // Keyboard Oper
  KeyValue[][][] = 0xA2;    // Keyboard Clear/Again
  KeyValue[][][] = 0xA3;    // Keyboard CrSel/Props
  KeyValue[][][] = 0xA4;    // Keyboard ExSel

  KeyValue[][][] = 0xB0;    // Keypad 00
  KeyValue[][][] = 0xB1;    // Keypad 000
  KeyValue[][][] = 0xB2;    // Thousands Separator
  KeyValue[][][] = 0xB5;    // Currency Sub-unit

  KeyValue[][][] = 0xB8;    // Keypad {
  KeyValue[][][] = 0xB9;    // Keypad }
  KeyValue[][][] = 0xBA;    // Keypad Tab
  KeyValue[][][] = 0xBB;    // Keypad Backspace
  KeyValue[][][] = 0xBC;    // Keypad A
  KeyValue[][][] = 0xBD;    // Keypad B
  KeyValue[][][] = 0xBE;    // Keypad C
  KeyValue[][][] = 0xBF;    // Keypad D
  KeyValue[][][] = 0xC0;    // Keypad E
  KeyValue[][][] = 0xC1;    // Keypad F
  KeyValue[][][] = 0xC2;    // Keypad XOR
  KeyValue[][][] = 0xC4;    // Keypad %
  KeyValue[][][] = 0xC5;    // Keypad <
  KeyValue[][][] = 0xC6;    // Keypad >
  KeyValue[][][] = 0xC7;    // Keypad &
  KeyValue[][][] = 0xC8;    // Keypad &&
  KeyValue[][][] = 0xC9;    // Keypad |
  KeyValue[][][] = 0xCA;    // Keypad ||
  KeyValue[][][] = 0xCB;    // Keypad :
  KeyValue[][][] = 0xCC;    // Keypad #
  KeyValue[][][] = 0xCD;    // Keypad Space
  KeyValue[][][] = 0xCE;    // Keypad @
  KeyValue[][][] = 0xCF;    // Keypad !
  KeyValue[][][] = 0xD0;    // Keypad Memory Store
  KeyValue[][][] = 0xD1;    // Keypad Memory Recall
  KeyValue[][][] = 0xD2;    // Keypad Memory Clear
  KeyValue[][][] = 0xD3;    // Keypad Memory Add
  KeyValue[][][] = 0xD4;    // Keypad Memory Subtract
  KeyValue[][][] = 0xD5;    // Keypad Memory Multiply
  KeyValue[][][] = 0xD6;    // Keypad Memory Divide
  KeyValue[][][] = 0xD7;    // Keypad +/-
  KeyValue[][][] = 0xD8;    // Keypad Clear
  KeyValue[][][] = 0xD9;    // Keypad Clear Entry
  KeyValue[][][] = 0xDA;    // Keypad Binary
  KeyValue[][][] = 0xDB;    // Keypad Octal
  KeyValue[][][] = 0xDC;    // Keypad Decimal
  KeyValue[][][] = 0xDD;    // Keypad Hexadecimal
*/
}
