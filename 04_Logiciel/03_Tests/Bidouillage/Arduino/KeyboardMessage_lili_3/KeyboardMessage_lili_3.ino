  /*
  Keyboard Message test for the Arduino Leonardo and Micro.
  Sends a text string when a button is pressed.

  The circuit:
  - pushbutton attached from (pin 4;5;6;7) to +5V
  - 10 kilohm resistor attached from (pin 4;5;6;7) to ground

  created 24 Oct 2011 by Ardiuno Team
  modified 10 mars 2019  by Lilian Tribouilloy
*/

#include "KeyboardAzertyFr.h"

const int button1Pin = 2;          // input pin for pushbutton
const int button2Pin = 3; 
const int button3Pin = 4; 
//const int button4Pin = 7; 
//const int button5Pin = 8; 
int previousButton1State = HIGH;   // for checking the state of a pushButton
int previousButton2State = HIGH;
int previousButton3State = HIGH;
//int previousButton4State = HIGH;
//int previousButton5State = HIGH;
int counter = 0;                      // button push counter
int counter2 = 0; 
char ctrlKey = KEY_LEFT_CTRL;
char supprimKey = KEY_DELETE;
char upKey = KEY_UP_ARROW;

void setup() {
  // make the pushButton pin an input:
  pinMode(button1Pin, INPUT);
  pinMode(button2Pin, INPUT);
  pinMode(button3Pin, INPUT);
//  pinMode(button4Pin, INPUT);
//  pinMode(button5Pin, INPUT);  
  // initialize control over the keyboard:
  KeyboardAzertyFr.begin();
}

void loop() {
  
  // Function of the pushbutton 1: Compteur de trucs
  int button1State = digitalRead(button1Pin);
  // if the button state has changed,
  if ((button1State != previousButton1State)
      // and it's currently pressed:
      && (button1State == HIGH)) {
    // increment the button counter
    counter++;
    // type out a message
    KeyboardAzertyFr.print("Il y a ");
    KeyboardAzertyFr.print(counter);
    KeyboardAzertyFr.println(" trucs.");
    KeyboardAzertyFr.press(KEY_RETURN);
    delay(200);
    KeyboardAzertyFr.releaseAll();
  }
  // save the current button state for comparison next time:
  previousButton1State = button1State;

  //******************************
  // Function of the pushbutton 2: Trame de Mail
  int button2State = digitalRead(button2Pin);
  // if the button state has changed,
  if ((button2State != previousButton2State)
      // and it's currently pressed:
      && (button2State == HIGH)) {
    // type out a message
    KeyboardAzertyFr.print("Salut les gens !");
    KeyboardAzertyFr.press(KEY_RETURN);
    delay(50);
    KeyboardAzertyFr.releaseAll();
    KeyboardAzertyFr.press(KEY_RETURN);
    delay(50);
    KeyboardAzertyFr.releaseAll();
    KeyboardAzertyFr.press(KEY_RETURN);
    delay(50);
    KeyboardAzertyFr.releaseAll();
    KeyboardAzertyFr.press(KEY_RETURN);
    delay(50);
    KeyboardAzertyFr.releaseAll();
    KeyboardAzertyFr.press(KEY_RETURN);
    delay(50);
    KeyboardAzertyFr.releaseAll();
    KeyboardAzertyFr.press(KEY_RETURN);
    delay(50);
    KeyboardAzertyFr.releaseAll();
    KeyboardAzertyFr.print("Merci");
    KeyboardAzertyFr.press(KEY_RETURN);
    delay(50);
    KeyboardAzertyFr.releaseAll();        
    KeyboardAzertyFr.print("Amicalement");
    KeyboardAzertyFr.press(KEY_RETURN);
    delay(50);
    KeyboardAzertyFr.releaseAll();
    KeyboardAzertyFr.print("Lilian");
    KeyboardAzertyFr.press(KEY_RETURN);
    delay(50);
    KeyboardAzertyFr.releaseAll();    
    KeyboardAzertyFr.press(upKey); // Reour au niveau du message
    delay(50);
    KeyboardAzertyFr.releaseAll(); // x1
    KeyboardAzertyFr.press(upKey);
    delay(50);
    KeyboardAzertyFr.releaseAll();  // x2
    KeyboardAzertyFr.press(upKey);
    delay(50);
    KeyboardAzertyFr.releaseAll();
    KeyboardAzertyFr.press(upKey);  // x3
    delay(50);
    KeyboardAzertyFr.releaseAll();  // x4
    KeyboardAzertyFr.press(upKey);
    delay(50);
    KeyboardAzertyFr.releaseAll();  // x5
    KeyboardAzertyFr.press(upKey);
    delay(50);
    KeyboardAzertyFr.releaseAll();  // x6
  }
  // save the current button state for comparison next time:
  previousButton2State = button2State;

 //*******************************
 // Function of the pushbutton 3 : Copier une ligne entière
  int button3State = digitalRead(button3Pin);
  // if the button state has changed,
  if ((button3State != previousButton3State)
      // and it's currently pressed:
      && (button3State == HIGH)) {
    // type out a message
    KeyboardAzertyFr.press(KEY_HOME);
    delay(50);
    KeyboardAzertyFr.releaseAll();
    KeyboardAzertyFr.press(KEY_LEFT_SHIFT);
    KeyboardAzertyFr.press(KEY_END);
    delay(50);
    KeyboardAzertyFr.releaseAll();
    KeyboardAzertyFr.press(KEY_LEFT_CTRL);
    KeyboardAzertyFr.press('c');
    delay(50);
    KeyboardAzertyFr.releaseAll();
    KeyboardAzertyFr.press(KEY_END);
    delay(50);
    KeyboardAzertyFr.releaseAll();
    KeyboardAzertyFr.press(KEY_RETURN);
    delay(50);
    KeyboardAzertyFr.releaseAll();
    KeyboardAzertyFr.press(KEY_HOME);
    delay(50);
    KeyboardAzertyFr.releaseAll();
    KeyboardAzertyFr.press(KEY_LEFT_CTRL);
    KeyboardAzertyFr.press('v');
    delay(50);
    KeyboardAzertyFr.releaseAll();
  }
  // save the current button state for comparison next time:
  previousButton3State = button3State;
/*
 //*******************************
 // Function of the pushbutton 4 : Pour reseter la page
  int button4State = digitalRead(button4Pin);
  // if the button state has changed,
  if ((button4State != previousButton4State)
      // and it's currently pressed:
      && (button4State == HIGH)) {
    // increment the button counter
    counter2++;
    // type out a message
    Keyboard.press(KEY_END);
    Keyboard.print("_");
    Keyboard.print(counter2);
    delay(200);
    Keyboard.releaseAll();
    Keyboard.press(KEY_DOWN_ARROW);
    delay(200);
    Keyboard.releaseAll();
  }
  // save the current button state for comparison next time:
  previousButton4State = button4State;

 //*******************************
 // Function of the pushbutton 5 : Pour reseter la page
  int button5State = digitalRead(button5Pin);
  // if the button state has changed,
  if ((button5State != previousButton5State)
      // and it's currently pressed:
      && (button5State == HIGH)) {
    // type out a message
    counter = 0;
    counter2 = 0;
  }
  // save the current button state for comparison next time:
  previousButton5State = button5State;
*/      
}
