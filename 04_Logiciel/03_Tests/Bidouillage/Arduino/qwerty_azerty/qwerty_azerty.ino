

// ---------------------------------------------------
// nico78
// scancode keyboard qwerty et correspondance azerty
// ---------------------------------------------------
//  ' ' est le symbole qui désigne un espace, il a la valeur 44
//  Alt Gr azerty                   €                                                                    ~  #  {  [  |  `  \  ^  @    ' '  ]  }  ¤       
//   Shift azerty       Q  B  C  D  E  F  G  H  I  J  K  L  ?  N  O  P  A  R  S  T  U  V  Z  X  Y  Z  1  2  3  4  5  6  7  8  9  0    ' '  °  +  ¨  £  µ  No fr  M  %  NONE  .  /  §    >
//         azerty       q  b  c  d  e  f  g  h  i  j  k  l  ,  n  o  p  a  r  s  t  u  v  z  x  y  z  &  é  "  '  (  -  è  _  ç  à    ' '  )  =  ^  $  *  No fr  m  ù   ²    ;  :  !    <
//         qwerty       a  b  c  d  e  f  g  h  i  j  k  l  m  n  o  p  q  r  s  t  u  v  w  x  y  z  1  2  3  4  5  6  7  8  9  0    ' '  -  =  [  ]  \  No US  ;  '   `    ,  .  /   No US     
//       scancode       4, 5, 6, 7, 8, 9, 10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,  44, 45,46,47,48,49,  50,  51,52, 53,  54,55,56,  100};

#include <Keyboard.h>

void setup() {

  // put your setup code here, to run once:
  
  Keyboard.begin();
  delay(5000);

  for (int c=4; c<40; c++){
    keyboardScanCode(c);
  }

  // 50 ou 0x32 n'est pas utilisé sur notre clavier
  for (int c=44; c<50; c++){
    keyboardScanCode(c);
  }

  for (int c=51; c<57; c++){
    keyboardScanCode(c);
  }

  // Scan code pour la touche '< et >'
  keyboardScanCode(100);
  
  Keyboard.end();
}

void loop() {
  // put your main code here, to run repeatedly:
}

void keyboardScanCode(byte code){
  Keyboard.press(code+136);
  delay(5);
  Keyboard.release(code+136);
}

