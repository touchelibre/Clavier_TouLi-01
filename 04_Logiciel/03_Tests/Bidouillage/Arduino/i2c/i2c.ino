# include <Wire.h>

int led13=13;
    int tempo=1000 ;  

void setup() {
  int time_wait=0;
  // put your setup code here, to run once:

    pinMode(led13, OUTPUT);
    Serial.begin(9600);  // Init UART
    Wire.begin(); //Init I2C

// 40h: KSO SELECT REGISTER
  Wire.beginTransmission(0x38);//address the 7 high bits of the adress
  Wire.write(byte(0x40)); // register address
  Wire.write(byte(0xA0)); // register data
  Wire.endTransmission();

  Wire.beginTransmission(0x42);
  Wire.read();
  Wire.endTransmission();

/*
// 41h: KSO SELECT REGISTER
  Wire.beginTransmission(0x39);//address the 7 high bits of the adress
  Wire.write(byte(0x40)); // register address
  Wire.write(byte(0xA0)); // register data
  Wire.endTransmission();
*/

}

void loop()
{
  Wire.requestFrom(0x42, 6);    // request 6 bytes from slave device #2

  while(Wire.available())    // slave may send less than requested
  {
    char c = Wire.read();    // receive a byte as character
    Serial.print(c);         // print the character
  }

  delay(100);
/*
  // put your main code here, to run repeatedly:
  digitalWrite(led13, HIGH);
  delay(tempo);
  digitalWrite(led13, LOW);
  delay(tempo);
*/
}

