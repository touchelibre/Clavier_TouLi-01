  /*
  Keyboard Message test for the Arduino Leonardo and Micro.
  Sends a text string when a button is pressed.

  The circuit:


  created 24 Oct 2011 by Ardiuno Team
  modified 10 mars 2019  by Lilian Tribouilloy
*/

#include "Keyboard.h"

const int PinOut1 = 2;          // output pins for column 1
const int PinOut2 = 3;          // output pins for column 2
const int PinOut3 = 4;          // output pins for column 3
const int PinIn1 = 5;           // input pins for line 1 
const int PinIn2 = 6;           // input pins for line 2
const int PinIn3 = 7;           // input pins for line 3

int prevPinIn1State = LOW;      // for checking the input state
int prevPinIn2State = LOW;
int prevPinIn3State = LOW;


void setup() {
  // make the pins an output or input:
  pinMode(PinOut1, OUTPUT);
  pinMode(PinOut2, OUTPUT);
  pinMode(PinOut3, OUTPUT);
  pinMode(PinIN1, INPUT);  
  pinMode(PinIN1, INPUT);
  pinMode(PinIN1, INPUT);
    
  // initialize control over the keyboard:
  Keyboard.begin();
}

void loop() {
  
  // Séquence de la matrice
  
  int PinIn1State = digitalRead(button1Pin);
  // if the button state has changed,
  if ((button1State != previousButton1State) && (button1State == HIGH)) {
    // type out a message
    Keyboard.print("Il y a ");
    Keyboard.print(counter);
    Keyboard.println(" trucs.");
    Keyboard.press(KEY_RETURN);
    delay(200);
    Keyboard.releaseAll();
  }
  previousButton1State = button1State;   // save the current button state for comparison next time

  //******************************
  // Function of the pushbutton 2: Trame de Mail
  int button2State = digitalRead(button2Pin);
  // if the button state has changed,
  if ((button2State != previousButton2State)
      // and it's currently pressed:
      && (button2State == HIGH)) {
    // type out a message
    Keyboard.print("Salut les gens !");
    Keyboard.press(KEY_RETURN);
    delay(50);
    Keyboard.releaseAll();
    Keyboard.press(KEY_RETURN);
    delay(50);
    Keyboard.releaseAll();
    Keyboard.press(KEY_RETURN);
    delay(50);
    Keyboard.releaseAll();
    Keyboard.press(KEY_RETURN);
    delay(50);
    Keyboard.releaseAll();
    Keyboard.press(KEY_RETURN);
    delay(50);
    Keyboard.releaseAll();
    Keyboard.press(KEY_RETURN);
    delay(50);
    Keyboard.releaseAll();
    Keyboard.print("Merci");
    Keyboard.press(KEY_RETURN);
    delay(50);
    Keyboard.releaseAll();        
    Keyboard.print("Amicalement");
    Keyboard.press(KEY_RETURN);
    delay(50);
    Keyboard.releaseAll();
    Keyboard.print("Lilian");
    Keyboard.press(KEY_RETURN);
    delay(50);
    Keyboard.releaseAll();    
    Keyboard.press(upKey); // Reour au niveau du message
    delay(50);
    Keyboard.releaseAll(); // x1
    Keyboard.press(upKey);
    delay(50);
    Keyboard.releaseAll();  // x2
    Keyboard.press(upKey);
    delay(50);
    Keyboard.releaseAll();
    Keyboard.press(upKey);  // x3
    delay(50);
    Keyboard.releaseAll();  // x4
    Keyboard.press(upKey);
    delay(50);
    Keyboard.releaseAll();  // x5
    Keyboard.press(upKey);
    delay(50);
    Keyboard.releaseAll();  // x6
  }
  // save the current button state for comparison next time:
  previousButton2State = button2State;

 //*******************************
 // Function of the pushbutton 3 : Copier une ligne entière
  int button3State = digitalRead(button3Pin);
  // if the button state has changed,
  if ((button3State != previousButton3State)
      // and it's currently pressed:
      && (button3State == HIGH)) {
    // type out a message
    Keyboard.press(KEY_HOME);
    delay(50);
    Keyboard.releaseAll();
    Keyboard.press(KEY_LEFT_SHIFT);
    Keyboard.press(KEY_END);
    delay(50);
    Keyboard.releaseAll();
    Keyboard.press(KEY_LEFT_CTRL);
    Keyboard.press('c');
    delay(50);
    Keyboard.releaseAll();
    Keyboard.press(KEY_END);
    delay(50);
    Keyboard.releaseAll();
    Keyboard.press(KEY_RETURN);
    delay(50);
    Keyboard.releaseAll();
    Keyboard.press(KEY_HOME);
    delay(50);
    Keyboard.releaseAll();
    Keyboard.press(KEY_LEFT_CTRL);
    Keyboard.press('v');
    delay(50);
    Keyboard.releaseAll();
  }
  // save the current button state for comparison next time:
  previousButton3State = button3State;

 //*******************************
 // Function of the pushbutton 4 : Pour reseter la page
  int button4State = digitalRead(button4Pin);
  // if the button state has changed,
  if ((button4State != previousButton4State)
      // and it's currently pressed:
      && (button4State == HIGH)) {
    // increment the button counter
    counter2++;
    // type out a message
    Keyboard.press(KEY_END);
    Keyboard.print("_");
    Keyboard.print(counter2);
    delay(200);
    Keyboard.releaseAll();
    Keyboard.press(KEY_DOWN_ARROW);
    delay(200);
    Keyboard.releaseAll();
  }
  // save the current button state for comparison next time:
  previousButton4State = button4State;

 //*******************************
 // Function of the pushbutton 5 : Pour reseter la page
  int button5State = digitalRead(button5Pin);
  // if the button state has changed,
  if ((button5State != previousButton5State)
      // and it's currently pressed:
      && (button5State == HIGH)) {
    // type out a message
    counter = 0;
    counter2 = 0;
  }
  // save the current button state for comparison next time:
  previousButton5State = button5State;
      
}
