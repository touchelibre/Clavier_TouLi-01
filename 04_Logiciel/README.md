[ToucheLibre project](http://touchelibre.fr)

![Logo](http://touchelibre.fr/wp-content/uploads/2019/03/Icon_ToucheLibre_V3.png)


# Partie logiciel

Ce dossier contient l’ensemble de l’étude de conception sur la partie logiciel du clavier TouLi, ainsi que l’ensemble du code source.


## 00_Spec_Logiciel

Documentation sur l’architecture logiciel et autre documentation technique de conception.


## 01_Pilotes

Les pilotes des dispositions de touches pour les différents système d’explotation.


## 02_Code

Conception détaillé et code source du clavier.


## 03_Tests

Tests de débeug et validation fonctionnelle.


