[ToucheLibre project](http://touchelibre.fr)

![Logo](http://touchelibre.fr/wp-content/uploads/2019/03/Icon_ToucheLibre_V3.png)


# Partie Électronique

## 00_Justification

Étude de conception et justification théorique de bon fonctionnement.


## 01_Schéma_Routage

Conception détaillé du circuit électronique avec KiCAD. Schéma et Routage.


## 02_Nomo

Liste détaillé des composants utilisés dans le circuit.


## 03_Gerber

Résultat du travail de conception. Fichiers prềts pour fabrication.


## 04_Tests

Débeug et test de vérification de bon fonctionnement.


## 05_DFMEA

Étude pour montrée la durée de vie du produit. Déterminer les points faible du point de vu de la fiabilité et chercher comment améliorer ces faiblesses.


