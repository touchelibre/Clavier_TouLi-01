EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:switches
LIBS:relays
LIBS:motors
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:carte_fleche_gauche-cache
EELAYER 25 0
EELAYER END
$Descr A2 23386 16535
encoding utf-8
Sheet 1 2
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Sheet
S 14550 13550 2900 1700
U 5BBF9DAA
F0 "touche_fleche" 60
F1 "touche_fleche.sch" 60
$EndSheet
Text Notes 9500 1450 0    315  ~ 63
Page de Suivi   /   Tracking Sheet
Wire Notes Line
	5400 2800 14450 2800
Wire Notes Line
	14450 6100 14450 2800
Wire Notes Line
	5400 6100 14450 6100
Wire Notes Line
	5400 6100 5400 2800
$EndSCHEMATC
