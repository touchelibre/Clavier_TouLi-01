EESchema Schematic File Version 4
LIBS:carte_clavier-rescue
LIBS:carte_clavier-cache
EELAYER 26 0
EELAYER END
$Descr A2 23386 16535
encoding utf-8
Sheet 1 5
Title "Clavier ToucheLibre   /   ToucheLibre Keyboard"
Date "16 juin 2018"
Rev "V0.0"
Comp "Lyliberté"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 "Page de Suivi   /   Tracking Sheet"
$EndDescr
Text Notes 6900 1050 0    315  ~ 63
Page de Suivi   /   Tracking Sheet
$Sheet
S 15700 14800 2450 850 
U 5B24D131
F0 "page_hierarchique" 79
F1 "page_hierarchique.sch" 79
$EndSheet
Wire Notes Line
	1850 2150 9350 2150
Wire Notes Line
	9350 2150 9350 5550
Wire Notes Line
	9350 5550 1850 5550
Wire Notes Line
	1850 5550 1850 2150
$EndSCHEMATC
