EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:switches
LIBS:relays
LIBS:motors
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:carte_fleche_droite-cache
EELAYER 25 0
EELAYER END
$Descr A2 23386 16535
encoding utf-8
Sheet 2 2
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L ML1A-11JW J?
U 1 1 5BA7F54F
P 10300 9100
F 0 "J?" H 10900 9850 60  0000 C CNN
F 1 "ML1A-11JW" H 11100 9250 60  0000 C CNN
F 2 "" H 10900 9850 60  0000 C CNN
F 3 "" H 10900 9850 60  0000 C CNN
	1    10300 9100
	1    0    0    -1  
$EndComp
$Comp
L ML1A-11JW J?
U 1 1 5BA7F5E6
P 11850 7700
F 0 "J?" H 12450 8450 60  0000 C CNN
F 1 "ML1A-11JW" H 12650 7850 60  0000 C CNN
F 2 "" H 12450 8450 60  0000 C CNN
F 3 "" H 12450 8450 60  0000 C CNN
	1    11850 7700
	1    0    0    -1  
$EndComp
$Comp
L ML1A-11JW J?
U 1 1 5BA7F67E
P 11850 10350
F 0 "J?" H 12450 11100 60  0000 C CNN
F 1 "ML1A-11JW" H 12650 10500 60  0000 C CNN
F 2 "" H 12450 11100 60  0000 C CNN
F 3 "" H 12450 11100 60  0000 C CNN
	1    11850 10350
	1    0    0    -1  
$EndComp
$Comp
L ML1A-11JW J?
U 1 1 5BA7F6AB
P 13700 9100
F 0 "J?" H 14300 9850 60  0000 C CNN
F 1 "ML1A-11JW" H 14500 9250 60  0000 C CNN
F 2 "" H 14300 9850 60  0000 C CNN
F 3 "" H 14300 9850 60  0000 C CNN
	1    13700 9100
	1    0    0    -1  
$EndComp
$Comp
L ML1A-11JW J?
U 1 1 5BA7F6E0
P 16500 10400
F 0 "J?" H 17100 11150 60  0000 C CNN
F 1 "ML1A-11JW" H 17300 10550 60  0000 C CNN
F 2 "" H 17100 11150 60  0000 C CNN
F 3 "" H 17100 11150 60  0000 C CNN
	1    16500 10400
	1    0    0    -1  
$EndComp
$Comp
L CONN_01X10 P?
U 1 1 5BA7F8B8
P 7750 7450
F 0 "P?" V 7900 7150 50  0000 C CNN
F 1 "CONN_01X10" V 8000 7350 50  0000 C CNN
F 2 "" H 7750 7450 50  0000 C CNN
F 3 "" H 7750 7450 50  0000 C CNN
	1    7750 7450
	0    -1   1    0   
$EndComp
$Comp
L D D?
U 1 1 5BA7FBB0
P 12500 7900
F 0 "D?" H 12500 8000 50  0000 C CNN
F 1 "D" H 12500 7800 50  0000 C CNN
F 2 "" H 12500 7900 50  0001 C CNN
F 3 "" H 12500 7900 50  0001 C CNN
	1    12500 7900
	-1   0    0    1   
$EndComp
$Comp
L D D?
U 1 1 5BA7FC8A
P 10950 9300
F 0 "D?" H 10950 9400 50  0000 C CNN
F 1 "D" H 10950 9200 50  0000 C CNN
F 2 "" H 10950 9300 50  0001 C CNN
F 3 "" H 10950 9300 50  0001 C CNN
	1    10950 9300
	-1   0    0    1   
$EndComp
$Comp
L D D?
U 1 1 5BA7FCE3
P 12500 10500
F 0 "D?" H 12500 10600 50  0000 C CNN
F 1 "D" H 12500 10400 50  0000 C CNN
F 2 "" H 12500 10500 50  0001 C CNN
F 3 "" H 12500 10500 50  0001 C CNN
	1    12500 10500
	-1   0    0    1   
$EndComp
$Comp
L D D?
U 1 1 5BA7FD2C
P 14350 9300
F 0 "D?" H 14350 9400 50  0000 C CNN
F 1 "D" H 14350 9200 50  0000 C CNN
F 2 "" H 14350 9300 50  0001 C CNN
F 3 "" H 14350 9300 50  0001 C CNN
	1    14350 9300
	-1   0    0    1   
$EndComp
$Comp
L D D?
U 1 1 5BA7FD9F
P 17200 10600
F 0 "D?" H 17200 10700 50  0000 C CNN
F 1 "D" H 17200 10500 50  0000 C CNN
F 2 "" H 17200 10600 50  0001 C CNN
F 3 "" H 17200 10600 50  0001 C CNN
	1    17200 10600
	-1   0    0    1   
$EndComp
$Comp
L GND #PWR?
U 1 1 5BA7FDD3
P 7150 7500
F 0 "#PWR?" H 7150 7250 50  0001 C CNN
F 1 "GND" H 7150 7350 50  0000 C CNN
F 2 "" H 7150 7500 50  0001 C CNN
F 3 "" H 7150 7500 50  0001 C CNN
	1    7150 7500
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR?
U 1 1 5BA7FE26
P 8350 7500
F 0 "#PWR?" H 8350 7250 50  0001 C CNN
F 1 "GND" H 8350 7350 50  0000 C CNN
F 2 "" H 8350 7500 50  0001 C CNN
F 3 "" H 8350 7500 50  0001 C CNN
	1    8350 7500
	1    0    0    -1  
$EndComp
Wire Wire Line
	7300 7250 7300 7200
Wire Wire Line
	7300 7200 7150 7200
Wire Wire Line
	7150 7200 7150 7500
Wire Wire Line
	8200 7250 8200 7200
Wire Wire Line
	8200 7200 8350 7200
Wire Wire Line
	8350 7200 8350 7500
Wire Wire Line
	7400 7250 7400 7050
Wire Wire Line
	7400 7050 6650 7050
Wire Wire Line
	7500 7250 7500 6900
Wire Wire Line
	7500 6900 6650 6900
Wire Wire Line
	7600 6250 7600 7250
Wire Wire Line
	7700 6350 7700 7250
Wire Wire Line
	7800 6450 7800 7250
Wire Wire Line
	7900 6550 7900 7250
Wire Wire Line
	8000 7250 8000 6650
Wire Wire Line
	8100 7250 8100 7100
Wire Wire Line
	8100 7100 8350 7100
Wire Wire Line
	8350 7100 8350 6950
$Comp
L VCC #PWR?
U 1 1 5BA80365
P 8350 6950
F 0 "#PWR?" H 8350 6800 50  0001 C CNN
F 1 "VCC" H 8350 7100 50  0000 C CNN
F 2 "" H 8350 6950 50  0001 C CNN
F 3 "" H 8350 6950 50  0001 C CNN
	1    8350 6950
	1    0    0    -1  
$EndComp
Text Label 6650 7050 0    60   ~ 0
LED_Arrow
Text Label 6650 6900 0    60   ~ 0
row_Arrow
Text Label 8350 6250 2    60   ~ 0
col_10
Wire Wire Line
	7600 6250 8350 6250
Wire Wire Line
	7700 6350 8350 6350
Wire Wire Line
	7800 6450 8350 6450
Wire Wire Line
	7900 6550 8350 6550
Wire Wire Line
	8000 6650 8350 6650
Text Label 8350 6350 2    60   ~ 0
col_11
Text Label 8350 6450 2    60   ~ 0
col_12
Text Label 8350 6550 2    60   ~ 0
col_13
Text Label 8350 6650 2    60   ~ 0
col_14
Wire Wire Line
	17050 10600 16900 10600
Wire Wire Line
	16900 10600 16900 10350
Wire Wire Line
	15450 9900 16550 9900
Wire Wire Line
	16500 9900 16500 9400
Wire Wire Line
	16500 9400 16900 9400
Wire Wire Line
	16900 9400 16900 9550
Connection ~ 16500 9900
Wire Wire Line
	9250 8600 10350 8600
Wire Wire Line
	10300 8600 10300 8100
Wire Wire Line
	10300 8100 10700 8100
Wire Wire Line
	10700 8100 10700 8250
Connection ~ 10300 8600
Wire Wire Line
	11800 8600 11050 8600
Wire Wire Line
	11800 6800 11800 9850
Wire Wire Line
	11800 6800 12250 6800
Wire Wire Line
	12250 6800 12250 6850
Connection ~ 11800 7200
Wire Wire Line
	13600 8600 13750 8600
Wire Wire Line
	14100 8250 14100 8200
Wire Wire Line
	14100 8200 13600 8200
Connection ~ 13600 8200
Wire Wire Line
	11800 9450 12250 9450
Wire Wire Line
	12250 9450 12250 9500
Wire Wire Line
	10800 9300 10700 9300
Wire Wire Line
	10700 9300 10700 9050
Wire Wire Line
	12350 7900 12250 7900
Wire Wire Line
	12250 7900 12250 7600
Wire Wire Line
	14100 9300 14100 9050
Wire Wire Line
	12250 10500 12350 10500
Wire Wire Line
	12250 10500 12250 10300
Wire Wire Line
	12600 7200 13600 7200
Wire Wire Line
	13600 7200 13600 8600
NoConn ~ 12600 9850
Connection ~ 11800 9450
Connection ~ 11800 8600
Wire Wire Line
	11900 7200 11800 7200
Wire Wire Line
	11800 9850 11900 9850
Wire Wire Line
	14200 9300 14100 9300
Wire Wire Line
	17350 10600 17850 10600
Text Label 17850 10600 2    60   ~ 0
col_14
Wire Wire Line
	11100 9300 11600 9300
Text Label 11600 9300 2    60   ~ 0
col_10
Wire Wire Line
	12650 7900 13150 7900
Text Label 13150 7900 2    60   ~ 0
col_11
Wire Wire Line
	12650 10500 13150 10500
Text Label 13150 10500 2    60   ~ 0
col_12
Wire Wire Line
	14500 9300 15000 9300
Text Label 15000 9300 2    60   ~ 0
col_13
Text Label 9250 8600 0    60   ~ 0
row_Arrow
Wire Wire Line
	15450 9900 15450 8600
Wire Wire Line
	15450 8600 14450 8600
NoConn ~ 17250 9900
$EndSCHEMATC
