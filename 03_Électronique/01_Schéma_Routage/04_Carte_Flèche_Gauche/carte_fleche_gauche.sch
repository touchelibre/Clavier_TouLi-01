EESchema Schematic File Version 4
LIBS:carte_fleche_gauche-cache
EELAYER 30 0
EELAYER END
$Descr A2 23386 16535
encoding utf-8
Sheet 1 2
Title "Schematics"
Date "20 oct 2019"
Rev "B"
Comp "Lilian T."
Comment1 "TL 500 004"
Comment2 "Clavier ToucheLibre"
Comment3 "Left Arrows Board"
Comment4 "Tracking Sheet"
$EndDescr
$Sheet
S 15650 14600 1700 650 
U 5BBF9DAA
F0 "touche_fleche" 60
F1 "touche_fleche.sch" 60
$EndSheet
Wire Notes Line style solid
	2500 3000 10200 3000
Wire Notes Line style solid
	2500 3000 2500 7500
Wire Notes Line style solid
	2500 3300 10200 3300
Text Notes 5000 3200 0    79   ~ 16
Core Design Tracking
Wire Notes Line style solid
	2500 4500 10200 4500
Wire Notes Line style solid
	2900 4200 2900 7500
Text Notes 2600 4400 0    79   ~ 16
Rev.
Text Notes 5400 4400 0    79   ~ 16
Description
Wire Notes Line style solid
	9200 4200 9200 7500
Wire Notes Line style solid
	10200 3000 10200 7500
Wire Notes Line style solid
	2500 7500 10200 7500
Text Notes 9550 4400 0    79   ~ 16
Date
Text Notes 6400 3200 0    59   Italic 0
(Modification of all the variants or layout impact)
Wire Notes Line style solid
	2500 4900 10200 4900
Wire Notes Line style solid
	2500 5450 10200 5450
Text Notes 2650 4750 0    79   ~ 0
A
Text Notes 3100 4750 0    79   ~ 0
Creation of schematics. Before layout.
Text Notes 9300 4750 0    79   ~ 0
25 aout 2019
Wire Notes Line style solid
	13000 3000 20700 3000
Wire Notes Line style solid
	13000 3000 13000 7500
Wire Notes Line style solid
	13000 3300 20700 3300
Text Notes 13600 3200 0    79   ~ 16
V1 Variant Tracking : Analogic + Without “Cursor / Page” Toggle
Wire Notes Line style solid
	13000 4500 20700 4500
Wire Notes Line style solid
	13400 4200 13400 7500
Text Notes 13100 4400 0    79   ~ 16
Rev.
Text Notes 15900 4400 0    79   ~ 16
Description
Wire Notes Line style solid
	19700 4200 19700 7500
Wire Notes Line style solid
	20700 3000 20700 7500
Wire Notes Line style solid
	13000 7500 20700 7500
Text Notes 20050 4400 0    79   ~ 16
Date
Text Notes 18100 3200 0    59   Italic 0
(Modification of this variant on bill of material)
Wire Notes Line style solid
	13000 4900 20700 4900
Wire Notes Line style solid
	13000 5450 20700 5450
Text Notes 13100 4750 0    79   ~ 0
A00
Text Notes 13600 4750 0    79   ~ 0
Creation of schematics. Before layout.
Text Notes 19800 4750 0    79   ~ 0
25 aout 2019
Wire Notes Line style solid
	2500 4200 10200 4200
Text Notes 2650 3950 0    79   ~ 0
List of the\nAlways Not\nMounted\nComponents
Wire Notes Line style solid
	3500 3300 3500 4200
Wire Notes Line style solid
	13000 4200 20700 4200
Text Notes 13150 3950 0    79   ~ 0
List of the\nComponents\nunder this\nvariant
Wire Notes Line style solid
	14000 3300 14000 4200
Wire Notes Line width 20 style solid
	500  1500 22900 1500
Text Notes 2650 1200 0    315  ~ 63
Schéma Électronique du Clavier ToucheLibre     ;     Carte Flèche Gauche
Text Notes 18000 14450 0    59   ~ 12
Remarques Générales :\nSauf indication contraire, les résistances sont en 1%, boitier 0402 / 1005M, soit 63mW.
Text Notes 3700 3850 0    79   ~ 0
ANM >  Empty\n\n(All the tagged components with CD (Core Design) are mounted for all the variants.)
Text Notes 14250 4050 0    79   ~ 0
G1 = Mounted     >  Q42001, Q42003, R42001-R42003, R42005\nG2 = Not Mounted >  D42001-D42003, D42005\nG3 = Not Mounted >  SW42005\nG4 = Not Mounted >  Q42002, R42004\nG5 = Not Mounted >  D42004
Wire Notes Line style solid
	13000 9000 20700 9000
Wire Notes Line style solid
	13000 9000 13000 13500
Wire Notes Line style solid
	13000 9300 20700 9300
Text Notes 13600 9200 0    79   ~ 16
V2 Variant Tracking : Analogic + WITH “Cursor / Page” Toggle
Wire Notes Line style solid
	13000 10500 20700 10500
Wire Notes Line style solid
	13400 10200 13400 13500
Text Notes 13100 10400 0    79   ~ 16
Rev.
Text Notes 15900 10400 0    79   ~ 16
Description
Wire Notes Line style solid
	19700 10200 19700 13500
Wire Notes Line style solid
	20700 9000 20700 13500
Wire Notes Line style solid
	13000 13500 20700 13500
Text Notes 20050 10400 0    79   ~ 16
Date
Text Notes 18050 9200 0    59   Italic 0
(Modification of this variant on bill of material)
Wire Notes Line style solid
	13000 10900 20700 10900
Wire Notes Line style solid
	13000 11450 20700 11450
Text Notes 13100 10750 0    79   ~ 0
A00
Text Notes 13600 10750 0    79   ~ 0
Creation of schematics. Before layout.
Text Notes 19800 10750 0    79   ~ 0
25 aout 2019
Wire Notes Line style solid
	13000 10200 20700 10200
Text Notes 13150 9950 0    79   ~ 0
List of the\nComponents\nunder this\nvariant
Wire Notes Line style solid
	14000 9300 14000 10200
Text Notes 14250 10050 0    79   ~ 0
G1 = Mounted     >  Q42001, Q42003, R42001-R42003, R42005\nG2 = Not Mounted >  D42001-D42003, D42005\nG3 = Mounted     >  SW42005\nG4 = Mounted     >  Q42002, R42004\nG5 = Not Mounted >  D42004
Text Notes 3100 5350 0    79   ~ 0
Modification to facilitate the layout:\n   * J42001 = reverse footprint issue.\n   * J42001 = modification of pin-out to get easy layout.
Text Notes 2650 5150 0    79   ~ 0
B
Text Notes 9300 5150 0    79   ~ 0
20 oct 2019
Text Notes 13600 5350 0    79   ~ 0
Modification to facilitate the layout:\n   * J42001 = reverse footprint issue.\n   * J42001 = modification of pin-out to get easy layout.
Text Notes 13100 5150 0    79   ~ 0
B00
Text Notes 19800 5150 0    79   ~ 0
20 oct 2019
Text Notes 13600 11350 0    79   ~ 0
Modification to facilitate the layout:\n   * J42001 = reverse footprint issue.\n   * J42001 = modification of pin-out to get easy layout.
Text Notes 13100 11150 0    79   ~ 0
B00
Text Notes 19800 11150 0    79   ~ 0
20 oct 2019
$EndSCHEMATC
