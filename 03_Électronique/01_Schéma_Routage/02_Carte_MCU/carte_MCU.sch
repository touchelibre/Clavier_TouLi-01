EESchema Schematic File Version 4
LIBS:carte_MCU-cache
EELAYER 30 0
EELAYER END
$Descr A2 23386 16535
encoding utf-8
Sheet 1 5
Title "Schematics"
Date "20 oct 2019"
Rev "B"
Comp "Lilian T."
Comment1 "TL 500 002"
Comment2 "Clavier ToucheLibre"
Comment3 "MCU Board"
Comment4 "Tracking Sheet"
$EndDescr
$Sheet
S 15900 14600 1450 650 
U 5D4A0749
F0 "Connector_Hierarchy" 50
F1 "Connector_Hierarchy.sch" 50
$EndSheet
Wire Notes Line style solid
	2500 3000 10200 3000
Wire Notes Line style solid
	2500 3000 2500 7500
Wire Notes Line style solid
	2500 3300 10200 3300
Text Notes 5000 3200 0    79   ~ 16
Core Design Tracking
Wire Notes Line style solid
	2500 4500 10200 4500
Wire Notes Line style solid
	2900 4200 2900 7500
Text Notes 2600 4400 0    79   ~ 16
Rev.
Text Notes 5400 4400 0    79   ~ 16
Description
Wire Notes Line style solid
	9200 4200 9200 7500
Wire Notes Line style solid
	10200 3000 10200 7500
Wire Notes Line style solid
	2500 7500 10200 7500
Text Notes 9550 4400 0    79   ~ 16
Date
Text Notes 6400 3200 0    59   Italic 0
(Modification of all the variants or layout impact)
Wire Notes Line style solid
	2500 4900 10200 4900
Wire Notes Line style solid
	2500 6150 10200 6150
Text Notes 2650 4750 0    79   ~ 0
A
Text Notes 3100 4750 0    79   ~ 0
Creation of schematics. Before Layout.
Text Notes 9300 4750 0    79   ~ 0
25 aout 2019
Wire Notes Line style solid
	13000 3000 20700 3000
Wire Notes Line style solid
	13000 3000 13000 7500
Wire Notes Line style solid
	13000 3300 20700 3300
Text Notes 13650 3200 0    79   ~ 16
V1 Variant Tracking : USB + Bluetooth + Analogic Matrix
Wire Notes Line style solid
	13000 4500 20700 4500
Wire Notes Line style solid
	13400 4200 13400 7500
Text Notes 13100 4400 0    79   ~ 16
Rev.
Text Notes 15900 4400 0    79   ~ 16
Description
Wire Notes Line style solid
	19700 4200 19700 7500
Wire Notes Line style solid
	20700 3000 20700 7500
Wire Notes Line style solid
	13000 7500 20700 7500
Text Notes 20050 4400 0    79   ~ 16
Date
Text Notes 17800 3200 0    59   Italic 0
(Modification of this variant on bill of material)
Wire Notes Line style solid
	13000 4900 20700 4900
Wire Notes Line style solid
	13000 6150 20700 6150
Text Notes 13100 4750 0    79   ~ 0
A00
Text Notes 13600 4750 0    79   ~ 0
Creation of schematics. Before Layout.
Text Notes 19800 4750 0    79   ~ 0
25 aout 2019
Wire Notes Line style solid
	2500 4200 10200 4200
Text Notes 2650 3950 0    79   ~ 0
List of the\nAlways Not\nMounted\nComponents
Wire Notes Line style solid
	3500 3300 3500 4200
Wire Notes Line style solid
	13000 4200 20700 4200
Text Notes 13150 3950 0    79   ~ 0
List of the\nComponents\nunder this\nvariant
Wire Notes Line style solid
	14000 3300 14000 4200
Wire Notes Line width 20 style solid
	500  1500 22900 1500
Text Notes 3400 1200 0    315  ~ 63
Schéma Électronique du Clavier ToucheLibre     ;     Carte MCU
Text Notes 17950 14450 0    59   ~ 12
Remarques Générales :\nSauf indication contraire, les résistances sont en 1%, boitier 0402 / 1005M, soit 63mW.
Text Notes 3600 3750 0    79   ~ 0
ANM >  AE25001, TP24001-TP24004, TP24008, TP24025-TP24027\n\n(All the tagged components with CD (Core Design) are mounted for all the variants.)
Text Notes 14100 4100 0    79   ~ 0
G1 = Mounted     >  D23001, R24004-R24005\nG2 = Mounted     >  C25001-C25013, D22001, D22002, D23002, FB25001-FB25007, \n                       L25001-L25003, R25001, R25003, U25001, Y25001\nG3 = Not Mounted >  D24001, D24002, R24008, R24009\nG4 = Mounted     >  R24010, R24011\nG5 = Mounted     >  C24013-C24020, R24012-R24019
Wire Notes Line style solid
	2500 9000 10200 9000
Wire Notes Line style solid
	2500 9000 2500 13500
Wire Notes Line style solid
	2500 9300 10200 9300
Text Notes 3150 9200 0    79   ~ 16
V2 Variant Tracking : USB Only + Analogic Matrix
Wire Notes Line style solid
	2500 10500 10200 10500
Wire Notes Line style solid
	2900 10200 2900 13500
Text Notes 2600 10400 0    79   ~ 16
Rev.
Text Notes 5400 10400 0    79   ~ 16
Description
Wire Notes Line style solid
	9200 10200 9200 13500
Wire Notes Line style solid
	10200 9000 10200 13500
Wire Notes Line style solid
	2500 13500 10200 13500
Text Notes 9550 10400 0    79   ~ 16
Date
Text Notes 7300 9200 0    59   Italic 0
(Modification of this variant on bill of material)
Wire Notes Line style solid
	2500 10900 10200 10900
Wire Notes Line style solid
	2500 12150 10200 12150
Text Notes 2600 10750 0    79   ~ 0
A00
Text Notes 3100 10750 0    79   ~ 0
Creation of schematics. Before Layout.
Text Notes 9300 10750 0    79   ~ 0
25 aout 2019
Wire Notes Line style solid
	2500 10200 10200 10200
Text Notes 2650 9950 0    79   ~ 0
List of the\nComponents\nunder this\nvariant
Wire Notes Line style solid
	3500 9300 3500 10200
Text Notes 3600 10100 0    79   ~ 0
G1 = Mounted     >  D23001, R24004-R24005\nG2 = Not Mounted >  C25001-C25013, D22001, D22002, D23002, FB25001-FB25007, \n                       L25001-L25003, R25001, R25003, U25001, Y25001\nG3 = Not Mounted >  D24001, D24002, R24008, R24009\nG4 = Not Mounted >  R24010, R24011\nG5 = Mounted     >  C24013-C24020, R24012-R24019
Wire Notes Line style solid
	13000 9000 20700 9000
Wire Notes Line style solid
	13000 9000 13000 13500
Wire Notes Line style solid
	13000 9300 20700 9300
Text Notes 13650 9200 0    79   ~ 16
V3 Variant Tracking : Bluetooth Only + Analogic Matrix
Wire Notes Line style solid
	13000 10500 20700 10500
Wire Notes Line style solid
	13400 10200 13400 13500
Text Notes 13100 10400 0    79   ~ 16
Rev.
Text Notes 15900 10400 0    79   ~ 16
Description
Wire Notes Line style solid
	19700 10200 19700 13500
Wire Notes Line style solid
	20700 9000 20700 13500
Wire Notes Line style solid
	13000 13500 20700 13500
Text Notes 20050 10400 0    79   ~ 16
Date
Text Notes 17800 9200 0    59   Italic 0
(Modification of this variant on bill of material)
Wire Notes Line style solid
	13000 10900 20700 10900
Wire Notes Line style solid
	13000 12150 20700 12150
Text Notes 13100 10750 0    79   ~ 0
A00
Text Notes 13600 10750 0    79   ~ 0
Creation of schematics. Before Layout.
Text Notes 19800 10750 0    79   ~ 0
25 aout 2019
Wire Notes Line style solid
	13000 10200 20700 10200
Text Notes 13150 9950 0    79   ~ 0
List of the\nComponents\nunder this\nvariant
Wire Notes Line style solid
	14000 9300 14000 10200
Text Notes 14100 10100 0    79   ~ 0
G1 = Not Mounted >  D23001, R24004-R24005\nG2 = Mounted     >  C25001-C25013, D22001, D22002, D23002, FB25001-FB25007, \n                       L25001-L25003, R25001, R25003, U25001, Y25001\nG3 = Not Mounted >  D24001, D24002, R24008, R24009\nG4 = Mounted     >  R24010, R24011\nG5 = Mounted     >  C24013-C24020, R24012-R24019
Text Notes 2650 5150 0    79   ~ 0
B
Text Notes 9300 5100 0    79   ~ 0
20 oct 2019
Text Notes 3050 6050 0    79   ~ 0
Modification to facilitate the layout:\n   * J22005 = Modification of pin-out\n   * The net USB_Dx change to USB2_N or _P, to be coherente to other boards.\n   * Add C22004 = 220nF on ON-OFF_2 to EMC.\n   * Add  C25013 to antenna mismatch.\n   * Add a GND pin on antenna AE25001.\n   * Add pin 21 in schematic part of nRF24L01P.\n   * R25002 is deleted to get better bulk.\n   * TP24005 to TP24007 and TP24009 to TP24024 are deleted.
Text Notes 13600 6050 0    79   ~ 0
Modification to facilitate the layout:\n   * J22005 = Modification of pin-out\n   * The net USB_Dx change to USB2_N or _P, to be coherente to other boards.\n   * Add C22004 = 220nF on ON-OFF_2 to EMC.\n   * Add  C25013 to antenna mismatch.\n   * Add a GND pin on antenna AE25001.\n   * Add pin 21 in schematic part of nRF24L01P.\n   * R25002 is deleted to get better bulk.\n   * TP24005 to TP24007 and TP24009 to TP24024 are deleted.
Text Notes 13600 12050 0    79   ~ 0
Modification to facilitate the layout:\n   * J22005 = Modification of pin-out\n   * The net USB_Dx change to USB2_N or _P, to be coherente to other boards.\n   * Add C22004 = 220nF on ON-OFF_2 to EMC.\n   * Add  C25013 to antenna mismatch.\n   * Add a GND pin on antenna AE25001.\n   * Add pin 21 in schematic part of nRF24L01P.\n   * R25002 is deleted to get better bulk.\n   * TP24005 to TP24007 and TP24009 to TP24024 are deleted.
Text Notes 3150 12050 0    79   ~ 0
Modification to facilitate the layout:\n   * J22005 = Modification of pin-out\n   * The net USB_Dx change to USB2_N or _P, to be coherente to other boards.\n   * Add C22004 = 220nF on ON-OFF_2 to EMC.\n   * Add  C25013 to antenna mismatch.\n   * Add a GND pin on antenna AE25001.\n   * Add pin 21 in schematic part of nRF24L01P.\n   * R25002 is deleted to get better bulk.\n   * TP24005 to TP24007 and TP24009 to TP24024 are deleted.
Text Notes 2600 11100 0    79   ~ 0
B00
Text Notes 9300 11150 0    79   ~ 0
20 oct 2019
Text Notes 13100 11150 0    79   ~ 0
B00
Text Notes 19800 11100 0    79   ~ 0
20 oct 2019
Text Notes 13100 5150 0    79   ~ 0
B00
Text Notes 19800 5150 0    79   ~ 0
20 oct 2019
$EndSCHEMATC
