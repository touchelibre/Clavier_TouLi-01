EESchema Schematic File Version 4
LIBS:carte_coco_simple-cache
EELAYER 30 0
EELAYER END
$Descr A2 23386 16535
encoding utf-8
Sheet 1 3
Title "Schematics"
Date "20 oct 2019"
Rev "B"
Comp "Lilian T."
Comment1 "TL 500 003"
Comment2 "Clavier ToucheLibre"
Comment3 "CoCo Board"
Comment4 "Tracking Sheet"
$EndDescr
$Sheet
S 15650 14600 1700 650 
U 5BBF869B
F0 "Connector_Hierarchy" 60
F1 "Connector_Hierarchy.sch" 60
$EndSheet
Wire Notes Line style solid
	2500 3000 10200 3000
Wire Notes Line style solid
	2500 3000 2500 7500
Wire Notes Line style solid
	2500 3300 10200 3300
Text Notes 5000 3200 0    79   ~ 16
Core Design Tracking
Wire Notes Line style solid
	2500 4500 10200 4500
Wire Notes Line style solid
	2900 4200 2900 7500
Text Notes 2600 4400 0    79   ~ 16
Rev.
Text Notes 5400 4400 0    79   ~ 16
Description
Wire Notes Line style solid
	9200 4200 9200 7500
Wire Notes Line style solid
	10200 3000 10200 7500
Wire Notes Line style solid
	2500 7500 10200 7500
Text Notes 9550 4400 0    79   ~ 16
Date
Text Notes 6400 3200 0    59   Italic 0
(Modification of all the variants or layout impact)
Wire Notes Line style solid
	2500 4900 10200 4900
Wire Notes Line style solid
	2500 6650 10200 6650
Text Notes 2650 4750 0    79   ~ 0
A
Text Notes 3100 4750 0    79   ~ 0
Creation of schematics. Before layout.
Text Notes 9300 4750 0    79   ~ 0
25 aout 2019
Wire Notes Line style solid
	2500 9000 10200 9000
Wire Notes Line style solid
	2500 9000 2500 13500
Wire Notes Line style solid
	2500 9300 10200 9300
Text Notes 3300 9200 0    79   ~ 16
V2 Variant Tracking : USB Only + Selection Switches
Wire Notes Line style solid
	2500 10500 10200 10500
Wire Notes Line style solid
	2900 10200 2900 13500
Text Notes 2600 10400 0    79   ~ 16
Rev.
Text Notes 5400 10400 0    79   ~ 16
Description
Wire Notes Line style solid
	9200 10200 9200 13500
Wire Notes Line style solid
	10200 9000 10200 13500
Wire Notes Line style solid
	2500 13500 10200 13500
Text Notes 9550 10400 0    79   ~ 16
Date
Text Notes 6900 9200 0    59   Italic 0
(Modification of this variant on bill of material)
Wire Notes Line style solid
	2500 10900 10200 10900
Wire Notes Line style solid
	2500 12650 10200 12650
Text Notes 2600 10750 0    79   ~ 0
A00
Text Notes 3100 10750 0    79   ~ 0
Creation of schematics. Before layout.
Text Notes 9300 10750 0    79   ~ 0
25 aout 2019
Wire Notes Line style solid
	2500 4200 10200 4200
Text Notes 2650 3950 0    79   ~ 0
List of the\nAlways Not\nMounted\nComponents
Wire Notes Line style solid
	3500 3300 3500 4200
Wire Notes Line style solid
	2500 10200 10200 10200
Text Notes 2650 9950 0    79   ~ 0
List of the\nComponents\nunder this\nvariant
Wire Notes Line style solid
	3500 9300 3500 10200
Wire Notes Line width 20 style solid
	500  1500 22900 1500
Text Notes 3400 1200 0    315  ~ 63
Schéma Électronique du Clavier ToucheLibre     ;     Carte CoCo
Text Notes 19650 1150 0    118  Italic 0
(Connectors & Commutation)
Text Notes 17950 14450 0    59   ~ 12
Remarques Générales :\nSauf indication contraire, les résistances sont en 1%, boitier 0402 / 1005M, soit 63mW.
Text Notes 3700 3850 0    79   ~ 0
ANM >  R33008, J32006, TP32001\n\n(All the tagged components with CD (Core Design) are mounted for all the variants.)
Text Notes 3700 10050 0    79   ~ 0
G1 = Mounted     >  R33004-R33007, SW33001\nG2 = Mounted     >  R33010, R33011, SW33002\nG3 = Mounted     >  C32001-C32005, F32001, FB32001, J32001, J32002, L32001,\n                       R32001-R32003, RV32001, U32001\nG4 = Not Mounted >  C32009, J35005, SW32001
Wire Notes Line style solid
	13000 9000 20700 9000
Wire Notes Line style solid
	13000 9000 13000 13500
Wire Notes Line style solid
	13000 9300 20700 9300
Text Notes 14200 9200 0    79   ~ 16
V3 Variant Tracking : Bluetooth Only
Wire Notes Line style solid
	13400 10200 13400 13500
Text Notes 13100 10400 0    79   ~ 16
Rev.
Text Notes 15900 10400 0    79   ~ 16
Description
Wire Notes Line style solid
	19700 10200 19700 13500
Wire Notes Line style solid
	13000 13500 20700 13500
Text Notes 16900 9200 0    59   Italic 0
(Modification of this variant on bill of material)
Wire Notes Line style solid
	13000 12650 20700 12650
Text Notes 13100 10750 0    79   ~ 0
A00
Text Notes 13600 10750 0    79   ~ 0
Creation of schematics. Before layout.
Wire Notes Line style solid
	13000 10200 20700 10200
Text Notes 13150 9950 0    79   ~ 0
List of the\nComponents\nunder this\nvariant
Wire Notes Line style solid
	14000 9300 14000 10200
Text Notes 14200 10050 0    79   ~ 0
G1 = Mounted     >  R33004-R33007, SW33001\nG2 = Mounted     >  R33010, R33011, SW33002\nG3 = Not Mounted >  C32001-C32005, F32001, FB32001, J32001, J32002, L32001,\n                       R32001-R32003, RV32001, U32001\nG4 = Mounted     >  C32009, J35005, SW32001
Wire Notes Line style solid
	13000 3000 20700 3000
Wire Notes Line style solid
	13000 3000 13000 7500
Wire Notes Line style solid
	13000 3300 20700 3300
Text Notes 13650 3200 0    79   ~ 16
V1 Variant Tracking : Without Selection Switches
Wire Notes Line style solid
	13000 4500 20700 4500
Wire Notes Line style solid
	13400 4200 13400 7500
Text Notes 13100 4400 0    79   ~ 16
Rev.
Text Notes 15900 4400 0    79   ~ 16
Description
Wire Notes Line style solid
	19700 4200 19700 7500
Wire Notes Line style solid
	20700 3000 20700 7500
Wire Notes Line style solid
	13000 7500 20700 7500
Text Notes 20050 4400 0    79   ~ 16
Date
Text Notes 16900 3200 0    59   Italic 0
(Modification of this variant on bill of material)
Wire Notes Line style solid
	13000 4900 20700 4900
Wire Notes Line style solid
	13000 6650 20700 6650
Text Notes 13100 4750 0    79   ~ 0
A00
Text Notes 13600 4750 0    79   ~ 0
Creation of schematics. Before layout.
Text Notes 19800 4750 0    79   ~ 0
25 aout 2019
Wire Notes Line style solid
	13000 4200 20700 4200
Text Notes 13150 3950 0    79   ~ 0
List of the\nComponents\nunder this\nvariant
Wire Notes Line style solid
	14000 3300 14000 4200
Text Notes 14200 4050 0    79   ~ 0
G1 = Not Mounted >  R33004-R33007, SW33001\nG2 = Not Mounted >  R33010, R33011, SW33002\nG3 = Mounted     >  C32001-C32005, F32001, FB32001, J32001, J32002, L32001,\n                       R32001-R32003, RV32001, U32001\nG4 = Mounted     >  C32009, J35005, SW32001
Text Notes 3100 6550 0    79   ~ 0
Modification to facilitate the layout:\n   * J32002 = 10 pins to be coherente with J12004 of Clavier board.\n   * Add J32006 to be coherente with J12002 of Clavier board.\n   * J32003, J32006, I32002 = reverse footprint issue\n   * J32004 = modification of pin-out\n   * J 32005 = 4 pins to EMC\n   * Add global port VBUS_F on USB detection to complete the net list.\n   * Rename GND pin of switch ON-OFF SW32001 in relation to footprint.\n   * L32001 is turn on X axe mirror and change pin numbers to right layout of USB.\n   * Net ON-OFF_2 is connected on SW32001-pin1 instead pin 3.\n   * J32002 turn to normal orientation type.\n   * Modification of name to differential routing: USB0_Dx became USB2_N or _P.\n   * Inversion between USB2_N and USB2_P.
Text Notes 2650 5100 0    79   ~ 0
B
Text Notes 9300 5150 0    79   ~ 0
20 oct 2019
Text Notes 13600 6550 0    79   ~ 0
Modification to facilitate the layout:\n   * J32002 = 10 pins to be coherente with J12004 of Clavier board.\n   * Add J32006 to be coherente with J12002 of Clavier board.\n   * J32003, J32006, I32002 = reverse footprint issue\n   * J32004 = modification of pin-out\n   * J 32005 = 4 pins to EMC\n   * Add global port VBUS_F on USB detection to complete the net list.\n   * Rename GND pin of switch ON-OFF SW32001 in relation to footprint.\n   * L32001 is turn on X axe mirror and change pin numbers to right layout of USB.\n   * Net ON-OFF_2 is connected on SW32001-pin1 instead pin 3.\n   * J32002 turn to normal orientation type.\n   * Modification of name to differential routing: USB0_Dx became USB2_N or _P.\n   * Inversion between USB2_N and USB2_P.
Text Notes 13100 5100 0    79   ~ 0
B00
Text Notes 19800 5100 0    79   ~ 0
20 oct 2019
Text Notes 13600 12550 0    79   ~ 0
Modification to facilitate the layout:\n   * J32002 = 10 pins to be coherente with J12004 of Clavier board.\n   * Add J32006 to be coherente with J12002 of Clavier board.\n   * J32003, J32006, I32002 = reverse footprint issue\n   * J32004 = modification of pin-out\n   * J 32005 = 4 pins to EMC\n   * Add global port VBUS_F on USB detection to complete the net list.\n   * Rename GND pin of switch ON-OFF SW32001 in relation to footprint.\n   * L32001 is turn on X axe mirror and change pin numbers to right layout of USB.\n   * Net ON-OFF_2 is connected on SW32001-pin1 instead pin 3.\n   * J32002 turn to normal orientation type.\n   * Modification of name to differential routing: USB0_Dx became USB2_N or _P.\n   * Inversion between USB2_N and USB2_P.
Text Notes 13100 11100 0    79   ~ 0
B00
Text Notes 3100 12550 0    79   ~ 0
Modification to facilitate the layout:\n   * J32002 = 10 pins to be coherente with J12004 of Clavier board.\n   * Add J32006 to be coherente with J12002 of Clavier board.\n   * J32003, J32006, I32002 = reverse footprint issue\n   * J32004 = modification of pin-out\n   * J 32005 = 4 pins to EMC\n   * Add global port VBUS_F on USB detection to complete the net list.\n   * Rename GND pin of switch ON-OFF SW32001 in relation to footprint.\n   * L32001 is turn on X axe mirror and change pin numbers to right layout of USB.\n   * Net ON-OFF_2 is connected on SW32001-pin1 instead pin 3.\n   * J32002 turn to normal orientation type.\n   * Modification of name to differential routing: USB0_Dx became USB2_N or _P.\n   * Inversion between USB2_N and USB2_P.
Text Notes 2600 11100 0    79   ~ 0
B00
Text Notes 9300 11150 0    79   ~ 0
20 oct 2019
Text Notes 19800 11150 0    79   ~ 0
20 oct 2019
Text Notes 19800 10750 0    79   ~ 0
25 aout 2019
Wire Notes Line style solid
	13000 10900 20700 10900
Text Notes 20050 10400 0    79   ~ 16
Date
Wire Notes Line style solid
	20700 9000 20700 13500
Wire Notes Line style solid
	13000 10500 20700 10500
$EndSCHEMATC
