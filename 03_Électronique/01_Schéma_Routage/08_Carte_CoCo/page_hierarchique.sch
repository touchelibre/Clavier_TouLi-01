EESchema Schematic File Version 4
LIBS:power
LIBS:device
LIBS:switches
LIBS:relays
LIBS:motors
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:carte_coco_simple-cache
EELAYER 26 0
EELAYER END
$Descr A2 23386 16535
encoding utf-8
Sheet 2 5
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Sheet
S 14650 5200 2550 2050
U 5CA9DC7A
F0 "Hub_USB" 50
F1 "Hub_USB.sch" 50
$EndSheet
$Comp
L carte_coco_simple-rescue:USB_B_Micro-Connector J?
U 1 1 5CA9F1C2
P 6150 3850
AR Path="/5BBF869B/5CA9DCAF/5CA9F1C2" Ref="J?"  Part="1" 
AR Path="/5BBF869B/5CA9F1C2" Ref="J?"  Part="1" 
AR Path="/5CA9F1C2" Ref="J?"  Part="1" 
F 0 "J?" H 6000 4350 50  0000 C CNN
F 1 "USB_B_Micro" H 6205 4226 50  0000 C CNN
F 2 "" H 6300 3800 50  0001 C CNN
F 3 "~" H 6300 3800 50  0001 C CNN
	1    6150 3850
	1    0    0    -1  
$EndComp
Wire Notes Line
	11850 12200 11850 2250
Wire Notes Line
	11850 2250 5950 2250
Wire Notes Line
	5950 2250 5950 12200
Wire Notes Line
	5950 12200 11850 12200
Text Notes 6050 2200 0    79   ~ 16
Connecteurs\nConnectors
$Comp
L carte_coco_simple-rescue:GND-power #PWR?
U 1 1 5CA9F2B5
P 6050 4400
F 0 "#PWR?" H 6050 4150 50  0001 C CNN
F 1 "GND-power" H 6055 4227 50  0001 C CNN
F 2 "" H 6050 4400 50  0001 C CNN
F 3 "" H 6050 4400 50  0001 C CNN
	1    6050 4400
	1    0    0    -1  
$EndComp
Wire Wire Line
	6050 4400 6050 4300
Wire Wire Line
	6050 4300 6150 4300
Wire Wire Line
	6150 4300 6150 4250
Connection ~ 6050 4300
Wire Wire Line
	6050 4300 6050 4250
NoConn ~ 6450 4050
Text GLabel 12350 3650 2    50   UnSpc ~ 10
VBUS_F
$Comp
L carte_coco_simple-rescue:VBUS-power #PWR?
U 1 1 5CA9F397
P 6550 3550
F 0 "#PWR?" H 6550 3400 50  0001 C CNN
F 1 "VBUS" H 6565 3723 50  0000 C CNN
F 2 "" H 6550 3550 50  0001 C CNN
F 3 "" H 6550 3550 50  0001 C CNN
	1    6550 3550
	1    0    0    -1  
$EndComp
Wire Wire Line
	6550 3550 6550 3650
$Comp
L carte_coco_simple-rescue:VBUS-power #PWR?
U 1 1 5CAA15CD
P 13900 3000
F 0 "#PWR?" H 13900 2850 50  0001 C CNN
F 1 "VBUS" H 13915 3173 50  0000 C CNN
F 2 "" H 13900 3000 50  0001 C CNN
F 3 "" H 13900 3000 50  0001 C CNN
	1    13900 3000
	1    0    0    -1  
$EndComp
Wire Wire Line
	13900 3000 13900 3100
$Comp
L carte_coco_simple-rescue:+BATT-power #PWR?
U 1 1 5CAA1746
P 13900 3800
F 0 "#PWR?" H 13900 3650 50  0001 C CNN
F 1 "+BATT" H 13915 3973 50  0000 C CNN
F 2 "" H 13900 3800 50  0001 C CNN
F 3 "" H 13900 3800 50  0001 C CNN
	1    13900 3800
	1    0    0    -1  
$EndComp
Wire Wire Line
	13900 3800 13900 3900
$Comp
L carte_coco_simple-rescue:NUP2202-Power_Protection U?
U 1 1 5CB3F8EB
P 7100 4350
F 0 "U?" H 7300 4200 50  0000 L CNN
F 1 "NUP2202" H 7150 4100 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-363_SC-70-6" H 7180 4425 50  0001 C CNN
F 3 "http://www.onsemi.ru.com/pub_link/Collateral/NUP2202W1-D.PDF" H 7180 4425 50  0001 C CNN
	1    7100 4350
	1    0    0    -1  
$EndComp
Wire Wire Line
	6900 4350 6850 4350
Wire Wire Line
	7300 4350 7350 4350
Wire Wire Line
	7100 4550 7100 4600
$Comp
L carte_coco_simple-rescue:GND-power #PWR?
U 1 1 5CB40B62
P 7100 4600
F 0 "#PWR?" H 7100 4350 50  0001 C CNN
F 1 "GND-power" H 7105 4427 50  0001 C CNN
F 2 "" H 7100 4600 50  0001 C CNN
F 3 "" H 7100 4600 50  0001 C CNN
	1    7100 4600
	1    0    0    -1  
$EndComp
Wire Notes Line
	7600 3250 7600 4750
Wire Notes Line
	7600 4750 6700 4750
Wire Notes Line
	6700 4750 6700 3250
Wire Notes Line
	6700 3250 7600 3250
Wire Wire Line
	13900 3100 14650 3100
Wire Wire Line
	13900 3900 14650 3900
Text Notes 4650 4000 0    79   ~ 16
USB vers Ordi\nUSB to PC
Text Notes 4550 10600 0    79   ~ 16
Vers Carte Clavier\nTo Keyboard Board
$Comp
L carte_coco_simple-rescue:GND-power #PWR?
U 1 1 5CB48AF1
P 6300 11400
F 0 "#PWR?" H 6300 11150 50  0001 C CNN
F 1 "GND-power" H 6305 11227 50  0001 C CNN
F 2 "" H 6300 11400 50  0001 C CNN
F 3 "" H 6300 11400 50  0001 C CNN
	1    6300 11400
	1    0    0    -1  
$EndComp
Wire Wire Line
	6300 11400 6300 11300
Wire Wire Line
	6300 11300 6200 11300
Wire Wire Line
	6300 11300 6300 11000
Wire Wire Line
	6300 11000 6200 11000
Connection ~ 6300 11300
Connection ~ 6300 11000
Wire Wire Line
	6300 10400 6200 10400
$Comp
L carte_coco_simple-rescue:+5V-power #PWR?
U 1 1 5CB4EB6F
P 7350 11100
F 0 "#PWR?" H 7350 10950 50  0001 C CNN
F 1 "+5V" H 7350 11250 50  0000 C CNN
F 2 "" H 7350 11100 50  0001 C CNN
F 3 "" H 7350 11100 50  0001 C CNN
	1    7350 11100
	1    0    0    -1  
$EndComp
$Comp
L carte_coco_simple-rescue:+3.3V-power #PWR?
U 1 1 5CB4EBAA
P 7600 11100
F 0 "#PWR?" H 7600 10950 50  0001 C CNN
F 1 "+3.3V" H 7600 11250 50  0000 C CNN
F 2 "" H 7600 11100 50  0001 C CNN
F 3 "" H 7600 11100 50  0001 C CNN
	1    7600 11100
	1    0    0    -1  
$EndComp
Wire Wire Line
	7600 11100 7600 11200
$Comp
L carte_coco_simple-rescue:GND-power #PWR?
U 1 1 5CB512F3
P 6600 11700
AR Path="/5BBF869B/5CA9DCAF/5CB512F3" Ref="#PWR?"  Part="1" 
AR Path="/5BBF869B/5CB512F3" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 6600 11450 50  0001 C CNN
F 1 "GND-power" H 6605 11527 50  0001 C CNN
F 2 "" H 6600 11700 50  0001 C CNN
F 3 "" H 6600 11700 50  0001 C CNN
	1    6600 11700
	1    0    0    -1  
$EndComp
$Comp
L carte_coco_simple-rescue:C-Device C?
U 1 1 5CB512F9
P 6600 11500
AR Path="/5BBF869B/5CA9DCAF/5CB512F9" Ref="C?"  Part="1" 
AR Path="/5BBF869B/5CB512F9" Ref="C?"  Part="1" 
F 0 "C?" H 6350 11600 50  0000 L CNN
F 1 "C" H 6350 11400 50  0000 L CNN
F 2 "" H 6638 11350 50  0001 C CNN
F 3 "~" H 6600 11500 50  0001 C CNN
	1    6600 11500
	1    0    0    -1  
$EndComp
Wire Wire Line
	6600 11650 6600 11700
$Comp
L carte_coco_simple-rescue:GND-power #PWR?
U 1 1 5CB51301
P 6900 11700
AR Path="/5BBF869B/5CA9DCAF/5CB51301" Ref="#PWR?"  Part="1" 
AR Path="/5BBF869B/5CB51301" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 6900 11450 50  0001 C CNN
F 1 "GND-power" H 6905 11527 50  0001 C CNN
F 2 "" H 6900 11700 50  0001 C CNN
F 3 "" H 6900 11700 50  0001 C CNN
	1    6900 11700
	1    0    0    -1  
$EndComp
$Comp
L carte_coco_simple-rescue:C-Device C?
U 1 1 5CB51307
P 6900 11500
AR Path="/5BBF869B/5CA9DCAF/5CB51307" Ref="C?"  Part="1" 
AR Path="/5BBF869B/5CB51307" Ref="C?"  Part="1" 
F 0 "C?" H 6950 11600 50  0000 L CNN
F 1 "C" H 6950 11400 50  0000 L CNN
F 2 "" H 6938 11350 50  0001 C CNN
F 3 "~" H 6900 11500 50  0001 C CNN
	1    6900 11500
	1    0    0    -1  
$EndComp
Wire Wire Line
	6900 11650 6900 11700
Wire Wire Line
	6600 11350 6600 11100
Connection ~ 6600 11100
Wire Wire Line
	17250 2900 18050 2900
Wire Wire Line
	18050 2900 18050 2800
Wire Wire Line
	17250 3500 18050 3500
Wire Wire Line
	18050 3500 18050 3400
$Comp
L carte_coco_simple-rescue:+5V-power #PWR?
U 1 1 5CB575C8
P 18050 2800
F 0 "#PWR?" H 18050 2650 50  0001 C CNN
F 1 "+5V" H 18065 2973 50  0000 C CNN
F 2 "" H 18050 2800 50  0001 C CNN
F 3 "" H 18050 2800 50  0001 C CNN
	1    18050 2800
	1    0    0    -1  
$EndComp
$Comp
L carte_coco_simple-rescue:+3.3V-power #PWR?
U 1 1 5CB57609
P 18050 3400
F 0 "#PWR?" H 18050 3250 50  0001 C CNN
F 1 "+3.3V" H 18065 3573 50  0000 C CNN
F 2 "" H 18050 3400 50  0001 C CNN
F 3 "" H 18050 3400 50  0001 C CNN
	1    18050 3400
	1    0    0    -1  
$EndComp
Text Label 11600 3850 2    50   ~ 0
USB0_DP_F
Text Label 11600 3950 2    50   ~ 0
USB0_DM_F
$Sheet
S 14600 8800 2500 3050
U 5BBF872B
F0 "touche_led" 60
F1 "touche_led.sch" 60
F2 "I2C_COCO_SDA" B L 14600 10600 50 
F3 "I2C_COCO_SCL" I L 14600 10500 50 
F4 "I2C_COCO_INT" O L 14600 10800 50 
F5 "Col_5bis_SW" I L 14600 9200 50 
F6 "Lig_Fbis_SW" O L 14600 9100 50 
$EndSheet
$Comp
L Connector:TestPoint TP?
U 1 1 5CD75CA4
P 11050 3600
F 0 "TP?" H 11108 3720 50  0000 L CNN
F 1 "TestPoint" H 11108 3629 50  0001 L CNN
F 2 "" H 11250 3600 50  0001 C CNN
F 3 "~" H 11250 3600 50  0001 C CNN
	1    11050 3600
	1    0    0    -1  
$EndComp
$Sheet
S 14650 2500 2600 2150
U 5CA9DCAF
F0 "Power_Supply" 50
F1 "Power_Supply.sch" 50
F2 "VBUS" U L 14650 3100 79 
F3 "VBAT" U L 14650 3900 79 
F4 "P5V" U R 17250 2900 79 
F5 "P3V3" U R 17250 3500 79 
F6 "P1V1" U R 17250 4100 79 
$EndSheet
Wire Wire Line
	17250 4100 18050 4100
Wire Wire Line
	18050 4100 18050 4000
$Comp
L power:+1V1 #PWR?
U 1 1 5CE39787
P 18050 4000
F 0 "#PWR?" H 18050 3850 50  0001 C CNN
F 1 "+1V1" H 18065 4173 50  0000 C CNN
F 2 "" H 18050 4000 50  0001 C CNN
F 3 "" H 18050 4000 50  0001 C CNN
	1    18050 4000
	1    0    0    -1  
$EndComp
$Comp
L Device:Varistor RV?
U 1 1 5D4B0B9D
P 8250 4400
F 0 "RV?" H 7900 4450 50  0000 L CNN
F 1 "Varistor" H 7850 4350 50  0000 L CNN
F 2 "" V 8180 4400 50  0001 C CNN
F 3 "~" H 8250 4400 50  0001 C CNN
	1    8250 4400
	1    0    0    -1  
$EndComp
$Comp
L carte_coco_simple-rescue:GND-power #PWR?
U 1 1 5D4B9B90
P 8250 4600
F 0 "#PWR?" H 8250 4350 50  0001 C CNN
F 1 "GND-power" H 8255 4427 50  0001 C CNN
F 2 "" H 8250 4600 50  0001 C CNN
F 3 "" H 8250 4600 50  0001 C CNN
	1    8250 4600
	1    0    0    -1  
$EndComp
Wire Wire Line
	8250 4600 8250 4550
Text Notes 6750 3200 0    50   ~ 10
ESD Protection
Wire Notes Line
	7700 3250 7700 4750
Wire Notes Line
	7700 4750 8400 4750
Wire Notes Line
	8400 4750 8400 3250
Wire Notes Line
	8400 3250 7700 3250
Text Notes 7650 3200 0    50   ~ 10
Overvoltage and\novercurrent Protection
$Comp
L power:GND #PWR?
U 1 1 5D4D2003
P 9650 4600
AR Path="/5D4D2003" Ref="#PWR?"  Part="1" 
AR Path="/5D4A0749/5D49F20C/5D4D2003" Ref="#PWR?"  Part="1" 
AR Path="/5D4A0749/5D55A9E5/5D4D2003" Ref="#PWR?"  Part="1" 
AR Path="/5BBF869B/5D4D2003" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 9650 4350 50  0001 C CNN
F 1 "GND" H 9655 4427 50  0001 C CNN
F 2 "" H 9650 4600 50  0001 C CNN
F 3 "" H 9650 4600 50  0001 C CNN
	1    9650 4600
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 5D4D200A
P 9650 4400
AR Path="/5D4D200A" Ref="C?"  Part="1" 
AR Path="/5D4A0749/5D49F20C/5D4D200A" Ref="C?"  Part="1" 
AR Path="/5D4A0749/5D55A9E5/5D4D200A" Ref="C?"  Part="1" 
AR Path="/5BBF869B/5D4D200A" Ref="C?"  Part="1" 
F 0 "C?" H 9700 4500 50  0000 L CNN
F 1 "10uF" H 9750 4400 50  0000 L CNN
F 2 "" H 9688 4250 50  0001 C CNN
F 3 "~" H 9650 4400 50  0001 C CNN
F 4 "10V" H 9800 4300 50  0000 C CNN "Tension"
	1    9650 4400
	1    0    0    -1  
$EndComp
$Comp
L Device:Ferrite_Bead FB?
U 1 1 5D4D2013
P 10000 3650
AR Path="/5D4D2013" Ref="FB?"  Part="1" 
AR Path="/5D4A0749/5D49F20C/5D4D2013" Ref="FB?"  Part="1" 
AR Path="/5D4A0749/5D55A9E5/5D4D2013" Ref="FB?"  Part="1" 
AR Path="/5BBF869B/5D4D2013" Ref="FB?"  Part="1" 
F 0 "FB?" V 9726 3650 50  0000 C CNN
F 1 "BLM18HE152SN1" V 9817 3650 50  0000 C CNN
F 2 "" V 9930 3650 50  0001 C CNN
F 3 "~" H 10000 3650 50  0001 C CNN
F 4 "500mA" V 9950 3900 50  0000 C CNN "Current"
	1    10000 3650
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5D4D201B
P 10600 4600
AR Path="/5D4D201B" Ref="#PWR?"  Part="1" 
AR Path="/5D4A0749/5D49F20C/5D4D201B" Ref="#PWR?"  Part="1" 
AR Path="/5D4A0749/5D55A9E5/5D4D201B" Ref="#PWR?"  Part="1" 
AR Path="/5BBF869B/5D4D201B" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 10600 4350 50  0001 C CNN
F 1 "GND" H 10605 4427 50  0001 C CNN
F 2 "" H 10600 4600 50  0001 C CNN
F 3 "" H 10600 4600 50  0001 C CNN
	1    10600 4600
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 5D4D2024
P 10600 4400
AR Path="/5D4D2024" Ref="C?"  Part="1" 
AR Path="/5D4A0749/5D49F20C/5D4D2024" Ref="C?"  Part="1" 
AR Path="/5D4A0749/5D55A9E5/5D4D2024" Ref="C?"  Part="1" 
AR Path="/5BBF869B/5D4D2024" Ref="C?"  Part="1" 
F 0 "C?" H 10300 4500 50  0000 L CNN
F 1 "10uF" H 10250 4400 50  0000 L CNN
F 2 "" H 10638 4250 50  0001 C CNN
F 3 "~" H 10600 4400 50  0001 C CNN
F 4 "10V" H 10350 4300 50  0000 C CNN "Tension"
	1    10600 4400
	1    0    0    -1  
$EndComp
Wire Wire Line
	9650 4600 9650 4550
Wire Wire Line
	10600 4550 10600 4600
Wire Wire Line
	6450 3650 6550 3650
$Comp
L Device:L_Core_Ferrite_Coupled_Small L?
U 1 1 5D4F49D8
P 10000 3900
F 0 "L?" H 9950 3750 50  0000 C CNN
F 1 "WE-CNSW_744232090" H 10100 3650 50  0000 C CNN
F 2 "" H 10000 3900 50  0001 C CNN
F 3 "~" H 10000 3900 50  0001 C CNN
	1    10000 3900
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 5D50026B
P 8950 4400
F 0 "R?" H 8650 4450 50  0000 L CNN
F 1 "47k" H 8700 4350 50  0000 L CNN
F 2 "" V 8880 4400 50  0001 C CNN
F 3 "~" H 8950 4400 50  0001 C CNN
	1    8950 4400
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5D500C92
P 9400 4600
AR Path="/5D500C92" Ref="#PWR?"  Part="1" 
AR Path="/5D4A0749/5D49F20C/5D500C92" Ref="#PWR?"  Part="1" 
AR Path="/5D4A0749/5D55A9E5/5D500C92" Ref="#PWR?"  Part="1" 
AR Path="/5BBF869B/5D4D1C79/5D500C92" Ref="#PWR?"  Part="1" 
AR Path="/5BBF869B/5D500C92" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 9400 4350 50  0001 C CNN
F 1 "GND" H 9405 4427 50  0001 C CNN
F 2 "" H 9400 4600 50  0001 C CNN
F 3 "" H 9400 4600 50  0001 C CNN
	1    9400 4600
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 5D500C99
P 9400 4400
AR Path="/5D500C99" Ref="C?"  Part="1" 
AR Path="/5D4A0749/5D49F20C/5D500C99" Ref="C?"  Part="1" 
AR Path="/5D4A0749/5D55A9E5/5D500C99" Ref="C?"  Part="1" 
AR Path="/5BBF869B/5D4D1C79/5D500C99" Ref="C?"  Part="1" 
AR Path="/5BBF869B/5D500C99" Ref="C?"  Part="1" 
F 0 "C?" H 9150 4500 50  0000 L CNN
F 1 "220nF" H 9050 4400 50  0000 L CNN
F 2 "" H 9438 4250 50  0001 C CNN
F 3 "~" H 9400 4400 50  0001 C CNN
F 4 "10V" H 9200 4300 50  0000 C CNN "Tension"
	1    9400 4400
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5D504FF9
P 8950 4600
AR Path="/5D504FF9" Ref="#PWR?"  Part="1" 
AR Path="/5D4A0749/5D49F20C/5D504FF9" Ref="#PWR?"  Part="1" 
AR Path="/5D4A0749/5D55A9E5/5D504FF9" Ref="#PWR?"  Part="1" 
AR Path="/5BBF869B/5D4D1C79/5D504FF9" Ref="#PWR?"  Part="1" 
AR Path="/5BBF869B/5D504FF9" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 8950 4350 50  0001 C CNN
F 1 "GND" H 8955 4427 50  0001 C CNN
F 2 "" H 8950 4600 50  0001 C CNN
F 3 "" H 8950 4600 50  0001 C CNN
	1    8950 4600
	1    0    0    -1  
$EndComp
Wire Wire Line
	8950 4600 8950 4550
Wire Wire Line
	9400 4600 9400 4550
Connection ~ 9650 3650
Wire Wire Line
	9650 3650 9850 3650
Wire Wire Line
	10600 3650 11050 3650
Connection ~ 10600 3650
Wire Notes Line
	8600 3250 8600 4750
Wire Notes Line
	8600 4750 10850 4750
Wire Notes Line
	10850 4750 10850 3250
Wire Notes Line
	8600 3250 10850 3250
Text Notes 9100 3200 0    50   ~ 10
EMC Filter
Connection ~ 9400 3650
Wire Wire Line
	9400 3650 9650 3650
Connection ~ 8950 3650
Wire Wire Line
	8950 3650 9400 3650
$Comp
L Connector_Generic:Conn_01x04 J?
U 1 1 5D54CEBB
P 6000 6600
F 0 "J?" H 5920 6917 50  0000 C CNN
F 1 "Conn_01x04" H 5750 6800 50  0000 C CNN
F 2 "" H 6000 6600 50  0001 C CNN
F 3 "~" H 6000 6600 50  0001 C CNN
	1    6000 6600
	-1   0    0    -1  
$EndComp
$Comp
L carte_coco_simple-rescue:GND-power #PWR?
U 1 1 5D551F5F
P 6300 6900
F 0 "#PWR?" H 6300 6650 50  0001 C CNN
F 1 "GND-power" H 6305 6727 50  0001 C CNN
F 2 "" H 6300 6900 50  0001 C CNN
F 3 "" H 6300 6900 50  0001 C CNN
	1    6300 6900
	1    0    0    -1  
$EndComp
Wire Wire Line
	6300 6900 6300 6800
Wire Wire Line
	6300 6500 6200 6500
Wire Wire Line
	6200 6800 6300 6800
Connection ~ 6300 6800
Wire Wire Line
	6300 6800 6300 6500
$Comp
L Device:R R?
U 1 1 5D555984
P 8050 6600
F 0 "R?" V 7850 6550 50  0000 C CNN
F 1 "10" V 7950 6550 50  0000 C CNN
F 2 "" V 7980 6600 50  0001 C CNN
F 3 "~" H 8050 6600 50  0001 C CNN
	1    8050 6600
	0    1    1    0   
$EndComp
$Comp
L Device:R R?
U 1 1 5D55598C
P 8050 6700
F 0 "R?" V 8150 6650 50  0000 C CNN
F 1 "10" V 8250 6650 50  0000 C CNN
F 2 "" V 7980 6700 50  0001 C CNN
F 3 "~" H 8050 6700 50  0001 C CNN
	1    8050 6700
	0    1    1    0   
$EndComp
Text Notes 4400 6750 0    79   ~ 16
USB vers Carte Clavier\nUSB to Keyboard
Wire Wire Line
	11050 3600 11050 3650
Connection ~ 11050 3650
Wire Wire Line
	11050 3650 12100 3650
Connection ~ 6550 3650
$Comp
L power:VBUS_F #PWR?
U 1 1 5D573F15
P 12100 3550
F 0 "#PWR?" H 12250 3450 50  0001 C CNN
F 1 "VBUS_F" H 12095 3724 50  0000 C CNN
F 2 "" H 12100 3550 50  0001 C CNN
F 3 "" H 12100 3550 50  0001 C CNN
	1    12100 3550
	1    0    0    -1  
$EndComp
Wire Wire Line
	12100 3550 12100 3650
Connection ~ 12100 3650
Text Label 6750 6600 2    50   ~ 0
USB0_DP
Text Label 6750 6700 2    50   ~ 0
USB0_DM
$Comp
L power:GND #PWR?
U 1 1 5D586A4E
P 7250 7300
AR Path="/5D586A4E" Ref="#PWR?"  Part="1" 
AR Path="/5D4A0749/5D49F20C/5D586A4E" Ref="#PWR?"  Part="1" 
AR Path="/5D4A0749/5D55A9E5/5D586A4E" Ref="#PWR?"  Part="1" 
AR Path="/5BBF869B/5D4D1C79/5D586A4E" Ref="#PWR?"  Part="1" 
AR Path="/5BBF869B/5D586A4E" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 7250 7050 50  0001 C CNN
F 1 "GND" H 7255 7127 50  0001 C CNN
F 2 "" H 7250 7300 50  0001 C CNN
F 3 "" H 7250 7300 50  0001 C CNN
	1    7250 7300
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 5D586A55
P 7250 7100
AR Path="/5D586A55" Ref="C?"  Part="1" 
AR Path="/5D4A0749/5D49F20C/5D586A55" Ref="C?"  Part="1" 
AR Path="/5D4A0749/5D55A9E5/5D586A55" Ref="C?"  Part="1" 
AR Path="/5BBF869B/5D4D1C79/5D586A55" Ref="C?"  Part="1" 
AR Path="/5BBF869B/5D586A55" Ref="C?"  Part="1" 
F 0 "C?" H 7000 7200 50  0000 L CNN
F 1 "1pF" H 6950 7100 50  0000 L CNN
F 2 "" H 7288 6950 50  0001 C CNN
F 3 "~" H 7250 7100 50  0001 C CNN
F 4 "10V" H 7050 7000 50  0000 C CNN "Tension"
	1    7250 7100
	1    0    0    -1  
$EndComp
Wire Wire Line
	7250 7300 7250 7250
$Comp
L power:GND #PWR?
U 1 1 5D58D4C8
P 7550 7300
AR Path="/5D58D4C8" Ref="#PWR?"  Part="1" 
AR Path="/5D4A0749/5D49F20C/5D58D4C8" Ref="#PWR?"  Part="1" 
AR Path="/5D4A0749/5D55A9E5/5D58D4C8" Ref="#PWR?"  Part="1" 
AR Path="/5BBF869B/5D4D1C79/5D58D4C8" Ref="#PWR?"  Part="1" 
AR Path="/5BBF869B/5D58D4C8" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 7550 7050 50  0001 C CNN
F 1 "GND" H 7555 7127 50  0001 C CNN
F 2 "" H 7550 7300 50  0001 C CNN
F 3 "" H 7550 7300 50  0001 C CNN
	1    7550 7300
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 5D58D4CF
P 7550 7100
AR Path="/5D58D4CF" Ref="C?"  Part="1" 
AR Path="/5D4A0749/5D49F20C/5D58D4CF" Ref="C?"  Part="1" 
AR Path="/5D4A0749/5D55A9E5/5D58D4CF" Ref="C?"  Part="1" 
AR Path="/5BBF869B/5D4D1C79/5D58D4CF" Ref="C?"  Part="1" 
AR Path="/5BBF869B/5D58D4CF" Ref="C?"  Part="1" 
F 0 "C?" H 7600 7200 50  0000 L CNN
F 1 "1pF" H 7700 7100 50  0000 L CNN
F 2 "" H 7588 6950 50  0001 C CNN
F 3 "~" H 7550 7100 50  0001 C CNN
F 4 "10V" H 7700 7000 50  0000 C CNN "Tension"
	1    7550 7100
	1    0    0    -1  
$EndComp
Wire Wire Line
	7550 7300 7550 7250
Wire Wire Line
	6200 6700 7550 6700
Wire Wire Line
	6200 6600 7250 6600
Wire Wire Line
	7550 6950 7550 6700
Connection ~ 7550 6700
Wire Wire Line
	7550 6700 7900 6700
Wire Wire Line
	7250 6950 7250 6600
Connection ~ 7250 6600
Wire Wire Line
	7250 6600 7900 6600
Text Label 9300 6600 2    50   ~ 0
USB0_DP_F
Text Label 9300 6700 2    50   ~ 0
USB0_DM_F
Text Label 9300 6600 2    50   ~ 0
USB0_DP_F
Text Label 9300 6700 2    50   ~ 0
USB0_DM_F
Wire Wire Line
	8200 6600 9300 6600
Wire Wire Line
	8200 6700 9300 6700
Wire Notes Line
	6850 6100 6850 7600
Wire Notes Line
	6850 7600 8350 7600
Wire Notes Line
	6850 6100 8350 6100
Wire Notes Line
	8350 6100 8350 7600
Text Notes 7000 6050 0    50   ~ 10
Matching
Text Notes 7200 6500 0    50   ~ 0
Capacitor\ncould be\nnot mounted
Wire Wire Line
	12100 3650 12350 3650
Wire Wire Line
	10100 3950 11600 3950
Wire Wire Line
	10100 3850 11600 3850
Wire Wire Line
	10150 3650 10600 3650
$Comp
L Device:Polyfuse F?
U 1 1 5D5CDD13
P 8000 3650
F 0 "F?" V 8300 3650 50  0000 L CNN
F 1 "1206L050/15" V 8200 3350 50  0000 L CNN
F 2 "" H 8050 3450 50  0001 L CNN
F 3 "~" H 8000 3650 50  0001 C CNN
F 4 "1A" V 8150 3750 50  0000 R BNN "Trip Current"
	1    8000 3650
	0    -1   -1   0   
$EndComp
Connection ~ 8250 3650
Wire Wire Line
	8250 3650 8950 3650
Wire Wire Line
	8150 3650 8250 3650
Wire Wire Line
	10600 3650 10600 4250
Wire Wire Line
	9650 3650 9650 4250
Wire Wire Line
	9400 3650 9400 4250
Wire Wire Line
	8950 3650 8950 4250
Wire Wire Line
	8250 3650 8250 4250
Wire Wire Line
	6450 3850 6850 3850
Wire Wire Line
	6550 3650 7100 3650
Wire Wire Line
	6450 3950 7350 3950
Wire Wire Line
	6850 4350 6850 3850
Connection ~ 6850 3850
Wire Wire Line
	6850 3850 9900 3850
Wire Wire Line
	7100 3650 7100 4150
Connection ~ 7100 3650
Wire Wire Line
	7100 3650 7850 3650
Wire Wire Line
	7350 3950 7350 4350
Connection ~ 7350 3950
Wire Wire Line
	7350 3950 9900 3950
$Comp
L carte_coco_simple-rescue:Conn_01x02-Connector_Generic J?
U 1 1 5D5F4CDD
P 6000 9100
F 0 "J?" H 5900 9350 50  0000 L CNN
F 1 "Conn_01x02" H 5550 9250 50  0000 L CNN
F 2 "" H 6000 9100 50  0001 C CNN
F 3 "" H 6000 9100 50  0001 C CNN
	1    6000 9100
	-1   0    0    -1  
$EndComp
Wire Wire Line
	6200 9100 14600 9100
Wire Wire Line
	6200 9200 14600 9200
$Comp
L power:VBUS_F #PWR?
U 1 1 5D687E6A
P 6600 9850
F 0 "#PWR?" H 6750 9750 50  0001 C CNN
F 1 "VBUS_F" H 6595 10024 50  0000 C CNN
F 2 "" H 6600 9850 50  0001 C CNN
F 3 "" H 6600 9850 50  0001 C CNN
	1    6600 9850
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x10 J?
U 1 1 5D6911EC
P 6000 10800
F 0 "J?" H 5920 11417 50  0000 C CNN
F 1 "Conn_01x10" H 5920 11326 50  0000 C CNN
F 2 "" H 6000 10800 50  0001 C CNN
F 3 "~" H 6000 10800 50  0001 C CNN
	1    6000 10800
	-1   0    0    -1  
$EndComp
Wire Wire Line
	6300 10400 6300 10700
Wire Wire Line
	6200 10900 6600 10900
Wire Wire Line
	6200 10700 6300 10700
Connection ~ 6300 10700
Wire Wire Line
	6300 10700 6300 11000
Wire Wire Line
	6200 10600 14600 10600
Wire Wire Line
	6200 10500 14600 10500
Wire Wire Line
	6200 10800 14600 10800
Wire Wire Line
	6600 9850 6600 9900
$Comp
L carte_coco_simple-rescue:GND-power #PWR?
U 1 1 5D6BE4A9
P 6900 10300
AR Path="/5BBF869B/5CA9DCAF/5D6BE4A9" Ref="#PWR?"  Part="1" 
AR Path="/5BBF869B/5D6BE4A9" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 6900 10050 50  0001 C CNN
F 1 "GND-power" H 6905 10127 50  0001 C CNN
F 2 "" H 6900 10300 50  0001 C CNN
F 3 "" H 6900 10300 50  0001 C CNN
	1    6900 10300
	1    0    0    -1  
$EndComp
$Comp
L carte_coco_simple-rescue:C-Device C?
U 1 1 5D6BE4AF
P 6900 10100
AR Path="/5BBF869B/5CA9DCAF/5D6BE4AF" Ref="C?"  Part="1" 
AR Path="/5BBF869B/5D6BE4AF" Ref="C?"  Part="1" 
F 0 "C?" H 7015 10146 50  0000 L CNN
F 1 "C" H 7015 10055 50  0000 L CNN
F 2 "" H 6938 9950 50  0001 C CNN
F 3 "~" H 6900 10100 50  0001 C CNN
	1    6900 10100
	1    0    0    -1  
$EndComp
Wire Wire Line
	6900 10250 6900 10300
Wire Wire Line
	6900 9950 6900 9900
Wire Wire Line
	6900 9900 6600 9900
Connection ~ 6600 9900
Wire Wire Line
	6600 9900 6600 10900
Wire Wire Line
	6600 11100 7350 11100
Wire Wire Line
	6900 11350 6900 11200
Connection ~ 6900 11200
Wire Wire Line
	6900 11200 7600 11200
Wire Wire Line
	6200 11100 6600 11100
Wire Wire Line
	6200 11200 6900 11200
$EndSCHEMATC
