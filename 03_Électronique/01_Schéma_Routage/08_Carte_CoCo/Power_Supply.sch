EESchema Schematic File Version 4
LIBS:power
LIBS:device
LIBS:switches
LIBS:relays
LIBS:motors
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:carte_coco_simple-cache
EELAYER 26 0
EELAYER END
$Descr A2 23386 16535
encoding utf-8
Sheet 5 5
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L carte_coco_simple-rescue:Varistor-Device RV?
U 1 1 5CA9F065
P 5300 3900
F 0 "RV?" V 5042 3900 50  0000 C CNN
F 1 "Varistor" V 5133 3900 50  0000 C CNN
F 2 "" V 5230 3900 50  0001 C CNN
F 3 "~" H 5300 3900 50  0001 C CNN
	1    5300 3900
	0    1    1    0   
$EndComp
Text GLabel 4000 3900 0    50   UnSpc ~ 10
VBUS
Text GLabel 4000 4800 0    50   UnSpc ~ 10
VBAT
Wire Wire Line
	4000 3900 4850 3900
Wire Wire Line
	4000 4800 4850 4800
$Comp
L carte_coco_simple-rescue:D_Schottky-Device D?
U 1 1 5CAA19DC
P 5850 3900
F 0 "D?" H 5850 4116 50  0000 C CNN
F 1 "D_Schottky" H 5850 4025 50  0000 C CNN
F 2 "" H 5850 3900 50  0001 C CNN
F 3 "~" H 5850 3900 50  0001 C CNN
	1    5850 3900
	-1   0    0    -1  
$EndComp
$Comp
L carte_coco_simple-rescue:D_Schottky-Device D?
U 1 1 5CAA1AC2
P 5850 4800
F 0 "D?" H 5850 5016 50  0000 C CNN
F 1 "D_Schottky" H 5850 4925 50  0000 C CNN
F 2 "" H 5850 4800 50  0001 C CNN
F 3 "~" H 5850 4800 50  0001 C CNN
	1    5850 4800
	-1   0    0    -1  
$EndComp
Wire Wire Line
	5450 3900 5700 3900
$Comp
L carte_coco_simple-rescue:GND-power #PWR?
U 1 1 5CAA1B6E
P 4850 4350
F 0 "#PWR?" H 4850 4100 50  0001 C CNN
F 1 "GND" H 4855 4177 50  0000 C CNN
F 2 "" H 4850 4350 50  0001 C CNN
F 3 "" H 4850 4350 50  0001 C CNN
	1    4850 4350
	1    0    0    -1  
$EndComp
$Comp
L carte_coco_simple-rescue:C-Device C?
U 1 1 5CAA1BE5
P 4850 4150
F 0 "C?" H 4965 4196 50  0000 L CNN
F 1 "C" H 4965 4105 50  0000 L CNN
F 2 "" H 4888 4000 50  0001 C CNN
F 3 "~" H 4850 4150 50  0001 C CNN
	1    4850 4150
	1    0    0    -1  
$EndComp
Wire Wire Line
	4850 4000 4850 3900
Connection ~ 4850 3900
Wire Wire Line
	4850 3900 5150 3900
Wire Wire Line
	4850 4300 4850 4350
$Comp
L carte_coco_simple-rescue:GND-power #PWR?
U 1 1 5CAA1D3B
P 4850 5300
F 0 "#PWR?" H 4850 5050 50  0001 C CNN
F 1 "GND" H 4855 5127 50  0000 C CNN
F 2 "" H 4850 5300 50  0001 C CNN
F 3 "" H 4850 5300 50  0001 C CNN
	1    4850 5300
	1    0    0    -1  
$EndComp
$Comp
L carte_coco_simple-rescue:C-Device C?
U 1 1 5CAA1D41
P 4850 5100
F 0 "C?" H 4965 5146 50  0000 L CNN
F 1 "C" H 4965 5055 50  0000 L CNN
F 2 "" H 4888 4950 50  0001 C CNN
F 3 "~" H 4850 5100 50  0001 C CNN
	1    4850 5100
	1    0    0    -1  
$EndComp
Wire Wire Line
	4850 5250 4850 5300
Wire Wire Line
	4850 4800 4850 4950
Connection ~ 4850 4800
Wire Wire Line
	4850 4800 5700 4800
Wire Wire Line
	6000 4800 6300 4800
Wire Wire Line
	6300 4800 6300 3900
Wire Wire Line
	6300 3900 6000 3900
$Comp
L carte_coco_simple-rescue:L-Device L?
U 1 1 5CAA22CC
P 8950 3900
F 0 "L?" V 9140 3900 50  0000 C CNN
F 1 "L" V 9049 3900 50  0000 C CNN
F 2 "" H 8950 3900 50  0001 C CNN
F 3 "~" H 8950 3900 50  0001 C CNN
	1    8950 3900
	0    -1   -1   0   
$EndComp
$Comp
L carte_coco_simple-rescue:GND-power #PWR?
U 1 1 5CAA2EAD
P 8600 4400
F 0 "#PWR?" H 8600 4150 50  0001 C CNN
F 1 "GND" H 8605 4227 50  0000 C CNN
F 2 "" H 8600 4400 50  0001 C CNN
F 3 "" H 8600 4400 50  0001 C CNN
	1    8600 4400
	1    0    0    -1  
$EndComp
$Comp
L carte_coco_simple-rescue:C-Device C?
U 1 1 5CAA2EB3
P 8600 4200
F 0 "C?" H 8715 4246 50  0000 L CNN
F 1 "C" H 8715 4155 50  0000 L CNN
F 2 "" H 8638 4050 50  0001 C CNN
F 3 "~" H 8600 4200 50  0001 C CNN
	1    8600 4200
	1    0    0    -1  
$EndComp
Wire Wire Line
	8600 4350 8600 4400
$Comp
L carte_coco_simple-rescue:GND-power #PWR?
U 1 1 5CAA2FE9
P 9300 4400
F 0 "#PWR?" H 9300 4150 50  0001 C CNN
F 1 "GND" H 9305 4227 50  0000 C CNN
F 2 "" H 9300 4400 50  0001 C CNN
F 3 "" H 9300 4400 50  0001 C CNN
	1    9300 4400
	1    0    0    -1  
$EndComp
$Comp
L carte_coco_simple-rescue:C-Device C?
U 1 1 5CAA2FEF
P 9300 4200
F 0 "C?" H 9415 4246 50  0000 L CNN
F 1 "C" H 9415 4155 50  0000 L CNN
F 2 "" H 9338 4050 50  0001 C CNN
F 3 "~" H 9300 4200 50  0001 C CNN
	1    9300 4200
	1    0    0    -1  
$EndComp
Wire Wire Line
	9300 4350 9300 4400
Wire Wire Line
	8600 4050 8600 3900
Wire Wire Line
	10100 3900 10100 3500
$Comp
L carte_coco_simple-rescue:VCC-power #PWR?
U 1 1 5CAA3534
P 10100 3500
F 0 "#PWR?" H 10100 3350 50  0001 C CNN
F 1 "VCC" H 10117 3673 50  0000 C CNN
F 2 "" H 10100 3500 50  0001 C CNN
F 3 "" H 10100 3500 50  0001 C CNN
	1    10100 3500
	1    0    0    -1  
$EndComp
$Comp
L carte_coco_simple-rescue:LTC3127_MSOP-Converter_DCDC U?
U 1 1 5CAC697A
P 12700 10200
F 0 "U?" H 12850 10100 79  0000 C CNN
F 1 "LTC3127_MSOP" H 13200 9900 79  0000 C CNN
F 2 "" H 12800 10050 79  0001 C CNN
F 3 "" H 12800 10050 79  0001 C CNN
	1    12700 10200
	1    0    0    -1  
$EndComp
$Comp
L carte_coco_simple-rescue:GND-power #PWR?
U 1 1 5CAC6981
P 14000 10500
F 0 "#PWR?" H 14000 10250 50  0001 C CNN
F 1 "GND" H 14005 10327 50  0000 C CNN
F 2 "" H 14000 10500 50  0001 C CNN
F 3 "" H 14000 10500 50  0001 C CNN
	1    14000 10500
	1    0    0    -1  
$EndComp
Wire Wire Line
	13700 10300 13700 10400
Wire Wire Line
	13700 10400 13900 10400
Wire Wire Line
	14300 10400 14300 10300
Wire Wire Line
	14100 10300 14100 10400
Connection ~ 14100 10400
Wire Wire Line
	14100 10400 14300 10400
Wire Wire Line
	13900 10300 13900 10400
Connection ~ 13900 10400
Wire Wire Line
	13900 10400 14000 10400
Wire Wire Line
	14000 10400 14000 10500
Connection ~ 14000 10400
Wire Wire Line
	14000 10400 14100 10400
$Comp
L carte_coco_simple-rescue:GND-power #PWR?
U 1 1 5CAC6993
P 11100 8750
F 0 "#PWR?" H 11100 8500 50  0001 C CNN
F 1 "GND" H 11105 8577 50  0000 C CNN
F 2 "" H 11100 8750 50  0001 C CNN
F 3 "" H 11100 8750 50  0001 C CNN
	1    11100 8750
	1    0    0    -1  
$EndComp
$Comp
L carte_coco_simple-rescue:C-Device C?
U 1 1 5CAC6999
P 11100 8550
F 0 "C?" H 11215 8596 50  0000 L CNN
F 1 "C" H 11215 8505 50  0000 L CNN
F 2 "" H 11138 8400 50  0001 C CNN
F 3 "~" H 11100 8550 50  0001 C CNN
	1    11100 8550
	1    0    0    -1  
$EndComp
Wire Wire Line
	11100 8700 11100 8750
$Comp
L carte_coco_simple-rescue:GND-power #PWR?
U 1 1 5CAC69A1
P 15850 8850
F 0 "#PWR?" H 15850 8600 50  0001 C CNN
F 1 "GND" H 15855 8677 50  0000 C CNN
F 2 "" H 15850 8850 50  0001 C CNN
F 3 "" H 15850 8850 50  0001 C CNN
	1    15850 8850
	1    0    0    -1  
$EndComp
$Comp
L carte_coco_simple-rescue:C-Device C?
U 1 1 5CAC69A7
P 15850 8650
F 0 "C?" H 15965 8696 50  0000 L CNN
F 1 "C" H 15965 8605 50  0000 L CNN
F 2 "" H 15888 8500 50  0001 C CNN
F 3 "~" H 15850 8650 50  0001 C CNN
	1    15850 8650
	1    0    0    -1  
$EndComp
Wire Wire Line
	15850 8800 15850 8850
$Comp
L carte_coco_simple-rescue:GND-power #PWR?
U 1 1 5CAC69AF
P 16350 8850
F 0 "#PWR?" H 16350 8600 50  0001 C CNN
F 1 "GND" H 16355 8677 50  0000 C CNN
F 2 "" H 16350 8850 50  0001 C CNN
F 3 "" H 16350 8850 50  0001 C CNN
	1    16350 8850
	1    0    0    -1  
$EndComp
$Comp
L carte_coco_simple-rescue:C-Device C?
U 1 1 5CAC69B5
P 16350 8650
F 0 "C?" H 16465 8696 50  0000 L CNN
F 1 "C" H 16465 8605 50  0000 L CNN
F 2 "" H 16388 8500 50  0001 C CNN
F 3 "~" H 16350 8650 50  0001 C CNN
	1    16350 8650
	1    0    0    -1  
$EndComp
Wire Wire Line
	16350 8800 16350 8850
$Comp
L carte_coco_simple-rescue:GND-power #PWR?
U 1 1 5CAC69BD
P 17250 8850
F 0 "#PWR?" H 17250 8600 50  0001 C CNN
F 1 "GND" H 17255 8677 50  0000 C CNN
F 2 "" H 17250 8850 50  0001 C CNN
F 3 "" H 17250 8850 50  0001 C CNN
	1    17250 8850
	1    0    0    -1  
$EndComp
$Comp
L carte_coco_simple-rescue:C-Device C?
U 1 1 5CAC69C3
P 17250 8650
F 0 "C?" H 17365 8696 50  0000 L CNN
F 1 "C" H 17365 8605 50  0000 L CNN
F 2 "" H 17288 8500 50  0001 C CNN
F 3 "~" H 17250 8650 50  0001 C CNN
	1    17250 8650
	1    0    0    -1  
$EndComp
Wire Wire Line
	17250 8800 17250 8850
$Comp
L carte_coco_simple-rescue:L-Device L?
U 1 1 5CAC69CB
P 11550 8300
F 0 "L?" V 11740 8300 50  0000 C CNN
F 1 "L" V 11649 8300 50  0000 C CNN
F 2 "" H 11550 8300 50  0001 C CNN
F 3 "~" H 11550 8300 50  0001 C CNN
	1    11550 8300
	0    -1   -1   0   
$EndComp
$Comp
L carte_coco_simple-rescue:L-Device L?
U 1 1 5CAC69D2
P 13650 7800
F 0 "L?" V 13840 7800 50  0000 C CNN
F 1 "L" V 13749 7800 50  0000 C CNN
F 2 "" H 13650 7800 50  0001 C CNN
F 3 "~" H 13650 7800 50  0001 C CNN
	1    13650 7800
	0    -1   -1   0   
$EndComp
$Comp
L carte_coco_simple-rescue:L-Device L?
U 1 1 5CAC69D9
P 16800 8300
F 0 "L?" V 16990 8300 50  0000 C CNN
F 1 "L" V 16899 8300 50  0000 C CNN
F 2 "" H 16800 8300 50  0001 C CNN
F 3 "~" H 16800 8300 50  0001 C CNN
	1    16800 8300
	0    -1   -1   0   
$EndComp
Wire Wire Line
	13100 7900 13100 7800
Wire Wire Line
	13100 7800 13500 7800
Wire Wire Line
	13800 7800 14250 7800
Wire Wire Line
	14250 7800 14250 7900
$Comp
L carte_coco_simple-rescue:GND-power #PWR?
U 1 1 5CAC69E4
P 11900 8750
F 0 "#PWR?" H 11900 8500 50  0001 C CNN
F 1 "GND" H 11905 8577 50  0000 C CNN
F 2 "" H 11900 8750 50  0001 C CNN
F 3 "" H 11900 8750 50  0001 C CNN
	1    11900 8750
	1    0    0    -1  
$EndComp
$Comp
L carte_coco_simple-rescue:C-Device C?
U 1 1 5CAC69EA
P 11900 8550
F 0 "C?" H 12015 8596 50  0000 L CNN
F 1 "C" H 12015 8505 50  0000 L CNN
F 2 "" H 11938 8400 50  0001 C CNN
F 3 "~" H 11900 8550 50  0001 C CNN
	1    11900 8550
	1    0    0    -1  
$EndComp
Wire Wire Line
	11900 8700 11900 8750
$Comp
L carte_coco_simple-rescue:R-Device R?
U 1 1 5CAC69F2
P 15250 8750
F 0 "R?" H 15320 8796 50  0000 L CNN
F 1 "R" H 15320 8705 50  0000 L CNN
F 2 "" V 15180 8750 50  0001 C CNN
F 3 "~" H 15250 8750 50  0001 C CNN
	1    15250 8750
	1    0    0    -1  
$EndComp
$Comp
L carte_coco_simple-rescue:R-Device R?
U 1 1 5CAC69F9
P 15250 9250
F 0 "R?" H 15320 9296 50  0000 L CNN
F 1 "R" H 15320 9205 50  0000 L CNN
F 2 "" V 15180 9250 50  0001 C CNN
F 3 "~" H 15250 9250 50  0001 C CNN
	1    15250 9250
	1    0    0    -1  
$EndComp
Wire Wire Line
	15250 8900 15250 9000
Wire Wire Line
	15250 9000 14750 9000
Connection ~ 15250 9000
Wire Wire Line
	15250 9000 15250 9100
Wire Wire Line
	14750 8300 15250 8300
Wire Wire Line
	15250 8600 15250 8300
Connection ~ 15250 8300
Wire Wire Line
	15250 8300 15850 8300
$Comp
L carte_coco_simple-rescue:GND-power #PWR?
U 1 1 5CAC6A08
P 15250 9500
F 0 "#PWR?" H 15250 9250 50  0001 C CNN
F 1 "GND" H 15255 9327 50  0000 C CNN
F 2 "" H 15250 9500 50  0001 C CNN
F 3 "" H 15250 9500 50  0001 C CNN
	1    15250 9500
	1    0    0    -1  
$EndComp
Wire Wire Line
	15250 9500 15250 9400
Wire Wire Line
	15850 8500 15850 8300
Connection ~ 15850 8300
Wire Wire Line
	15850 8300 16350 8300
Wire Wire Line
	16950 8300 17250 8300
Wire Wire Line
	17250 8300 17250 8500
Wire Wire Line
	16350 8500 16350 8300
Connection ~ 16350 8300
Wire Wire Line
	16350 8300 16650 8300
Wire Wire Line
	12600 8300 12500 8300
Wire Wire Line
	11900 8400 11900 8300
Connection ~ 11900 8300
Wire Wire Line
	11900 8300 11700 8300
Wire Wire Line
	11100 8400 11100 8300
Wire Wire Line
	11100 8300 11400 8300
$Comp
L carte_coco_simple-rescue:GND-power #PWR?
U 1 1 5CAC6A1D
P 14900 10450
F 0 "#PWR?" H 14900 10200 50  0001 C CNN
F 1 "GND" H 14905 10277 50  0000 C CNN
F 2 "" H 14900 10450 50  0001 C CNN
F 3 "" H 14900 10450 50  0001 C CNN
	1    14900 10450
	1    0    0    -1  
$EndComp
$Comp
L carte_coco_simple-rescue:C-Device C?
U 1 1 5CAC6A23
P 14900 10250
F 0 "C?" H 15015 10296 50  0000 L CNN
F 1 "C" H 15015 10205 50  0000 L CNN
F 2 "" H 14938 10100 50  0001 C CNN
F 3 "~" H 14900 10250 50  0001 C CNN
	1    14900 10250
	1    0    0    -1  
$EndComp
Wire Wire Line
	14900 10400 14900 10450
$Comp
L carte_coco_simple-rescue:R-Device R?
U 1 1 5CAC6A2B
P 15400 10250
F 0 "R?" H 15470 10296 50  0000 L CNN
F 1 "R" H 15470 10205 50  0000 L CNN
F 2 "" V 15330 10250 50  0001 C CNN
F 3 "~" H 15400 10250 50  0001 C CNN
	1    15400 10250
	1    0    0    -1  
$EndComp
$Comp
L carte_coco_simple-rescue:GND-power #PWR?
U 1 1 5CAC6A32
P 15400 10500
F 0 "#PWR?" H 15400 10250 50  0001 C CNN
F 1 "GND" H 15405 10327 50  0000 C CNN
F 2 "" H 15400 10500 50  0001 C CNN
F 3 "" H 15400 10500 50  0001 C CNN
	1    15400 10500
	1    0    0    -1  
$EndComp
Wire Wire Line
	15400 10500 15400 10400
Wire Wire Line
	15400 10100 15400 10000
Wire Wire Line
	15400 10000 14900 10000
Wire Wire Line
	14900 10100 14900 10000
Connection ~ 14900 10000
Wire Wire Line
	14900 10000 14750 10000
$Comp
L carte_coco_simple-rescue:R-Device R?
U 1 1 5CAC6A3E
P 12450 10250
F 0 "R?" H 12150 10300 50  0000 L CNN
F 1 "R" H 12150 10200 50  0000 L CNN
F 2 "" V 12380 10250 50  0001 C CNN
F 3 "~" H 12450 10250 50  0001 C CNN
	1    12450 10250
	1    0    0    -1  
$EndComp
$Comp
L carte_coco_simple-rescue:GND-power #PWR?
U 1 1 5CAC6A45
P 12450 10500
F 0 "#PWR?" H 12450 10250 50  0001 C CNN
F 1 "GND" H 12455 10327 50  0000 C CNN
F 2 "" H 12450 10500 50  0001 C CNN
F 3 "" H 12450 10500 50  0001 C CNN
	1    12450 10500
	1    0    0    -1  
$EndComp
Wire Wire Line
	12450 10500 12450 10400
Wire Wire Line
	12450 10100 12450 10000
Wire Wire Line
	12450 10000 12600 10000
Wire Wire Line
	12600 9600 12450 9600
Wire Wire Line
	12450 9600 12450 9650
$Comp
L carte_coco_simple-rescue:GND-power #PWR?
U 1 1 5CAC6A50
P 12450 9650
F 0 "#PWR?" H 12450 9400 50  0001 C CNN
F 1 "GND" H 12455 9477 50  0000 C CNN
F 2 "" H 12450 9650 50  0001 C CNN
F 3 "" H 12450 9650 50  0001 C CNN
	1    12450 9650
	1    0    0    -1  
$EndComp
$Comp
L carte_coco_simple-rescue:R-Device R?
U 1 1 5CAC6A56
P 12500 8800
F 0 "R?" H 12200 8850 50  0000 L CNN
F 1 "R" H 12200 8750 50  0000 L CNN
F 2 "" V 12430 8800 50  0001 C CNN
F 3 "~" H 12500 8800 50  0001 C CNN
	1    12500 8800
	1    0    0    -1  
$EndComp
Wire Wire Line
	12600 9200 12500 9200
Wire Wire Line
	12500 9200 12500 8950
Wire Wire Line
	12500 8300 12500 8650
Connection ~ 12500 8300
Wire Wire Line
	12500 8300 11900 8300
Wire Wire Line
	10100 3900 10300 3900
Wire Wire Line
	11100 8300 10300 8300
Wire Wire Line
	10300 8300 10300 3900
Connection ~ 11100 8300
Wire Wire Line
	18100 8300 18100 8000
Wire Notes Line
	17700 7400 17700 10900
Wire Notes Line
	17700 10900 10700 10900
Wire Notes Line
	10700 10900 10700 7400
Wire Notes Line
	10700 7400 17700 7400
Wire Wire Line
	17250 8300 18100 8300
Connection ~ 17250 8300
$Comp
L carte_coco_simple-rescue:+3.3V-power #PWR?
U 1 1 5CAE3279
P 18100 8000
F 0 "#PWR?" H 18100 7850 50  0001 C CNN
F 1 "+3.3V" H 18115 8173 50  0000 C CNN
F 2 "" H 18100 8000 50  0001 C CNN
F 3 "" H 18100 8000 50  0001 C CNN
	1    18100 8000
	1    0    0    -1  
$EndComp
Wire Wire Line
	18100 8300 18300 8300
Connection ~ 18100 8300
Text GLabel 18600 8300 2    79   UnSpc ~ 16
P3V3
$Comp
L carte_coco_simple-rescue:LTC3127_MSOP-Converter_DCDC U?
U 1 1 5CAEB121
P 12700 5800
F 0 "U?" H 12850 5700 79  0000 C CNN
F 1 "LTC3127_MSOP" H 13200 5500 79  0000 C CNN
F 2 "" H 12800 5650 79  0001 C CNN
F 3 "" H 12800 5650 79  0001 C CNN
	1    12700 5800
	1    0    0    -1  
$EndComp
$Comp
L carte_coco_simple-rescue:GND-power #PWR?
U 1 1 5CAEB128
P 14000 6100
F 0 "#PWR?" H 14000 5850 50  0001 C CNN
F 1 "GND" H 14005 5927 50  0000 C CNN
F 2 "" H 14000 6100 50  0001 C CNN
F 3 "" H 14000 6100 50  0001 C CNN
	1    14000 6100
	1    0    0    -1  
$EndComp
Wire Wire Line
	13700 5900 13700 6000
Wire Wire Line
	13700 6000 13900 6000
Wire Wire Line
	14300 6000 14300 5900
Wire Wire Line
	14100 5900 14100 6000
Connection ~ 14100 6000
Wire Wire Line
	14100 6000 14300 6000
Wire Wire Line
	13900 5900 13900 6000
Connection ~ 13900 6000
Wire Wire Line
	13900 6000 14000 6000
Wire Wire Line
	14000 6000 14000 6100
Connection ~ 14000 6000
Wire Wire Line
	14000 6000 14100 6000
$Comp
L carte_coco_simple-rescue:GND-power #PWR?
U 1 1 5CAEB13A
P 11100 4350
F 0 "#PWR?" H 11100 4100 50  0001 C CNN
F 1 "GND" H 11105 4177 50  0000 C CNN
F 2 "" H 11100 4350 50  0001 C CNN
F 3 "" H 11100 4350 50  0001 C CNN
	1    11100 4350
	1    0    0    -1  
$EndComp
$Comp
L carte_coco_simple-rescue:C-Device C?
U 1 1 5CAEB140
P 11100 4150
F 0 "C?" H 11215 4196 50  0000 L CNN
F 1 "C" H 11215 4105 50  0000 L CNN
F 2 "" H 11138 4000 50  0001 C CNN
F 3 "~" H 11100 4150 50  0001 C CNN
	1    11100 4150
	1    0    0    -1  
$EndComp
Wire Wire Line
	11100 4300 11100 4350
$Comp
L carte_coco_simple-rescue:GND-power #PWR?
U 1 1 5CAEB148
P 15850 4450
F 0 "#PWR?" H 15850 4200 50  0001 C CNN
F 1 "GND" H 15855 4277 50  0000 C CNN
F 2 "" H 15850 4450 50  0001 C CNN
F 3 "" H 15850 4450 50  0001 C CNN
	1    15850 4450
	1    0    0    -1  
$EndComp
$Comp
L carte_coco_simple-rescue:C-Device C?
U 1 1 5CAEB14E
P 15850 4250
F 0 "C?" H 15965 4296 50  0000 L CNN
F 1 "C" H 15965 4205 50  0000 L CNN
F 2 "" H 15888 4100 50  0001 C CNN
F 3 "~" H 15850 4250 50  0001 C CNN
	1    15850 4250
	1    0    0    -1  
$EndComp
Wire Wire Line
	15850 4400 15850 4450
$Comp
L carte_coco_simple-rescue:GND-power #PWR?
U 1 1 5CAEB156
P 16350 4450
F 0 "#PWR?" H 16350 4200 50  0001 C CNN
F 1 "GND" H 16355 4277 50  0000 C CNN
F 2 "" H 16350 4450 50  0001 C CNN
F 3 "" H 16350 4450 50  0001 C CNN
	1    16350 4450
	1    0    0    -1  
$EndComp
$Comp
L carte_coco_simple-rescue:C-Device C?
U 1 1 5CAEB15C
P 16350 4250
F 0 "C?" H 16465 4296 50  0000 L CNN
F 1 "C" H 16465 4205 50  0000 L CNN
F 2 "" H 16388 4100 50  0001 C CNN
F 3 "~" H 16350 4250 50  0001 C CNN
	1    16350 4250
	1    0    0    -1  
$EndComp
Wire Wire Line
	16350 4400 16350 4450
$Comp
L carte_coco_simple-rescue:GND-power #PWR?
U 1 1 5CAEB164
P 17250 4450
F 0 "#PWR?" H 17250 4200 50  0001 C CNN
F 1 "GND" H 17255 4277 50  0000 C CNN
F 2 "" H 17250 4450 50  0001 C CNN
F 3 "" H 17250 4450 50  0001 C CNN
	1    17250 4450
	1    0    0    -1  
$EndComp
$Comp
L carte_coco_simple-rescue:C-Device C?
U 1 1 5CAEB16A
P 17250 4250
F 0 "C?" H 17365 4296 50  0000 L CNN
F 1 "C" H 17365 4205 50  0000 L CNN
F 2 "" H 17288 4100 50  0001 C CNN
F 3 "~" H 17250 4250 50  0001 C CNN
	1    17250 4250
	1    0    0    -1  
$EndComp
Wire Wire Line
	17250 4400 17250 4450
$Comp
L carte_coco_simple-rescue:L-Device L?
U 1 1 5CAEB172
P 11550 3900
F 0 "L?" V 11740 3900 50  0000 C CNN
F 1 "L" V 11649 3900 50  0000 C CNN
F 2 "" H 11550 3900 50  0001 C CNN
F 3 "~" H 11550 3900 50  0001 C CNN
	1    11550 3900
	0    -1   -1   0   
$EndComp
$Comp
L carte_coco_simple-rescue:L-Device L?
U 1 1 5CAEB179
P 13650 3400
F 0 "L?" V 13840 3400 50  0000 C CNN
F 1 "L" V 13749 3400 50  0000 C CNN
F 2 "" H 13650 3400 50  0001 C CNN
F 3 "~" H 13650 3400 50  0001 C CNN
	1    13650 3400
	0    -1   -1   0   
$EndComp
$Comp
L carte_coco_simple-rescue:L-Device L?
U 1 1 5CAEB180
P 16800 3900
F 0 "L?" V 16990 3900 50  0000 C CNN
F 1 "L" V 16899 3900 50  0000 C CNN
F 2 "" H 16800 3900 50  0001 C CNN
F 3 "~" H 16800 3900 50  0001 C CNN
	1    16800 3900
	0    -1   -1   0   
$EndComp
Wire Wire Line
	13100 3500 13100 3400
Wire Wire Line
	13100 3400 13500 3400
Wire Wire Line
	13800 3400 14250 3400
Wire Wire Line
	14250 3400 14250 3500
$Comp
L carte_coco_simple-rescue:GND-power #PWR?
U 1 1 5CAEB18B
P 11900 4350
F 0 "#PWR?" H 11900 4100 50  0001 C CNN
F 1 "GND" H 11905 4177 50  0000 C CNN
F 2 "" H 11900 4350 50  0001 C CNN
F 3 "" H 11900 4350 50  0001 C CNN
	1    11900 4350
	1    0    0    -1  
$EndComp
$Comp
L carte_coco_simple-rescue:C-Device C?
U 1 1 5CAEB191
P 11900 4150
F 0 "C?" H 12015 4196 50  0000 L CNN
F 1 "C" H 12015 4105 50  0000 L CNN
F 2 "" H 11938 4000 50  0001 C CNN
F 3 "~" H 11900 4150 50  0001 C CNN
	1    11900 4150
	1    0    0    -1  
$EndComp
Wire Wire Line
	11900 4300 11900 4350
$Comp
L carte_coco_simple-rescue:R-Device R?
U 1 1 5CAEB199
P 15250 4350
F 0 "R?" H 15320 4396 50  0000 L CNN
F 1 "R" H 15320 4305 50  0000 L CNN
F 2 "" V 15180 4350 50  0001 C CNN
F 3 "~" H 15250 4350 50  0001 C CNN
	1    15250 4350
	1    0    0    -1  
$EndComp
$Comp
L carte_coco_simple-rescue:R-Device R?
U 1 1 5CAEB1A0
P 15250 4850
F 0 "R?" H 15320 4896 50  0000 L CNN
F 1 "R" H 15320 4805 50  0000 L CNN
F 2 "" V 15180 4850 50  0001 C CNN
F 3 "~" H 15250 4850 50  0001 C CNN
	1    15250 4850
	1    0    0    -1  
$EndComp
Wire Wire Line
	15250 4500 15250 4600
Wire Wire Line
	15250 4600 14750 4600
Connection ~ 15250 4600
Wire Wire Line
	15250 4600 15250 4700
Wire Wire Line
	14750 3900 15250 3900
Wire Wire Line
	15250 4200 15250 3900
Connection ~ 15250 3900
Wire Wire Line
	15250 3900 15850 3900
$Comp
L carte_coco_simple-rescue:GND-power #PWR?
U 1 1 5CAEB1AF
P 15250 5100
F 0 "#PWR?" H 15250 4850 50  0001 C CNN
F 1 "GND" H 15255 4927 50  0000 C CNN
F 2 "" H 15250 5100 50  0001 C CNN
F 3 "" H 15250 5100 50  0001 C CNN
	1    15250 5100
	1    0    0    -1  
$EndComp
Wire Wire Line
	15250 5100 15250 5000
Wire Wire Line
	15850 4100 15850 3900
Connection ~ 15850 3900
Wire Wire Line
	15850 3900 16350 3900
Wire Wire Line
	16950 3900 17250 3900
Wire Wire Line
	17250 3900 17250 4100
Wire Wire Line
	16350 4100 16350 3900
Connection ~ 16350 3900
Wire Wire Line
	16350 3900 16650 3900
Wire Wire Line
	12600 3900 12500 3900
Wire Wire Line
	11900 4000 11900 3900
Connection ~ 11900 3900
Wire Wire Line
	11900 3900 11700 3900
Wire Wire Line
	11100 4000 11100 3900
Wire Wire Line
	11100 3900 11400 3900
$Comp
L carte_coco_simple-rescue:GND-power #PWR?
U 1 1 5CAEB1C4
P 14900 6050
F 0 "#PWR?" H 14900 5800 50  0001 C CNN
F 1 "GND" H 14905 5877 50  0000 C CNN
F 2 "" H 14900 6050 50  0001 C CNN
F 3 "" H 14900 6050 50  0001 C CNN
	1    14900 6050
	1    0    0    -1  
$EndComp
$Comp
L carte_coco_simple-rescue:C-Device C?
U 1 1 5CAEB1CA
P 14900 5850
F 0 "C?" H 15015 5896 50  0000 L CNN
F 1 "C" H 15015 5805 50  0000 L CNN
F 2 "" H 14938 5700 50  0001 C CNN
F 3 "~" H 14900 5850 50  0001 C CNN
	1    14900 5850
	1    0    0    -1  
$EndComp
Wire Wire Line
	14900 6000 14900 6050
$Comp
L carte_coco_simple-rescue:R-Device R?
U 1 1 5CAEB1D2
P 15400 5850
F 0 "R?" H 15470 5896 50  0000 L CNN
F 1 "R" H 15470 5805 50  0000 L CNN
F 2 "" V 15330 5850 50  0001 C CNN
F 3 "~" H 15400 5850 50  0001 C CNN
	1    15400 5850
	1    0    0    -1  
$EndComp
$Comp
L carte_coco_simple-rescue:GND-power #PWR?
U 1 1 5CAEB1D9
P 15400 6100
F 0 "#PWR?" H 15400 5850 50  0001 C CNN
F 1 "GND" H 15405 5927 50  0000 C CNN
F 2 "" H 15400 6100 50  0001 C CNN
F 3 "" H 15400 6100 50  0001 C CNN
	1    15400 6100
	1    0    0    -1  
$EndComp
Wire Wire Line
	15400 6100 15400 6000
Wire Wire Line
	15400 5700 15400 5600
Wire Wire Line
	15400 5600 14900 5600
Wire Wire Line
	14900 5700 14900 5600
Connection ~ 14900 5600
Wire Wire Line
	14900 5600 14750 5600
$Comp
L carte_coco_simple-rescue:R-Device R?
U 1 1 5CAEB1E5
P 12450 5850
F 0 "R?" H 12150 5900 50  0000 L CNN
F 1 "R" H 12150 5800 50  0000 L CNN
F 2 "" V 12380 5850 50  0001 C CNN
F 3 "~" H 12450 5850 50  0001 C CNN
	1    12450 5850
	1    0    0    -1  
$EndComp
$Comp
L carte_coco_simple-rescue:GND-power #PWR?
U 1 1 5CAEB1EC
P 12450 6100
F 0 "#PWR?" H 12450 5850 50  0001 C CNN
F 1 "GND" H 12455 5927 50  0000 C CNN
F 2 "" H 12450 6100 50  0001 C CNN
F 3 "" H 12450 6100 50  0001 C CNN
	1    12450 6100
	1    0    0    -1  
$EndComp
Wire Wire Line
	12450 6100 12450 6000
Wire Wire Line
	12450 5700 12450 5600
Wire Wire Line
	12450 5600 12600 5600
Wire Wire Line
	12600 5200 12450 5200
Wire Wire Line
	12450 5200 12450 5250
$Comp
L carte_coco_simple-rescue:GND-power #PWR?
U 1 1 5CAEB1F7
P 12450 5250
F 0 "#PWR?" H 12450 5000 50  0001 C CNN
F 1 "GND" H 12455 5077 50  0000 C CNN
F 2 "" H 12450 5250 50  0001 C CNN
F 3 "" H 12450 5250 50  0001 C CNN
	1    12450 5250
	1    0    0    -1  
$EndComp
$Comp
L carte_coco_simple-rescue:R-Device R?
U 1 1 5CAEB1FD
P 12500 4400
F 0 "R?" H 12200 4450 50  0000 L CNN
F 1 "R" H 12200 4350 50  0000 L CNN
F 2 "" V 12430 4400 50  0001 C CNN
F 3 "~" H 12500 4400 50  0001 C CNN
	1    12500 4400
	1    0    0    -1  
$EndComp
Wire Wire Line
	12600 4800 12500 4800
Wire Wire Line
	12500 4800 12500 4550
Wire Wire Line
	12500 3900 12500 4250
Connection ~ 12500 3900
Wire Wire Line
	12500 3900 11900 3900
Wire Wire Line
	11100 3900 10300 3900
Connection ~ 11100 3900
Wire Wire Line
	18100 3900 18100 3600
Wire Notes Line
	17700 3000 17700 6500
Wire Notes Line
	17700 6500 10700 6500
Wire Notes Line
	10700 6500 10700 3000
Wire Notes Line
	10700 3000 17700 3000
Wire Wire Line
	17250 3900 18100 3900
Connection ~ 17250 3900
Wire Wire Line
	18100 3900 18300 3900
Connection ~ 18100 3900
Text GLabel 18600 3900 2    79   UnSpc ~ 16
P5V
Text Notes 10750 2950 0    79   ~ 16
P5V_Main\nAlimentation Principale +5V\n+5V Main Power Supply 
Text Notes 10700 7350 0    79   ~ 16
P3V3_Main\nAlimentation Principale +3,3V\n+3.3V Main Power Supply 
$Comp
L carte_coco_simple-rescue:+5V-power #PWR?
U 1 1 5CAF8E42
P 18100 3600
F 0 "#PWR?" H 18100 3450 50  0001 C CNN
F 1 "+5V" H 18115 3773 50  0000 C CNN
F 2 "" H 18100 3600 50  0001 C CNN
F 3 "" H 18100 3600 50  0001 C CNN
	1    18100 3600
	1    0    0    -1  
$EndComp
Connection ~ 10300 3900
Wire Notes Line
	8350 3500 9850 3500
Wire Notes Line
	9850 3500 9850 4800
Wire Notes Line
	9850 4800 8350 4800
Wire Notes Line
	8350 4800 8350 3500
Wire Notes Line
	6500 3500 6500 5600
Wire Notes Line
	6500 5600 4400 5600
Wire Notes Line
	4400 5600 4400 3500
Wire Notes Line
	4400 3500 6500 3500
Text Notes 8400 3450 0    79   ~ 16
Filtre d’Entrée\nInput Filter
Text Notes 4450 3400 0    79   ~ 16
Protection d’Entrée ; 2 Voies\nInput Protection ; 2 Channels
Text Notes 10500 1650 0    79   ~ 16
Blindage\nShielding
$Comp
L Device:RF_Shield_One_Piece J?
U 1 1 5CB49941
P 14600 2200
F 0 "J?" H 15230 2191 50  0000 L CNN
F 1 "RF_Shield_One_Piece" H 15230 2100 50  0000 L CNN
F 2 "" H 14600 2100 50  0001 C CNN
F 3 "~" H 14600 2100 50  0001 C CNN
	1    14600 2200
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5CB49A1B
P 14600 2600
F 0 "#PWR?" H 14600 2350 50  0001 C CNN
F 1 "GND" H 14605 2427 50  0000 C CNN
F 2 "" H 14600 2600 50  0001 C CNN
F 3 "" H 14600 2600 50  0001 C CNN
	1    14600 2600
	1    0    0    -1  
$EndComp
Wire Notes Line width 12 style dash_dot rgb(194, 101, 0)
	17900 1700 10500 1700
Wire Notes Line width 12 style dash_dot rgb(195, 101, 0)
	10500 11100 17900 11100
Wire Notes Line width 12 style dash_dot rgb(194, 101, 0)
	10500 1700 10500 11100
Wire Notes Line width 12 style dash_dot rgb(194, 100, 0)
	17900 1700 17900 11100
$Comp
L Connector:TestPoint TP?
U 1 1 5CC9B1BA
P 18300 3750
F 0 "TP?" H 18358 3870 50  0000 L CNN
F 1 "TestPoint" H 18358 3779 50  0000 L CNN
F 2 "" H 18500 3750 50  0001 C CNN
F 3 "~" H 18500 3750 50  0001 C CNN
	1    18300 3750
	1    0    0    -1  
$EndComp
Wire Wire Line
	18300 3750 18300 3900
Connection ~ 18300 3900
Wire Wire Line
	18300 3900 18600 3900
$Comp
L Connector:TestPoint TP?
U 1 1 5CCB2ADE
P 18300 8150
F 0 "TP?" H 18358 8270 50  0000 L CNN
F 1 "TestPoint" H 18358 8179 50  0000 L CNN
F 2 "" H 18500 8150 50  0001 C CNN
F 3 "~" H 18500 8150 50  0001 C CNN
	1    18300 8150
	1    0    0    -1  
$EndComp
Wire Wire Line
	18300 8150 18300 8300
Connection ~ 18300 8300
Wire Wire Line
	18300 8300 18600 8300
$Comp
L Switch:SW_SPST SW?
U 1 1 5CCC72BD
P 7450 3900
F 0 "SW?" H 7450 4135 50  0000 C CNN
F 1 "SW_SPST" H 7450 4044 50  0000 C CNN
F 2 "" H 7450 3900 50  0001 C CNN
F 3 "" H 7450 3900 50  0001 C CNN
	1    7450 3900
	1    0    0    -1  
$EndComp
Wire Wire Line
	8600 3900 8800 3900
Wire Wire Line
	10100 3900 9300 3900
Connection ~ 10100 3900
Wire Wire Line
	9300 4050 9300 3900
Connection ~ 9300 3900
Wire Wire Line
	9300 3900 9100 3900
Wire Wire Line
	7650 3900 8600 3900
Connection ~ 8600 3900
Wire Wire Line
	7250 3900 6300 3900
Connection ~ 6300 3900
Wire Notes Line
	7000 3500 7900 3500
Wire Notes Line
	7900 3500 7900 4200
Wire Notes Line
	7900 4200 7000 4200
Wire Notes Line
	7000 4200 7000 3500
Text Notes 7050 3450 0    79   ~ 16
ON/OFF\n
$Comp
L Regulator_Linear:TPS71701DCK U?
U 1 1 5CD7A466
P 13150 13100
F 0 "U?" H 13450 13642 50  0000 C CNN
F 1 "TPS71701DCK" H 13450 13551 50  0000 C CNN
F 2 "" H 13250 13650 50  0001 C CNN
F 3 "" H 13250 13650 50  0001 C CNN
	1    13150 13100
	1    0    0    -1  
$EndComp
$Comp
L carte_coco_simple-rescue:GND-power #PWR?
U 1 1 5CD7F852
P 14550 13350
F 0 "#PWR?" H 14550 13100 50  0001 C CNN
F 1 "GND" H 14555 13177 50  0000 C CNN
F 2 "" H 14550 13350 50  0001 C CNN
F 3 "" H 14550 13350 50  0001 C CNN
	1    14550 13350
	1    0    0    -1  
$EndComp
$Comp
L carte_coco_simple-rescue:C-Device C?
U 1 1 5CD7F858
P 14550 13150
F 0 "C?" H 14665 13196 50  0000 L CNN
F 1 "C" H 14665 13105 50  0000 L CNN
F 2 "" H 14588 13000 50  0001 C CNN
F 3 "~" H 14550 13150 50  0001 C CNN
	1    14550 13150
	1    0    0    -1  
$EndComp
Wire Wire Line
	14550 13300 14550 13350
Wire Wire Line
	14550 12800 14550 13000
Wire Wire Line
	15300 12800 15300 12500
Wire Wire Line
	14550 12800 15300 12800
Connection ~ 14550 12800
Wire Wire Line
	15300 12800 15500 12800
Connection ~ 15300 12800
Text GLabel 15800 12800 2    79   UnSpc ~ 16
P1V1
$Comp
L Connector:TestPoint TP?
U 1 1 5CD7F870
P 15500 12650
F 0 "TP?" H 15558 12770 50  0000 L CNN
F 1 "TestPoint" H 15558 12679 50  0000 L CNN
F 2 "" H 15700 12650 50  0001 C CNN
F 3 "~" H 15700 12650 50  0001 C CNN
	1    15500 12650
	1    0    0    -1  
$EndComp
Wire Wire Line
	15500 12650 15500 12800
Connection ~ 15500 12800
Wire Wire Line
	15500 12800 15800 12800
$Comp
L power:+1V1 #PWR?
U 1 1 5CD903C3
P 15300 12500
F 0 "#PWR?" H 15300 12350 50  0001 C CNN
F 1 "+1V1" H 15315 12673 50  0000 C CNN
F 2 "" H 15300 12500 50  0001 C CNN
F 3 "" H 15300 12500 50  0001 C CNN
	1    15300 12500
	1    0    0    -1  
$EndComp
$Comp
L carte_coco_simple-rescue:R-Device R?
U 1 1 5CD9B7ED
P 14050 13050
F 0 "R?" H 14120 13096 50  0000 L CNN
F 1 "R" H 14120 13005 50  0000 L CNN
F 2 "" V 13980 13050 50  0001 C CNN
F 3 "~" H 14050 13050 50  0001 C CNN
	1    14050 13050
	1    0    0    -1  
$EndComp
$Comp
L carte_coco_simple-rescue:R-Device R?
U 1 1 5CD9B7F4
P 14050 13550
F 0 "R?" H 14120 13596 50  0000 L CNN
F 1 "R" H 14120 13505 50  0000 L CNN
F 2 "" V 13980 13550 50  0001 C CNN
F 3 "~" H 14050 13550 50  0001 C CNN
	1    14050 13550
	1    0    0    -1  
$EndComp
Wire Wire Line
	14050 13200 14050 13300
Wire Wire Line
	14050 13300 13900 13300
Connection ~ 14050 13300
Wire Wire Line
	14050 13300 14050 13400
$Comp
L carte_coco_simple-rescue:GND-power #PWR?
U 1 1 5CD9B800
P 14050 13800
F 0 "#PWR?" H 14050 13550 50  0001 C CNN
F 1 "GND" H 14055 13627 50  0000 C CNN
F 2 "" H 14050 13800 50  0001 C CNN
F 3 "" H 14050 13800 50  0001 C CNN
	1    14050 13800
	1    0    0    -1  
$EndComp
Wire Wire Line
	14050 13800 14050 13700
Wire Wire Line
	13850 12950 13900 12950
Wire Wire Line
	13900 12950 13900 13300
Wire Wire Line
	13850 12800 14050 12800
Wire Wire Line
	14050 12900 14050 12800
Connection ~ 14050 12800
Wire Wire Line
	14050 12800 14550 12800
$Comp
L carte_coco_simple-rescue:GND-power #PWR?
U 1 1 5CDD7634
P 13450 13250
F 0 "#PWR?" H 13450 13000 50  0001 C CNN
F 1 "GND" H 13455 13077 50  0000 C CNN
F 2 "" H 13450 13250 50  0001 C CNN
F 3 "" H 13450 13250 50  0001 C CNN
	1    13450 13250
	1    0    0    -1  
$EndComp
Wire Wire Line
	13450 13200 13450 13250
$Comp
L carte_coco_simple-rescue:GND-power #PWR?
U 1 1 5CDEA380
P 12700 13350
F 0 "#PWR?" H 12700 13100 50  0001 C CNN
F 1 "GND" H 12705 13177 50  0000 C CNN
F 2 "" H 12700 13350 50  0001 C CNN
F 3 "" H 12700 13350 50  0001 C CNN
	1    12700 13350
	1    0    0    -1  
$EndComp
$Comp
L carte_coco_simple-rescue:C-Device C?
U 1 1 5CDEA386
P 12700 13150
F 0 "C?" H 12815 13196 50  0000 L CNN
F 1 "C" H 12815 13105 50  0000 L CNN
F 2 "" H 12738 13000 50  0001 C CNN
F 3 "~" H 12700 13150 50  0001 C CNN
	1    12700 13150
	1    0    0    -1  
$EndComp
Wire Wire Line
	12700 13300 12700 13350
Wire Wire Line
	12700 12800 12700 13000
Wire Wire Line
	12700 12800 13000 12800
Connection ~ 12700 12800
Wire Wire Line
	12200 12800 12700 12800
$Comp
L power:+3.3V #PWR?
U 1 1 5CDF0ED0
P 12200 12500
F 0 "#PWR?" H 12200 12350 50  0001 C CNN
F 1 "+3.3V" H 12215 12673 50  0000 C CNN
F 2 "" H 12200 12500 50  0001 C CNN
F 3 "" H 12200 12500 50  0001 C CNN
	1    12200 12500
	1    0    0    -1  
$EndComp
Wire Wire Line
	12200 12800 12200 12500
Wire Wire Line
	13050 12950 13000 12950
Wire Wire Line
	13000 12950 13000 12800
Connection ~ 13000 12800
Wire Wire Line
	13000 12800 13050 12800
Wire Notes Line
	15050 14100 12500 14100
Wire Notes Line
	12500 14100 12500 12400
Wire Notes Line
	12500 12400 15050 12400
Wire Notes Line
	15050 12400 15050 14100
Text Notes 12550 12300 0    79   ~ 16
P1V1\nAlimentation +1,1V\n+1.1V Power Supply 
$EndSCHEMATC
