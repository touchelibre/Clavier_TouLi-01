EESchema Schematic File Version 4
LIBS:power
LIBS:device
LIBS:switches
LIBS:relays
LIBS:motors
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:carte_coco_simple-cache
EELAYER 26 0
EELAYER END
$Descr A2 23386 16535
encoding utf-8
Sheet 3 5
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Interface_USB:TUSB4041I U?
U 1 1 5CAA9212
P 11900 6350
F 0 "U?" H 10900 4350 50  0000 C CNN
F 1 "TUSB4041I" H 11050 4250 50  0000 C CNN
F 2 "Package_QFP:HTQFP-64-1EP_10x10mm_P0.5mm_EP8x8mm_Mask4.4x4.4mm_ThermalVias" H 13100 8350 50  0001 L CNN
F 3 "http://www.ti.com/lit/ds/symlink/tusb4041i.pdf" H 11600 6550 50  0001 C CNN
	1    11900 6350
	1    0    0    -1  
$EndComp
$Comp
L power:+1V1 #PWR?
U 1 1 5CAA95EB
P 11500 3950
F 0 "#PWR?" H 11500 3800 50  0001 C CNN
F 1 "+1V1" H 11515 4123 50  0000 C CNN
F 2 "" H 11500 3950 50  0001 C CNN
F 3 "" H 11500 3950 50  0001 C CNN
	1    11500 3950
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR?
U 1 1 5CAA9627
P 11000 3950
F 0 "#PWR?" H 11000 3800 50  0001 C CNN
F 1 "+3.3V" H 11015 4123 50  0000 C CNN
F 2 "" H 11000 3950 50  0001 C CNN
F 3 "" H 11000 3950 50  0001 C CNN
	1    11000 3950
	1    0    0    -1  
$EndComp
Wire Wire Line
	11000 4250 11000 4150
Wire Wire Line
	11000 4150 11100 4150
Wire Wire Line
	11300 4150 11300 4250
Wire Wire Line
	11200 4250 11200 4150
Connection ~ 11200 4150
Wire Wire Line
	11200 4150 11300 4150
Wire Wire Line
	11100 4250 11100 4150
Connection ~ 11100 4150
Wire Wire Line
	11000 4150 11000 3950
Connection ~ 11000 4150
Wire Wire Line
	11500 4250 11500 4150
Wire Wire Line
	11500 4150 11600 4150
Wire Wire Line
	12100 4150 12100 4250
Wire Wire Line
	11600 4250 11600 4150
Connection ~ 11600 4150
Wire Wire Line
	11600 4150 11700 4150
Wire Wire Line
	11700 4250 11700 4150
Connection ~ 11700 4150
Wire Wire Line
	11800 4250 11800 4150
Connection ~ 11800 4150
Wire Wire Line
	11800 4150 11900 4150
Wire Wire Line
	11900 4250 11900 4150
Connection ~ 11900 4150
Wire Wire Line
	11900 4150 12000 4150
Wire Wire Line
	12000 4250 12000 4150
Connection ~ 12000 4150
Wire Wire Line
	12000 4150 12100 4150
Wire Wire Line
	11500 3950 11500 4150
Connection ~ 11500 4150
Wire Wire Line
	11700 4150 11800 4150
Wire Wire Line
	11100 4150 11200 4150
$Comp
L power:GND #PWR?
U 1 1 5CAAA3B8
P 11900 8550
F 0 "#PWR?" H 11900 8300 50  0001 C CNN
F 1 "GND" H 11905 8377 50  0000 C CNN
F 2 "" H 11900 8550 50  0001 C CNN
F 3 "" H 11900 8550 50  0001 C CNN
	1    11900 8550
	1    0    0    -1  
$EndComp
Wire Wire Line
	11900 8550 11900 8450
NoConn ~ 10600 6050
NoConn ~ 10600 6150
NoConn ~ 10600 6250
NoConn ~ 10600 6350
NoConn ~ 10600 6450
NoConn ~ 10600 6550
NoConn ~ 10600 6650
NoConn ~ 10600 6750
NoConn ~ 10600 6850
NoConn ~ 10600 6950
NoConn ~ 10600 7050
NoConn ~ 13200 7050
NoConn ~ 13200 7150
NoConn ~ 13200 7250
NoConn ~ 13200 7350
NoConn ~ 13200 7450
NoConn ~ 13200 7550
NoConn ~ 13200 7650
NoConn ~ 13200 7750
NoConn ~ 13200 7850
NoConn ~ 13200 8050
NoConn ~ 13200 8150
$Comp
L Interface_USB:TUSB2036 U?
U 1 1 5CE44459
P 15350 6350
F 0 "U?" H 15350 8128 50  0000 C CNN
F 1 "TUSB2036" H 15350 8037 50  0000 C CNN
F 2 "Package_QFP:LQFP-32_7x7mm_P0.8mm" H 16400 4850 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/tusb2036.pdf" H 15350 6350 50  0001 C CNN
	1    15350 6350
	1    0    0    -1  
$EndComp
$EndSCHEMATC
