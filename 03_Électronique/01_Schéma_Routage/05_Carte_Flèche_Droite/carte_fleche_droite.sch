EESchema Schematic File Version 4
LIBS:carte_fleche_droite-cache
EELAYER 30 0
EELAYER END
$Descr A2 23386 16535
encoding utf-8
Sheet 1 2
Title "Schematics"
Date "20 oct 2019"
Rev "B"
Comp "Lilian T."
Comment1 "TL 500 005"
Comment2 "Clavier ToucheLibre"
Comment3 "Right Arrows Board"
Comment4 "Tracking Sheet"
$EndDescr
$Sheet
S 15650 14600 1700 650 
U 5BBF8496
F0 "touche_fleche" 60
F1 "touche_fleche.sch" 60
$EndSheet
Wire Notes Line style solid
	2500 3000 9700 3000
Wire Notes Line style solid
	2500 3000 2500 7500
Wire Notes Line style solid
	2500 3300 9700 3300
Text Notes 5000 3200 0    79   ~ 16
Core Design Tracking
Wire Notes Line style solid
	2500 4500 9700 4500
Wire Notes Line style solid
	2900 4200 2900 7500
Text Notes 2600 4400 0    79   ~ 16
Rev.
Text Notes 5400 4400 0    79   ~ 16
Description
Wire Notes Line style solid
	8700 4200 8700 7500
Wire Notes Line style solid
	9700 3000 9700 7500
Wire Notes Line style solid
	2500 7500 9700 7500
Text Notes 9050 4400 0    79   ~ 16
Date
Text Notes 6400 3200 0    59   Italic 0
(Modification of all the variants or layout impact)
Wire Notes Line style solid
	2500 4900 9700 4900
Wire Notes Line style solid
	2500 5450 9700 5450
Text Notes 2650 4750 0    79   ~ 0
A
Text Notes 3100 4750 0    79   ~ 0
Creation of schematics. Before layout.
Text Notes 8800 4750 0    79   ~ 0
25 aout 2019
Wire Notes Line style solid
	13000 3000 20200 3000
Wire Notes Line style solid
	13000 3000 13000 7500
Wire Notes Line style solid
	13000 3300 20200 3300
Text Notes 13600 3200 0    79   ~ 16
V1 Variant Tracking : Anologic + Without “Cursor / Page” Toggle
Wire Notes Line style solid
	13000 4500 20200 4500
Wire Notes Line style solid
	13400 4200 13400 7500
Text Notes 13100 4400 0    79   ~ 16
Rev.
Text Notes 15900 4400 0    79   ~ 16
Description
Wire Notes Line style solid
	19200 4200 19200 7500
Wire Notes Line style solid
	20200 3000 20200 7500
Wire Notes Line style solid
	13000 7500 20200 7500
Text Notes 19550 4400 0    79   ~ 16
Date
Text Notes 17750 3200 0    59   Italic 0
(Modification of this variant on bill of material)
Wire Notes Line style solid
	13000 4900 20200 4900
Wire Notes Line style solid
	13000 5450 20200 5450
Text Notes 13100 4750 0    79   ~ 0
A00
Text Notes 13600 4750 0    79   ~ 0
Creation of schematics. Before layout.
Text Notes 19300 4750 0    79   ~ 0
25 aout 2019
Wire Notes Line style solid
	2500 4200 9700 4200
Text Notes 2650 3950 0    79   ~ 0
List of the\nAlways Not\nMounted\nComponents
Wire Notes Line style solid
	3500 3300 3500 4200
Wire Notes Line style solid
	13000 4200 20200 4200
Text Notes 13150 3950 0    79   ~ 0
List of the\nComponents\nunder this\nvariant
Wire Notes Line style solid
	14000 3300 14000 4200
Wire Notes Line width 20 style solid
	500  1500 22900 1500
Text Notes 3400 1200 0    315  ~ 63
Schéma Électronique du Clavier ToucheLibre     ;     Carte Flèche Droite
Text Notes 17950 14450 0    59   ~ 12
Remarques Générales :\nSauf indication contraire, les résistances sont en 1%, boitier 0402 / 1005M, soit 63mW.
Text Notes 3700 3850 0    79   ~ 0
ANM > Empty\n\n(All the tagged components with CD (Core Design) are mounted for all the variants.)
Text Notes 14200 4050 0    79   ~ 0
G1 = Mounted     >  Q52001, Q52002, R52001-R52004\nG2 = Not Mounted >  D52001-D52004\nG3 = Not Mounted >  SW52005\nG4 = Not Mounted >  Q52003, R52005\nG5 = Not Mounted >  D52005
Wire Notes Line style solid
	13000 9000 20200 9000
Wire Notes Line style solid
	13000 9000 13000 13500
Wire Notes Line style solid
	13000 9300 20200 9300
Text Notes 13600 9200 0    79   ~ 16
V2 Variant Tracking : Analogic + WITH “Cursor / Page” Toggle
Wire Notes Line style solid
	13000 10500 20200 10500
Wire Notes Line style solid
	13400 10200 13400 13500
Text Notes 13100 10400 0    79   ~ 16
Rev.
Text Notes 15900 10400 0    79   ~ 16
Description
Wire Notes Line style solid
	19200 10200 19200 13500
Wire Notes Line style solid
	20200 9000 20200 13500
Wire Notes Line style solid
	13000 13500 20200 13500
Text Notes 19550 10400 0    79   ~ 16
Date
Text Notes 17750 9200 0    59   Italic 0
(Modification of this variant on bill of material)
Wire Notes Line style solid
	13000 10900 20200 10900
Wire Notes Line style solid
	13000 11500 20200 11500
Text Notes 13100 10750 0    79   ~ 0
A00
Text Notes 13600 10750 0    79   ~ 0
Creation of schematics. Before layout.
Text Notes 19300 10750 0    79   ~ 0
25 aout 2019
Wire Notes Line style solid
	13000 10200 20200 10200
Text Notes 13150 9950 0    79   ~ 0
List of the\nComponents\nunder this\nvariant
Wire Notes Line style solid
	14000 9300 14000 10200
Text Notes 14200 10050 0    79   ~ 0
G1 = Mounted     >  Q52001, Q52002, R52001-R52004\nG2 = Not Mounted >  D52001-D52004\nG3 = Mounted     >  SW52005\nG4 = Mounted     >  Q52003, R52005\nG5 = Not Mounted >  D52005
Text Notes 3100 5350 0    79   ~ 0
Modification to facilitate the layout:\n   * J52001 = reverse footprint issue\n   * J52001 = modification of pin-out to get easy layout.
Text Notes 2650 5150 0    79   ~ 0
B
Text Notes 8800 5150 0    79   ~ 0
20 oct 2019
Text Notes 13600 5350 0    79   ~ 0
Modification to facilitate the layout:\n   * J52001 = reverse footprint issue\n   * J52001 = modification of pin-out to get easy layout.
Text Notes 13100 5150 0    79   ~ 0
B00
Text Notes 19300 5150 0    79   ~ 0
20 oct 2019
Text Notes 13600 11400 0    79   ~ 0
Modification to facilitate the layout:\n   * J52001 = reverse footprint issue\n   * J52001 = modification of pin-out to get easy layout.
Text Notes 13100 11150 0    79   ~ 0
B00
Text Notes 19300 11150 0    79   ~ 0
20 oct 2019
$EndSCHEMATC
