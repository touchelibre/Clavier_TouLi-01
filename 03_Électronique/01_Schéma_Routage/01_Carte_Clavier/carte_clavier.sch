EESchema Schematic File Version 4
LIBS:carte_clavier-cache
EELAYER 30 0
EELAYER END
$Descr A2 23386 16535
encoding utf-8
Sheet 1 4
Title "Schematic"
Date "20 oct 2019"
Rev "B"
Comp "Lilian T."
Comment1 "TL 500 001"
Comment2 "Clavier ToucheLibre"
Comment3 "Clavier Board"
Comment4 "Tracking Sheet"
$EndDescr
$Sheet
S 15050 14600 2300 700 
U 5B24D131
F0 "Connector_Hierarchy" 79
F1 "Connector_Hierarchy.sch" 79
$EndSheet
Wire Notes Line style solid
	2500 3000 10200 3000
Wire Notes Line style solid
	2500 3000 2500 7500
Wire Notes Line style solid
	2500 3300 10200 3300
Text Notes 5000 3200 0    79   ~ 16
Core Design Tracking
Wire Notes Line style solid
	2500 4500 10200 4500
Wire Notes Line style solid
	2900 4200 2900 7500
Text Notes 2600 4400 0    79   ~ 16
Rev.
Text Notes 5400 4400 0    79   ~ 16
Description
Wire Notes Line style solid
	9200 4200 9200 7500
Wire Notes Line style solid
	10200 3000 10200 7500
Wire Notes Line style solid
	2500 7500 10200 7500
Text Notes 9550 4400 0    79   ~ 16
Date
Text Notes 6400 3200 0    59   Italic 0
(Modification of all the variants or layout impact)
Wire Notes Line style solid
	2500 4900 10200 4900
Wire Notes Line style solid
	2500 6200 10200 6200
Text Notes 2650 4750 0    79   ~ 0
A
Text Notes 3100 4750 0    79   ~ 0
Creation of schematics. Before Layout.
Text Notes 9300 4750 0    79   ~ 0
25 aout 2019
Wire Notes Line style solid
	13000 3000 20700 3000
Wire Notes Line style solid
	13000 3000 13000 7500
Wire Notes Line style solid
	13000 3300 20700 3300
Text Notes 14200 3200 0    79   ~ 16
V1 Variant Tracking : Analogic Matrix
Wire Notes Line style solid
	13000 4600 20700 4600
Wire Notes Line style solid
	13400 4300 13400 7500
Text Notes 13100 4500 0    79   ~ 16
Rev.
Text Notes 15900 4500 0    79   ~ 16
Description
Wire Notes Line style solid
	19700 4300 19700 7500
Wire Notes Line style solid
	20700 3000 20700 7500
Wire Notes Line style solid
	13000 7500 20700 7500
Text Notes 20050 4500 0    79   ~ 16
Date
Text Notes 16900 3200 0    59   Italic 0
(Modification of this variant on bill of material)
Wire Notes Line style solid
	13000 5000 20700 5000
Wire Notes Line style solid
	13000 6300 20700 6300
Text Notes 13100 4850 0    79   ~ 0
A00
Text Notes 13600 4850 0    79   ~ 0
Creation of schematics. Before Layout.
Text Notes 19800 4850 0    79   ~ 0
25 aout 2019
Wire Notes Line style solid
	2500 4200 10200 4200
Text Notes 2650 3950 0    79   ~ 0
List of the\nAlways Not\nMounted\nComponents
Wire Notes Line style solid
	3500 3300 3500 4200
Wire Notes Line style solid
	13000 4300 20700 4300
Text Notes 13150 3950 0    79   ~ 0
List of the\nComponents\nunder this\nvariant
Wire Notes Line style solid
	14000 3300 14000 4300
Wire Notes Line width 20 style solid
	500  1500 22900 1500
Text Notes 3400 1200 0    315  ~ 63
Schéma Électronique du Clavier ToucheLibre     ;     Carte Clavier
Text Notes 17900 14450 0    59   ~ 12
Remarques Générales :\nSauf indication contraire, les résistances sont en 1%, boitier 0402 / 1005M, soit 63mW.
Text Notes 3700 3800 0    79   ~ 0
ANM >  R13001, TP13001-TP13028\n\n(All the tagged components with CD (Core Design) are mounted for all the variants.)
Text Notes 14100 4250 0    79   ~ 0
G1 = Mounted     >  Q14001-Q14052, R14009-R14108, R14116-R14119\nG2 = Not Mounted >  C13001-C13003, D14001-D14104, Q13001, Q13002, R12001-R12003, \n                       R13002-R13004, U13001\nG3 = 10kΩ        >  R14001-R14008 = TL 200 008\nG4 = Mounted     >  J12009, J12010\nG5 = Mounted     >  J12004, J12005\nG6 = Mounted     >  J12002
Wire Notes Line style solid
	13000 9000 20700 9000
Wire Notes Line style solid
	13000 9000 13000 13500
Wire Notes Line style solid
	13000 9300 20700 9300
Text Notes 14200 9200 0    79   ~ 16
V2 Variant Tracking : Logic Matrix
Wire Notes Line style solid
	13000 10600 20700 10600
Wire Notes Line style solid
	13400 10300 13400 13500
Text Notes 13100 10500 0    79   ~ 16
Rev.
Text Notes 15900 10500 0    79   ~ 16
Description
Wire Notes Line style solid
	19700 10300 19700 13500
Wire Notes Line style solid
	20700 9000 20700 13500
Wire Notes Line style solid
	13000 13500 20700 13500
Text Notes 20050 10500 0    79   ~ 16
Date
Text Notes 16900 9200 0    59   Italic 0
(Modification of this variant on bill of material)
Wire Notes Line style solid
	13000 11000 20700 11000
Wire Notes Line style solid
	13000 12300 20700 12300
Text Notes 13100 10850 0    79   ~ 0
A00
Text Notes 13600 10850 0    79   ~ 0
Creation of schematics. Before Layout.
Text Notes 19800 10850 0    79   ~ 0
25 aout 2019
Wire Notes Line style solid
	13000 10300 20700 10300
Text Notes 13150 9950 0    79   ~ 0
List of the\nComponents\nunder this\nvariant
Wire Notes Line style solid
	14000 9300 14000 10300
Text Notes 14100 10250 0    79   ~ 0
G1 = Not Mounted >  Q14001-Q14052, R14009-R14108, R14116-R14119\nG2 = Mounted     >  C13001-C13003, D14001-D14104, Q13001, Q13002, R12001-R12003, \n                       R13002-R13004, U13001\nG3 = 0Ω          >  R14001-R14008 = TL 200 001\nG4 = Mounted     >  J12009, J12010\nG5 = Mounted     >  J12004, J12005\nG6 = Mounted     >  J12002
Text Notes 3100 6100 0    79   ~ 0
Modification to facilitate the layout:\n   * J12001 = 4 pins instead of 2 pins\n   * J12010, J12005, J12008 = reverse footprint issue\n   * J12003, J12006, J12007, J12008 = modification of pin-out to get easy layout\n   * Add global port VBUS_F to complete the net list.\n   * Modification of USB net name to be compliant with Kicad rule (_N and _P termination)\n   * Rename net Trans_Col_4 to Trans_F4\n   * The input line reading is placed in middle (Lig_xx became Lig_bis_xx)\n   * C13001 : error footprint, S0805 is applied
Text Notes 9300 5150 0    79   ~ 0
20 oct 2019
Text Notes 2650 5150 0    79   ~ 0
B
Text Notes 13600 6200 0    79   ~ 0
Modification to facilitate the layout:\n   * J12001 = 4 pins instead of 2 pins\n   * J12010, J12005, J12008 = reverse footprint issue\n   * J12003, J12006, J12007, J12008 = modification of pin-out to get easy layout\n   * Add global port VBUS_F to complete the net list.\n   * Modification of USB net name to be compliant with Kicad rule (_N and _P termination)\n   * Rename net Trans_Col_4 to Trans_F4\n   * The input line reading is placed in middle (Lig_xx became Lig_bis_xx)\n   * C13001 : error footprint, S0805 is applied
Text Notes 19800 5250 0    79   ~ 0
20 oct 2019
Text Notes 13100 5250 0    79   ~ 0
B00
Text Notes 13600 12200 0    79   ~ 0
Modification to facilitate the layout:\n   * J12001 = 4 pins instead of 2 pins\n   * J12010, J12005, J12008 = reverse footprint issue\n   * J12003, J12006, J12007, J12008 = modification of pin-out to get easy layout\n   * Add global port VBUS_F to complete the net list.\n   * Modification of USB net name to be compliant with Kicad rule (_N and _P termination)\n   * Rename net Trans_Col_4 to Trans_F4\n   * The input line reading is placed in middle (Lig_xx became Lig_bis_xx)\n   * C13001 : error footprint, S0805 is applied
Text Notes 19800 11250 0    79   ~ 0
20 oct 2019
Text Notes 13100 11250 0    79   ~ 0
B00
$EndSCHEMATC
