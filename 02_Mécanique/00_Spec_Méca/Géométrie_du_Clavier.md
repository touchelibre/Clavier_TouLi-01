# Géométrie du Clavier

## Les choix ergonomiques

Voici les paradigmes de base :

* Cas d’usage : Quand on est confortablement installé au bureau ou à la maison.
* Clavier «full size», c’est à dire avec pavé numérique et toutes les touches de fonctions.
* Les mains (sauf accident) sont symétriques. Donc, le clavier doit être symétrique.
* Adapter la position des touches selon la longueur des doigts.
* Position des mains en diagonale pour ne pas se tordre le poignet.
* Sur‑élever le poignet pour éviter le syndrome du canal carpien.
* Maximisation de l’utilisation des pouces et minimisation de l’utilisation des auriculaires.
* Pavé numérique placé au milieu pour pouvoir l’utiliser des deux mains.
* Le bon positionnement des mains et des doigts, doit être trouvé intuitivement par l’utilisateur.


## Les repères tactiles et visuels

En marron : repère visuel   ;   Étoile : repère tactile

Les carrés pour le monde des lettres, les ronds pour le monde des chiffres.

Cela structure l’espace et rend la mémorisation du clavier plus naturelle.

Mettre les glyphes sur les touches est une option que l’on considère contre‑productive pour l’apprentissage du clavier. Est‑ce un hasard si sur un instrument de musique les notes ne sont jamais écrites ? Et ça n’a jamais empêcher personne de jouer pendant des heures…


## Quel doigt utiliser où ?

Vert : pouce   ;  Orange : index   ;   Bleu : majeur   ;   Jaune : annulaire   ;   Rouge : auriculaire   ;   Blanc : «free style»   ;   Entouré en rose :  Position de repos

L’utilisation des 10 doigts est recommandée pour une bonne efficacité et une bonne fluidité dans l’utilisation.


## Taux d’accessibilité ?

Couleur chaude (vers le rouge) : les plus accessibles   ;   Couleur froide (vers le bleu) : les moins accessibles

(Il s’agit d’une estimation approximative.)

Cela va guider ensuite comment placer les caractères les plus utilisés. Voir la page «Disposition Kéa».


## Le cas de l’handicap

Les cas des handicaps ou des malformations sont très nombreux et aux conséquences multiples. Voir wikipédia.

Ils ne seront pas traités dans une premier temps.

Le premier cas traité devrait être le cas de la perte d’une main. En créant une disposition qui facilite l’écriture avec une seule main…

