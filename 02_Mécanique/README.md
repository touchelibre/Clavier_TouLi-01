[ToucheLibre project](http://touchelibre.fr)

![Logo](http://touchelibre.fr/wp-content/uploads/2019/03/Icon_ToucheLibre_V3.png)


# Partie Mécanique

## 00_Spec_Méca

Spécification mécanique du clavier.


## 01_Dessin_Design

Expérimentation, premiers prototypes et ébauche de dessin.


## 02_Plan_2D

Fichiers nécessaires à la fabrication ou le transfert de dessin nécessaire pour le routage du PCB.


## 03_Plan_3D

Conception détaillé du clavier en 3D avec FreeCAD.


## 04_Décoration

Habillage esthétique du clavier, marqueterie, gravure, etc.


