#Alphabet Stylé

##Recherche d’alphabet original

Pour faire un clavier stylé avec un alphabet qui semble être extraterrestre.
On peut écrire sur le clavier un alphabet inconnue du grand public.

Exemple :
A6A0 Bamum A6FF
ꚠꚡꚢꚣꚤꚥ	ꚦꚧꚨꚩꚪꚫ
ꚬꚭꚮꚯꚰꚱ	ꚲꚳꚴꚵꚶꚷ
ꚸꚹꚺꚻꚼꚽ	ꚾꚿꛀꛁꛂꛃ
ꛄꛅꛆꛇꛈꛉ	ꛊꛋꛌꛍꛎꛏ
ꛐꛑꛒꛓꛔꛕ	ꛖꛗꛘꛙꛚꛛ
ꛜꛝꛞꛟꛠꛡ	ꛢꛣꛤꛥꛦꛧ
ꛨꛩꛪꛫꛬꛭ	ꛮꛯ꛲꛳꛴꛵
꛶꛷

Ou le N’ko:
0123456789
߀߁߂߃߄߅߆߇߈߉
aeiouyéèà
ߊߍߌߐߎߋߏ
Décision : Non c’est de droite à gauche


##Implémentation

###Pour la partie alphabetique et les spéciaux
Au hazard
~%$@"«	»‑<>/©
ꚠꚡꚢꚣꚤꚥ	ꚦꚧꚨꚩꚪꚫ

képuv(	)lcdmz
ꚬꚭꚮꚯꚰꚱ	ꚲꚳꚴꚵꚶꚷ

èoaie,	.rstnx
ꛄꛅꛆꛇꛈꚽ	ꛊꛋꛌꛍꛎꛏ

àfyjw’	^hgbqç
ꛐꛑꛒꛓꛔꛕ	ꛖꛗꛘꛙꛚꛛ

###Pour pavé numérique
0123456789	,.	+-=		*/^		()€
ꛯꛦꛧꛨꛩꛪꛫꛬꛭꛮ	꛵꛳	꛲ꚿ꛷		ꚾꛂꚼ		⌊⌉₪

Pour +-=*/^()€ , je n’ai pas trouvé les symbloles correspondants.

###Pour les modifiers et les fonctoins
Aucun changement notable simplement faire un choix sur :
On ne met rien sur les autres touches ou on met les symboles techniques ?




