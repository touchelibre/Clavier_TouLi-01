EESchema Schematic File Version 4
LIBS:carte_clavier-rescue
LIBS:carte_clavier-cache
EELAYER 26 0
EELAYER END
$Descr A2 23386 16535
encoding utf-8
Sheet 2 5
Title "Clavier ToucheLibre   /   ToucheLibre Keyboard"
Date "16 juin 2018"
Rev "V0.0"
Comp "Lyliberté"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text Notes 1300 1200 0    79   ~ 16
Connecteurs
Wire Notes Line
	1250 1300 7100 1300
Wire Notes Line
	7100 1300 7100 15500
Wire Notes Line
	7100 15500 1250 15500
Wire Notes Line
	1250 15500 1250 1300
$Sheet
S 7700 12400 3500 2600
U 5B24D581
F0 "alim" 79
F1 "alim.sch" 79
$EndSheet
$Sheet
S 14100 8050 2650 6100
U 5B24D537
F0 "Connecteurs" 79
F1 "Connecteurs.sch" 79
$EndSheet
$Sheet
S 7600 1100 14700 1250
U 5B24D4AC
F0 "clavier" 79
F1 "clavier.sch" 79
$EndSheet
$EndSCHEMATC
