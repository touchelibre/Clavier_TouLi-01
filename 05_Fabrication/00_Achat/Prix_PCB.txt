JLCPCB

https://jlcpcb.com/quote#/

Paramètres :

Layers					4
Dimensions				424 x 243 mm²
PCB Qty					5
PCB Thickness			1,6 mm
Impedance control		Yes / JLC7628
PCB Color				Red
Surface Finish			Enig RoHS
Copper Weight			1 oz
Gold Fingers			no
Material Details		FR4 standard Tg 130°C-140°C
Panel By JLCPCB			no
Flying Probe Test		Fully tested
Castellated Holes		no
Different Design		5
Specify Order Number	no

Remark					?


Estimation prix en ligne :

    Charge Details
    Engineering fee:€28.64
    Panel:€51.90
    Large Size:€20.76
    Surface Finish:€12.98
    Board:€52.98

Build Time:

    4-5 days €0

Total Price:€167.26

Weight:1.71kg
Shipping Estimate:
€17.66  Via  France Direct Line(signature)  

Delivery Time:7-10 business days

Total = 167,26+17,66 = 184,92 €
(T/5 = 36,984 €)


~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

PCB‑prototype

https://www.pcbprototype.com/fr/devis-pcb-fr4

Paramètres :

	Nb de couches : 4
	Empilement : suivre les spec fournies
	Epaisseur matière : 1,6mm
	TG : 135°C
	Finition : ENIG
	Vernis épargne : rouge
	Sérigraphie : sans
	Epaisseur cuivre externe (base/fini) : 17 / 35 μm
	Epaisseur cuivre interne : 35 μm
	mise en flan : Different type/flan
	Longueur du flan (X) : 424 mm
	Largeur du flan (Y) : 243 mm
	Nb de type différent de C.I. : 4
	Nb total de C.I./flan : 5
	Various PCB type on one panel : Mise en flan réalisé par client
	Avez vous besoin des fichiers pâte à braser : oui
	Souhaitez-vous valider les gerbers avant production? oui
	Largeur/espace de piste >= 0,125 mm
	Diamètre de perçage fini >= 0,25 mm
	Contrôle d'impédance oui
	Via in pad
	1/2 via métalisé
	Tranche métalisée
	Jump V-cut
	Contre perçage
	Bord de carte biseauté
	Impression carbone
	Masque pelable
	Via bouché
	IPC Class : 2
	Date Code : Non
	Logo ROHS : Non
	Logo UL : Non
	Pochoir : Non
	Epaisseur : Non
	Réduction des ouvertures : Non


Estimation du Prix en ligne :

Nb de flans :		5
Prix unitaire		€ 93.41
Prix total C.I		€ 467.04
Prix pochoir		€ 0
Coût logistique		€ 29.02
Coût total			€ 496.07
(496,07/5 = 99,214 €)

Délai de production
Date C.I. rendu
9 jours
08/11/2019


~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


https://www.pcbgogo.com/pcb-board-quote.html


Material : Normal FR-4 Board
Board type : Panel by Customer
Route Process : Panel as Tab Route
Different Design in Panel : 5
Size (panel) : 424 × 243 mm²
Quantity (panel) : 5
Layers : 4 Layers
FR4-TG : TG 130-140
Thickness : 1,6 mm
Min Track/Spacing : 6/6mil (6mils in um = 152,4e0 um)
Min Hole Size : 0.3mm
Solder Mask : Red
Silkscreen : None  
Gold Fingers : No
Surface Finish : Immersion gold(ENIG)
Thickness of Immersion Gold : 1U"
Finished Copper : 1 oz. Cu



PCB Build time

4-5 days    $403
Price Comparison Matrix
Shipping costs $42
6-9 days ,   Weight: 2.303 kg
    PCB Cost $ 403
    Shipping $ 42
Total Amount :      $445
(445/5 = 89 $)


~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


https://be.eurocircuits.com/shop/orders


Service STANDARD pool
Estimated shipment date 19-11-2019
Quantity 5 panels
Board surface / Order surface
10.30 dm² / 51.52 dm²
Prices Net
Single panel : € 169.75
Total boards : € 848.75
Prices Gross* include 21.00% VAT.
Single panel : € 205.40
Total boards : € 1026.99
(1026,99/5 = 205,398€)



